package com.ptravels;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.ptravels.global.Constants;
import com.ptravels.global.Utils;

public class BaseActivity extends AppCompatActivity {
    private IntentFilter networkChangeIntentFilter;
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {


        /* running the thread because we might get the no network dialog in android 7 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Utils utils = Utils.getUtils();
                                if (!utils.isNetworkAvailable(context)) {
                                    if (!BaseActivity.this.isFinishing())
                                        utils.showEnableInternetDialog(BaseActivity.this);
                                } else {
                                    if (!BaseActivity.this.isFinishing()) {
                                        utils.dismissNetworkDialog();
                                        netBack(true);
                                    }
                                }
                            }
                        }, 500);

                    }
                });




            }
        }
    };
    private boolean isReceiverRegistered;

    @Override
    protected void onStart() {
        super.onStart();
        networkChangeIntentFilter = new IntentFilter();
        networkChangeIntentFilter.addAction(/*Constants.NETWORK_CHANGED*/"android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangeReceiver, networkChangeIntentFilter);
        isReceiverRegistered = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils utils = Utils.getUtils();
        if (!utils.isNetworkAvailable(BaseActivity.this)) {
            if (!BaseActivity.this.isFinishing())
                utils.showEnableInternetDialog(BaseActivity.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (networkChangeReceiver != null && isReceiverRegistered)
                unregisterReceiver(networkChangeReceiver);
            isReceiverRegistered = false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.getUtils().dismissNetworkDialog();
    }

    public void netBack(boolean isBack){

    }
}
