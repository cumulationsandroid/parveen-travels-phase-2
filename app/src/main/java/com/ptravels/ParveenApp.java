package com.ptravels;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.myBooking.data.model.PnrReview;

import java.util.List;

import io.fabric.sdk.android.Fabric;


/**
 * Created by Anirudh Uppunda on 18/4/17.
 */

public class ParveenApp extends Application {

    private static ParveenApp mContext;
    private static boolean isRatingDialogShown;


    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Fabric.with(this, new Crashlytics());
        mContext = this;
//        Utils.printHashKey(mContext);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public static String getAccessToken(){
        return SharedPrefUtils.getSharedPrefUtils().getAccessToken(mContext);
    }

    public static List<PnrReview> getCompletedPNRsList() {
        return ParveenDBHelper.getParveenDBHelper(mContext).getCompletedReviewPendingPNRs();
    }

    public static String getMacAddress(){
        return Utils.getUtils().getMacAddr();
    }

    public static boolean isRatingDialogShown(){
        return isRatingDialogShown;
    }

    public static void setIsRatingDialogShown(boolean isRatingDialogShown) {
        ParveenApp.isRatingDialogShown = isRatingDialogShown;
    }
}
