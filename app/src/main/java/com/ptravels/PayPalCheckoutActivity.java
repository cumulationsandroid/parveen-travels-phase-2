package com.ptravels;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.ptravels.global.Constants;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * This activity will do the paypal transaction and also does the receive.json
 */
public class PayPalCheckoutActivity extends AppCompatActivity {

    public static final String EXTRA_DATA = "paypal_extra_data";
    public static final String EXTRA_PNR = "paypal_extra_pnr";
    private String TAG = "PayPalCheckoutActivity";
    private WebView webView;
    private ProgressDialog progress;
    private String pnrStr;
    private AlertDialog dialog;
    private String dummyQuery;


    public static Map<String, String> getQueryMap(String queryString) {
        Map<String,String> map = new HashMap<>();
        String[] array = queryString.split("&");
        for (String str : array) {
            String[] pair = str.split("=");
            if (2 != pair.length) {
                map.put(pair[0],"");
            }
            map.put(pair[0],pair[1]);
        }
        return map;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal_checkout);

        Uri uri = getIntent().getData();
        if (uri == null || uri.toString().length() == 0) {
            exitWithError(null);
            return;
        }

        try {
            String urlstr=uri.toString();
            urlstr= urlstr.substring(urlstr.lastIndexOf(",") + 1);
            dummyQuery = "eTransactionId=" + getIntent().getStringExtra(EXTRA_PNR) + "" +
                    "&token=" + urlstr + "" +
                    "&accessToken=" + ParveenApp.getAccessToken();
        }catch (Exception e){
            dummyQuery = null;
        }


        pnrStr = getIntent().getStringExtra(EXTRA_PNR);
        showProgress("Initiating PayPal...");
        webView = findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new PPWebViewClient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(uri.toString());
    }

    private void exitWithError(String query) {
        if(query==null){
            query = dummyQuery;
        }
        hideProgress();
        setResult(RESULT_CANCELED,new Intent().putExtra(EXTRA_DATA,query));
        finish();
    }

    private void exitWithSuccess(String extra) {
        hideProgress();
        setResult(RESULT_OK, new Intent().putExtra(EXTRA_DATA, extra));
        finish();
    }

    private void processRequest(String url) {

        String query;

        try {
            URL url1 = new URL(url);
            query = url1.getQuery();
            Log.e(TAG, "query-" + query);
        } catch (Exception e) {
            e.printStackTrace();
            query = null;
        }

        if (!url.contains("paymentId")) {
            query += "&paymentId=";
            query += "&PayerID=";
            query += "&accessToken="+ ParveenApp.getAccessToken();
            exitWithError(query);
            return;
        }else{
            query += "&accessToken="+ ParveenApp.getAccessToken();
            exitWithSuccess(query);
        }

//        url = url.replace("/" + pnrStr, "");
//        url = url.replace("receive", "receive.json");
//        url += "&accessToken=" + ParveenApp.getAccessToken();
//
//        showProgress("Verifying payment...");
//
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder()
//                .url(url)
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                exitWithError();
//                Log.e(TAG, "processRequest-onFailure", e);
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                if (response.isSuccessful()) {
//                    String responseStr = response.body().string();
//                    exitWithSuccess(responseStr);
//                } else {
//                    exitWithError();
//                }
//            }
//        });
    }

    private void showProgress(String msg) {
        if (progress == null) {
            progress = new ProgressDialog(this);
        }
        progress.setMessage(msg);
        progress.show();
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void onBackPressed() {
        showBackPressAlert("Abort booking?", getString(R.string.paymentBackPressAlertMsg));
    }

    private void showBackPressAlert(String title, String msg) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog, null);
        builder.setView(customView);
        builder.setCancelable(false);
        TextView titleTv = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setVisibility(View.VISIBLE);
        firstBtn.setText("CANCEL");
        secondBtn.setText("OK");
        titleTv.setText(title);
        descrpt.setText(msg);

        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                exitWithError(null);
            }
        });

        if (dialog == null || !dialog.isShowing())
            dialog = builder.create();
        dialog.show();

    }

    private class PPWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            System.out.println("URL: " + url);
            if (url.startsWith(Constants.PAYPAL_REDIRECT_URL_RELEASE)) {
                processRequest(url);
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

// http://139.59.30.0:8080/parveen-travels/api/app/paypal/receive.json?
// eTransactionId=BSPT779681733&
// paymentId=PAYID-LRC2LEY92V3147770234112W&
// token=EC-8HU83656WB8370050&
// PayerID=TNW3KLZQQFGR6&
// accessToken=MAUATRBRAFTRX6TVY6FV

// http://139.59.30.0:8080/parveen-travels/api/app/paypal/receive/BSPT295227453?
// eTransactionId=BSPT295227453&
// paymentId=PAYID-LRC2LEY92V3147770234112W&
// token=EC-8HU83656WB8370050&
// PayerID=TNW3KLZQQFGR6

}
