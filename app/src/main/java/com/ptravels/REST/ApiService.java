package com.ptravels.REST;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.response.SendRefreshTokenResponse;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.model.response.UpdateBookingResponse;
import com.ptravels.bookTicket.data.PaymentGatewayOffers;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.ValidateCouponRequest;
import com.ptravels.bookTicket.data.model.ValidateCouponResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.home.data.model.Directions;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.HomeImageResponse;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.model.VehicleResponse;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.offers.data.model.response.AddonsResponse;
import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.stationsList.data.model.response.PathParamRequest;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface ApiService {
    @GET("{accessToken}/station.json")
    Single<StationsListResponse> getStations(@Path("accessToken") String token);

    @POST("{accessToken}/search.json")
    Single<BusSearchResponse> getBusSearchResults(@Path("accessToken") String token,
                                                  @Body BusSearchRequest request);

    @POST("register.json")
    Single<SignupResponse> getSignupResponse(@Body SignupRequest request);

    @POST("login.json")
    Single<SigninResponse> getSigninResponseSingle(@Body SigninRequest request);

    @POST("{accessToken}/busmap.json")
    Single<BusMapResponse> getBusMapLayout(@Path("accessToken") String token,
                                           @Body BusMapRequest request);

    @POST("{accessToken}/ticket/block.json")
    Single<BlockTicketResponse> blockTicket(@Path("accessToken") String token,@Body BlockTicketRequest request);

    @POST("{accessToken}/ticket/history.json")
    Single<HistoryResponse> getMyBookingDetails(@Path("accessToken") String token, @Body MyBookingRequest request);

    @POST("{accessToken}/ticket/confirm/cancel.json")
    Single<CancelTicketResponse> cancelTicket(@Path("accessToken") String token,@Body CancelTicketRequest cancelTicketRequest);

    @GET("{accessToken}/payment/gateway.json")
    Single<GatewayResponse> getPaymentGateways(@Path("accessToken") String token);

    @POST("{accessToken}/preTransactionRedirect.json")
    Single<PreTransactionResponse> getPreTransactionRedirect(@Path("accessToken") String token, @Body PreTransactionRequest request);

    @POST("{accessToken}/ticket/book.json")
    Single<BookTicketResponse> bookTicket(@Path("accessToken") String token, @Body BookTicketRequest request);

    @POST("{accessToken}/{pnr}/receive.json")
    Single<ReceiveResponse> receivePaytmInfo(@Path("accessToken") String token, @Path("pnr") String pnr);

//    @POST("paypal/receive.json")
//    Single<ReceiveResponse> receivePaypalInfo(@Query("eTransactionId") String eTransactionId,
//                                              @Query("token") String token,
//                                              @Query("accessToken") String accessToken,
//                                              @Query("PayerID") String PayerID,
//                                              @Query("paymentId") String paymentId);

    @POST("paypal/receive.json")
    Single<ReceiveResponse> receivePaypalInfo(@QueryMap Map<String, String> params);


    @POST("{accessToken}/ticket/{pnr}/detail.json")
    Single<TicketDetailResponse> getTicketDetails(@Path("accessToken") String token, @Path("pnr") String pnr);

    @GET
//    @Streaming
    Single<ResponseBody> getHomeImage(@Url String imageUrl);

    @GET("app/home-image")
    Single<HomeImageResponse> getImageDetails();

    @POST("app/booking")
    Single<SendBookingResponse> sendBookingData(@Body SendBookingRequest request);

    @POST("app/booking-status")
    Single<UpdateBookingResponse> updateBookingStatus(@Body UpdateBookingStatusRequest request);

    @GET("app/feedback-options")
    Single<FeedbackOptionsResponse> getFeedbackOptions();

    @POST("app/review")
    Single<SubmitFeedbackResponse> sendFeedback(@Body SubmitFeedbackRequest request);

    @POST("app/device-token")
    Single<SendRefreshTokenResponse> sendRefreshToken(@Body SendRefreshTokenRequest request);

    @GET("{accessToken}/getAddons.json")
    Single<AddonsResponse> getAddonsList(@Path("accessToken") String token);

    @POST("{accessToken}/{couponCode}/{userMobile}/valid.json")
    Single<ValidateCouponResponse> validateCoupon(@Path("accessToken") String token,
                                                  @Path("couponCode") String couponCode,
                                                  @Path("userMobile") String userMobile,
                                                  @Body ValidateCouponRequest request);

    @GET
    Single<LiveTrackResponse> getLiveTracks(@Url String trackUrl);

    @POST("{accessToken}/{PNR}/vehicle.json")
    Single<VehicleResponse> getVehicleInfo(@Path("accessToken") String token, @Path("PNR") String PNR);

    /*Mock*/
    @POST("/login")
    Single<SigninResponse> getMockLogin(@Body SigninRequest request);

    @GET("/station")
    Single<StationsListResponse> getMockStations();

    @POST("/search")
    Single<BusSearchResponse> getMockBusSearchResults();

//    @POST("/busmap")
//    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

    @POST("/busmap_sleeper")
    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

//    @POST("/busmap_sleeper2")
//    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

    @POST("/history")
    Single<HistoryResponse> getMockMyBookingDetails(@Body MyBookingRequest request);

    @GET("/gateway")
    Single<GatewayResponse> getMockPaymentGatewayDetails();

    /*
    * {"gatewayMode":"CC","paymentId":"paytm","pnr":"BSPT172789958"}*/
    @GET("/preTransactionRedirect")
    Single<PreTransactionResponse> getMockPreTransactionData();

    @POST("/ticketDetail")
    Single<TicketDetailResponse> getMockTicketDetails(@Body PathParamRequest request);

    @POST("/cancelTicket")
    Single<CancelTicketResponse> mockCancelTicket(@Body CancelTicketRequest cancelTicketRequest);


    @GET("api/directions/json")
    Single<Directions> getDirections(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode, @Query("key") String key);

    @GET("offers.json")
    Single<List<PaymentGatewayOffers>> getOffersToDisplay();


}
