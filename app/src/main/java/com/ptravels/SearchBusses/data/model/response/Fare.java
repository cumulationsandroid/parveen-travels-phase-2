package com.ptravels.SearchBusses.data.model.response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Fare {
    private double fare;
    private String seatType;
    private double serviceTax;

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(double serviceTax) {
        this.serviceTax = serviceTax;
    }
}
