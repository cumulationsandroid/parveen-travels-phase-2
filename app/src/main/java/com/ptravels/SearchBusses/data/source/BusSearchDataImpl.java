package com.ptravels.SearchBusses.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.SearchBusses.domain.BusSearchDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BusSearchDataImpl implements BusSearchDataInterface {
    @Override
    public Single<BusSearchResponse> getBusSearchResults(BusSearchRequest request) {
        /*RestClient.enableMockApi = true;

        return RestClient.getApiService().getMockBusSearchResults()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/

        return RestClient.getApiService().getBusSearchResults(ParveenApp.getAccessToken(),request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
