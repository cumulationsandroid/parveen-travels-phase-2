package com.ptravels.SearchBusses.presentation;

import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BoardingPoint;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.SearchBusses.data.model.response.DroppingPoint;
import com.ptravels.SearchBusses.data.model.response.Schedule;
import com.ptravels.SearchBusses.domain.BusSearchUseCase;
import com.ptravels.global.Utils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BusSearchPresenterImpl implements BusSearchPresenter {

    private BusSearchUseCase busSearchUseCase;
    private BusSearchView busSearchView;
    HashMap<Integer, BoardingPoint> boardingPointHashMap = new HashMap<>();
    HashMap<Integer, DroppingPoint> droppingPointHashMap = new HashMap<>();
    private List<String> brPtNamesList = new ArrayList<>();
    private List<String> drPtNamesList = new ArrayList<>();

    public BusSearchPresenterImpl(BusSearchView busSearchView, BusSearchUseCase busSearchUseCase) {
        this.busSearchView = busSearchView;
        this.busSearchUseCase = busSearchUseCase;
    }

    @Override
    public void getBusSearchResults(final BusSearchRequest busSearchRequest) {
        if (Utils.isOnline()) {
            busSearchUseCase.execute(busSearchRequest)
                    .subscribe(new SingleSubscriber<BusSearchResponse>() {
                        @Override
                        public void onSuccess(final BusSearchResponse busSearchResponse) {
                            if (busSearchResponse != null && busSearchResponse.getSuccess()) {
                                /**
                                 *  Creating the obeservable to find the boardingMap and deboardingMap
                                 */
                                Observable.create(new Observable.OnSubscribe<BusSearchResponse>() {
                                    @Override
                                    public void call(Subscriber<? super BusSearchResponse> subscriber) {

                                        if (busSearchResponse.getData() != null && busSearchResponse.getData().getSchedules() != null) {
                                            ArrayList<Schedule> schedules = (ArrayList<Schedule>) busSearchResponse.getData().getSchedules();
                                            for (int i = 0; i < schedules.size(); i++) {

                                                long arrivalMillis = Utils.getUtils().getArrivalTimeMillis(busSearchRequest.getTravelDate(), "yyyy-MM-dd",
                                                        schedules.get(i).getArrivalTime(), "h:mm a", schedules.get(i).getDepartureTime());
                                                long departMillis = Utils.getDeptTimeInMillis(busSearchRequest.getTravelDate(), "yyyy-MM-dd",
                                                        schedules.get(i).getDepartureTime(), "h:mm a");
                                                schedules.get(i).setArrivalTimeMillis(arrivalMillis);
                                                schedules.get(i).setDepartureTimeMillis(departMillis);

                                                long diffe = Math.abs(departMillis - arrivalMillis);
                                                String duration = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toHours(diffe),
                                                        TimeUnit.MILLISECONDS.toMinutes(diffe) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diffe)));
                                                schedules.get(i).setDuration(duration);

                                                ArrayList<BoardingPoint> boardingPoints = (ArrayList<BoardingPoint>) schedules.get(i).getBoardingPoints();
                                                ArrayList<DroppingPoint> droppingPoints = (ArrayList<DroppingPoint>) schedules.get(i).getDroppingPoints();
                                                compileHashMapFromStations(boardingPoints, droppingPoints);
                                            }

                                            Collections.sort(schedules, new DeptTimeMillisSortComparator(busSearchRequest.getTravelDate()));
                                            busSearchResponse.getData().setSchedules(schedules);
                                        }
                                        subscriber.onNext(busSearchResponse);
                                        subscriber.onCompleted();
                                        return;
                                    }


                                }).observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(new Action1<BusSearchResponse>() {
                                            @Override
                                            public void call(BusSearchResponse busSearchResponse) {
//                                            busSearchView.showBusSearchResults(busSearchResponse, boardingPointHashMap, droppingPointHashMap);
                                                busSearchView.showBusSearchResults(busSearchResponse, brPtNamesList, drPtNamesList);
                                            }

                                        }, new Action1<Throwable>() {
                                            @Override
                                            public void call(Throwable throwable) {
                                                busSearchView.showError(throwable.getMessage());
                                            }
                                        });
                            } else {
                                String errors = RestClient.get200StatusErrors(busSearchResponse.getError());
                                busSearchView.showError(errors);
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            busSearchView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    busSearchView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    busSearchView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    busSearchView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                busSearchView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            busSearchView.showError(Constants.NO_INTERNET);
        }
    }

    private void compileHashMapFromStations(ArrayList<BoardingPoint> boardingPoints, ArrayList<DroppingPoint> droppingPoints) {

        for (BoardingPoint point : boardingPoints) {
            if (!brPtNamesList.contains(point.getStationPointName()))
                brPtNamesList.add(point.getStationPointName());
//            boardingPointHashMap.put(point.getStationPointId(), point);
        }

        for (DroppingPoint dpoint : droppingPoints) {
//            droppingPointHashMap.put(dpoint.getStationPointId(), dpoint);
            if (!drPtNamesList.contains(dpoint.getStationPointName()))
                drPtNamesList.add(dpoint.getStationPointName());
        }
    }

    public class DeptTimeMillisSortComparator implements Comparator<Schedule> {
        String travelDate;

        public DeptTimeMillisSortComparator(String travelDate) {
            this.travelDate = travelDate;
        }

        @Override
        public int compare(Schedule o1, Schedule o2) {
            return Long.valueOf(o1.getDepartureTimeMillis()).compareTo((o2.getDepartureTimeMillis()));
        }
    }


}