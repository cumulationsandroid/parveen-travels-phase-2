package com.ptravels.adminPanel.data.model;

import java.io.Serializable;

/**
 * Created by Amit Tumkur on 01-03-2018.
 */

public class FcmData implements Serializable{
    private String title;
    private String description;
    private String offer_id;
    private String image_url;

    private String from_city;
    private String to_city;
    private String travel_date;
    private String pnr;
    private String device_token;

    public FcmData(String title, String description, String offer_id, String image_url) {
        this.title = title;
        this.description = description;
        this.offer_id = offer_id;
        this.image_url = image_url;
    }

    public FcmData(String from_city, String to_city, String travel_date, String pnr, String device_token) {
        this.from_city = from_city;
        this.to_city = to_city;
        this.travel_date = travel_date;
        this.pnr = pnr;
        this.device_token = device_token;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public String getTitle() {
        return title;
    }

    public String getFrom_city() {
        return from_city;
    }

    public String getTo_city() {
        return to_city;
    }

    public String getTravel_date() {
        return travel_date;
    }

    public String getPnr() {
        return pnr;
    }

    public String getDevice_token() {
        return device_token;
    }
}
