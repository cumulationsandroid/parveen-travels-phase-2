package com.ptravels.adminPanel.data.model.request;

public class SendBookingRequest
{
    private String travel_date;

    private String price;

    private String booking_date;

    private String booking_status;

    private String from_city;

    private String to_city;

    private String pnr;

    private String device_id;

    private String seats;

    private String mobile_os;

    public String getTravel_date ()
    {
        return travel_date;
    }

    public void setTravel_date (String travel_date)
    {
        this.travel_date = travel_date;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getBooking_date ()
    {
        return booking_date;
    }

    public void setBooking_date (String booking_date)
    {
        this.booking_date = booking_date;
    }

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getFrom_city ()
    {
        return from_city;
    }

    public void setFrom_city (String from_city)
    {
        this.from_city = from_city;
    }

    public String getTo_city ()
    {
        return to_city;
    }

    public void setTo_city (String to_city)
    {
        this.to_city = to_city;
    }

    public String getPnr ()
    {
        return pnr;
    }

    public void setPnr (String pnr)
    {
        this.pnr = pnr;
    }

    public String getDevice_id ()
    {
        return device_id;
    }

    public void setDevice_id (String device_id)
    {
        this.device_id = device_id;
    }

    public String getSeats ()
    {
        return seats;
    }

    public void setSeats (String seats)
    {
        this.seats = seats;
    }

    public String getMobile_os ()
    {
        return mobile_os;
    }

    public void setMobile_os (String mobile_os)
    {
        this.mobile_os = mobile_os;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [travel_date = "+travel_date+", price = "+price+", booking_date = "+booking_date+", booking_status = "+booking_status+", from_city = "+from_city+", to_city = "+to_city+", pnr = "+pnr+", device_id = "+device_id+", seats = "+seats+", mobile_os = "+mobile_os+"]";
    }
}