package com.ptravels.adminPanel.data.model.request;

public class SendRefreshTokenRequest {
    private String device_token;

    private String device_id;

    private String mobile_os;

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMobile_os() {
        return mobile_os;
    }

    public void setMobile_os(String mobile_os) {
        this.mobile_os = mobile_os;
    }

    @Override
    public String toString() {
        return "ClassPojo [device_token = " + device_token + ", device_id = " + device_id + ", mobile_os = " + mobile_os + "]";
    }
}