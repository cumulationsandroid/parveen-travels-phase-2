package com.ptravels.adminPanel.data.model.request;

public class UpdateBookingStatusRequest
{
    private String booking_status;

    private String pnr;

    public String getBooking_status ()
    {
        return booking_status;
    }

    public void setBooking_status (String booking_status)
    {
        this.booking_status = booking_status;
    }

    public String getPnr ()
    {
        return pnr;
    }

    public void setPnr (String pnr)
    {
        this.pnr = pnr;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [booking_status = "+booking_status+", pnr = "+pnr+"]";
    }
}