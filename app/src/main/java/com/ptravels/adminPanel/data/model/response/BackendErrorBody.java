package com.ptravels.adminPanel.data.model.response;

/**
 * Created by Amit Tumkur on 21-02-2018.
 */

public class BackendErrorBody {
    private String status;
    private String description;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
