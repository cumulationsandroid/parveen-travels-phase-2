package com.ptravels.adminPanel.data.model.response;

public class BookingRecords
{
    private String booking_id;

    public String getBooking_id ()
    {
        return booking_id;
    }

    public void setBooking_id (String booking_id)
    {
        this.booking_id = booking_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [booking_id = "+booking_id+"]";
    }
}