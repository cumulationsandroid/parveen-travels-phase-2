package com.ptravels.adminPanel.data.model.response;

public class SendBookingResponse
{
    private String status;

    private BookingRecords records;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public BookingRecords getRecords ()
    {
        return records;
    }

    public void setRecords (BookingRecords records)
    {
        this.records = records;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", records = "+records+"]";
    }
}