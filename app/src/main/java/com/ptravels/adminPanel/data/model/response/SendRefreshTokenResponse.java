package com.ptravels.adminPanel.data.model.response;

public class SendRefreshTokenResponse
{
    private String status;

    private TokenRecords records;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public TokenRecords getRecords ()
    {
        return records;
    }

    public void setRecords (TokenRecords records)
    {
        this.records = records;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", records = "+records+"]";
    }
}