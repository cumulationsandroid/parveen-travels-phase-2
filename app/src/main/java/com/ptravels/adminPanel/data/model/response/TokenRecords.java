package com.ptravels.adminPanel.data.model.response;

/**
 * Created by Amit Tumkur on 23-02-2018.
 */

public class TokenRecords {
    private String t_id;

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }
}
