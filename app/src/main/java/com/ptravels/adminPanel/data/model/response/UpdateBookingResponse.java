package com.ptravels.adminPanel.data.model.response;

/**
 * Created by Amit Tumkur on 22-02-2018.
 */

public class UpdateBookingResponse {
    private String status;
    private String records;
    private String description;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
