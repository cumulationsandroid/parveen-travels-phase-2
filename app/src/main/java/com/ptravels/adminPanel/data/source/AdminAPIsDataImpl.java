package com.ptravels.adminPanel.data.source;

import com.ptravels.REST.RestClient;
import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.model.response.SendRefreshTokenResponse;
import com.ptravels.adminPanel.data.model.response.UpdateBookingResponse;
import com.ptravels.adminPanel.domain.AdminAPIsDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class AdminAPIsDataImpl implements AdminAPIsDataInterface {

    @Override
    public Single<SendBookingResponse> sendBookingData(SendBookingRequest request) {
        return RestClient.getApiServiceForAdminPanel().sendBookingData(request)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<UpdateBookingResponse> updateBookingStatus(UpdateBookingStatusRequest request) {
        return RestClient.getApiServiceForAdminPanel().updateBookingStatus(request)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<SendRefreshTokenResponse> sendToken(SendRefreshTokenRequest request) {
        return RestClient.getApiServiceForAdminPanel().sendRefreshToken(request)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
