package com.ptravels.adminPanel.domain;

import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.model.response.SendRefreshTokenResponse;
import com.ptravels.adminPanel.data.model.response.UpdateBookingResponse;

import rx.Single;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public interface AdminAPIsDataInterface {
    Single<SendBookingResponse> sendBookingData(SendBookingRequest request);
    Single<UpdateBookingResponse> updateBookingStatus(UpdateBookingStatusRequest request);
    Single<SendRefreshTokenResponse> sendToken(SendRefreshTokenRequest request);
}
