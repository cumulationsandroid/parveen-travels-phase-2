package com.ptravels.adminPanel.domain;

import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.source.AdminAPIsDataImpl;
import com.ptravels.global.UseCase;

import rx.Single;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class SendBookingDataUseCase extends UseCase<SendBookingRequest,SendBookingResponse> {
    private AdminAPIsDataInterface adminAPIsDataInterface = new AdminAPIsDataImpl();

    @Override
    public Single<SendBookingResponse> buildUseCase(SendBookingRequest request) {
        return adminAPIsDataInterface.sendBookingData(request);
    }
}
