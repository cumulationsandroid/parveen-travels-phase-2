package com.ptravels.adminPanel.domain;

import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.model.response.UpdateBookingResponse;
import com.ptravels.adminPanel.data.source.AdminAPIsDataImpl;
import com.ptravels.global.UseCase;

import rx.Single;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class UpdateBookingUseCase extends UseCase<UpdateBookingStatusRequest,UpdateBookingResponse> {
    private AdminAPIsDataInterface adminAPIsDataInterface = new AdminAPIsDataImpl();

    @Override
    public Single<UpdateBookingResponse> buildUseCase(UpdateBookingStatusRequest request) {
        return adminAPIsDataInterface.updateBookingStatus(request);
    }
}
