package com.ptravels.adminPanel.presentation;

import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.domain.SendBookingDataUseCase;
import com.ptravels.adminPanel.domain.SendTokenUseCase;
import com.ptravels.adminPanel.domain.UpdateBookingUseCase;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public interface AdminAPIsPresenter {
    void sendBookingDataToAdminPanel(SendBookingDataUseCase sendBookingDataUseCase, SendBookingRequest request);
    void updateBookingToAdmin(UpdateBookingUseCase updateBookingUseCase, UpdateBookingStatusRequest request);
    void sendTokenToPanel(SendTokenUseCase sendTokenUseCase, SendRefreshTokenRequest request);
}
