package com.ptravels.adminPanel.presentation;

import android.util.Log;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.data.model.response.SendBookingResponse;
import com.ptravels.adminPanel.data.model.response.SendRefreshTokenResponse;
import com.ptravels.adminPanel.data.model.response.UpdateBookingResponse;
import com.ptravels.adminPanel.domain.SendBookingDataUseCase;
import com.ptravels.adminPanel.domain.SendTokenUseCase;
import com.ptravels.adminPanel.domain.UpdateBookingUseCase;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class AdminAPIsPresenterImpl implements AdminAPIsPresenter {


    @Override
    public void sendBookingDataToAdminPanel(SendBookingDataUseCase sendBookingDataUseCase, final SendBookingRequest request) {
        if (Utils.isOnline()) {
            sendBookingDataUseCase.execute(request)
                    .subscribe(new SingleSubscriber<SendBookingResponse>() {
                        @Override
                        public void onSuccess(SendBookingResponse value) {
                            if (value!=null) {
                                if (value.getStatus().equalsIgnoreCase("success")) {
                                    Log.d("sendDataToAdminPanel", "booking id " + value.getRecords().getBooking_id());
                                    ParveenDBHelper.getParveenDBHelper(ParveenApp.getContext()).updatePnrSentStatusToDb(request.getPnr()
                                            , value.getRecords().getBooking_id());
                                } else if (value.getStatus().equalsIgnoreCase("error")){
                                    if (value.getDescription().equalsIgnoreCase("PNR already exits!")){
                                        Log.d("sendDataToAdminPanel", "PNR already exits!");
                                        ParveenDBHelper.getParveenDBHelper(ParveenApp.getContext()).updatePnrSentStatusToDb(request.getPnr()
                                                , "");
                                    }
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            if (error.getMessage()!=null)
                                Log.d("OffersPresenter",error.getMessage());
                            if (error instanceof HttpException) {
                                ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                Log.d("sendDataToAdminPanel",RestClient.getBackendError(responseBody));
                            }
                        }
                    });
        }
    }

    @Override
    public void updateBookingToAdmin(UpdateBookingUseCase updateBookingUseCase, final UpdateBookingStatusRequest request) {
        if (Utils.isOnline()) {
            updateBookingUseCase.execute(request)
                    .subscribe(new SingleSubscriber<UpdateBookingResponse>() {
                        @Override
                        public void onSuccess(UpdateBookingResponse value) {
                            if (value != null) {
                                if (value.getStatus().equalsIgnoreCase("success")) {
                                    Log.d("status", value.getRecords());
                                    ParveenDBHelper.getParveenDBHelper(ParveenApp.getContext())
                                            .updatePnrBookingStatusToDb(request.getPnr(),request.getBooking_status());
                                } else if (value.getStatus().equalsIgnoreCase("error")) {
                                    Log.d("status", value.getDescription());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            if (error.getMessage()!=null)
                                Log.d("OffersPresenter",error.getMessage());
                            if (error instanceof HttpException) {
                                ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                Log.d("sendDataToAdminPanel",RestClient.getBackendError(responseBody));
                            }
                        }
                    });
        }
    }

    @Override
    public void sendTokenToPanel(SendTokenUseCase sendTokenUseCase, final SendRefreshTokenRequest request) {
        if (Utils.isOnline()) {
            sendTokenUseCase.execute(request)
                    .subscribe(new SingleSubscriber<SendRefreshTokenResponse>() {
                        @Override
                        public void onSuccess(SendRefreshTokenResponse response) {
                            if (response != null) {
                                if (response.getStatus().equalsIgnoreCase("success")) {
                                    Log.d("token Id", response.getRecords().getT_id());
                                    SharedPrefUtils.getSharedPrefUtils().updateFcmToken(ParveenApp.getContext(),request.getDevice_token());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            if (error.getMessage()!=null)
                                Log.d("OffersPresenter",error.getMessage());
                            if (error instanceof HttpException) {
                                ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                Log.d("sendDataToAdminPanel",RestClient.getBackendError(responseBody));
                            }
                        }
                    });
        }
    }
}
