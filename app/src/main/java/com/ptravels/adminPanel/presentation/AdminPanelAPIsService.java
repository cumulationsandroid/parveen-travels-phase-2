package com.ptravels.adminPanel.presentation;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.ptravels.ParveenApp;
import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.data.model.request.UpdateBookingStatusRequest;
import com.ptravels.adminPanel.domain.SendBookingDataUseCase;
import com.ptravels.adminPanel.domain.SendTokenUseCase;
import com.ptravels.adminPanel.domain.UpdateBookingUseCase;
import com.ptravels.global.Constants;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.myBooking.data.model.PnrReview;

import java.util.List;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class AdminPanelAPIsService extends IntentService {
    private AdminAPIsPresenter adminAPIsPresenter = new AdminAPIsPresenterImpl();

    public AdminPanelAPIsService(){
        super("AdminPanelAPIsService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent!=null){
            String apiType = intent.getStringExtra(Constants.API_TYPE);
            if (apiType!=null){
                if (apiType.equalsIgnoreCase(Constants.SEND_BOOKING_DATA)){
                    PnrReview pnrReview = (PnrReview) intent.getSerializableExtra(Constants.PNR_REVIEW_MODEL);
                    if (pnrReview!=null){
                        sendBookingData(pnrReview);
                    }
                }

                if (apiType.equalsIgnoreCase(Constants.SEND_DB_BOOKING_DATA)){
                    List<SendBookingRequest> pendingPnrs = ParveenDBHelper.getParveenDBHelper(ParveenApp.getContext())
                            .getServerPendingPNRs();
                    for (SendBookingRequest request : pendingPnrs){
                        sendBookingData(request);
                    }
                }

                if (apiType.equalsIgnoreCase(Constants.UPDATE_BOOKING_DATA)){
                    String pnr = intent.getStringExtra(ParveenDBHelper.PNR);
                    String status = intent.getStringExtra(ParveenDBHelper.BOOKING_STATUS);

                    UpdateBookingStatusRequest request = new UpdateBookingStatusRequest();
                    request.setPnr(pnr);
                    request.setBooking_status(status);
                    adminAPIsPresenter.updateBookingToAdmin(new UpdateBookingUseCase(),request);
                }

                if (apiType.equalsIgnoreCase(Constants.SEND_TOKEN_TO_SERVER)){
                    String token = intent.getStringExtra(Constants.FCM_TOKEN);

                    SendRefreshTokenRequest request = new SendRefreshTokenRequest();
                    request.setDevice_token(token);
                    request.setDevice_id(ParveenApp.getMacAddress());
                    request.setMobile_os("Android");

                    AdminAPIsPresenter presenter = new AdminAPIsPresenterImpl();
                    presenter.sendTokenToPanel(new SendTokenUseCase(),request);
                }
            }
        }
    }

    private void sendBookingData(PnrReview pnrReview) {
        SendBookingRequest request = new SendBookingRequest();
        request.setPnr(pnrReview.getPnr());
        request.setTravel_date(pnrReview.getTravelDate());
        request.setPrice(pnrReview.getPrice());
        request.setBooking_status(pnrReview.getBookingStatus());
        if (pnrReview.getBookingDate()!=null)
            request.setBooking_date(pnrReview.getBookingDate());
        else
            request.setBooking_date(pnrReview.getTravelDate());
        request.setFrom_city(pnrReview.getFrmCity());
        request.setTo_city(pnrReview.getToCity());
        request.setSeats(pnrReview.getSeats());
        request.setDevice_id(ParveenApp.getMacAddress());
        request.setMobile_os("Android");

//        OffersPresenter adminAPIsPresenter = new OffersPresenterImpl();
        adminAPIsPresenter.sendBookingDataToAdminPanel(new SendBookingDataUseCase(),request);
    }

    private void sendBookingData(SendBookingRequest request) {
        adminAPIsPresenter.sendBookingDataToAdminPanel(new SendBookingDataUseCase(),request);
    }
}
