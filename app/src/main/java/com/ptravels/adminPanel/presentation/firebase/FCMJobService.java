package com.ptravels.adminPanel.presentation.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.ptravels.R;
import com.ptravels.adminPanel.data.model.FcmData;
import com.ptravels.global.Constants;
import com.ptravels.home.presentation.HomeActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FCMJobService extends JobService {
    private static final String TAG = FCMJobService.class.getSimpleName();
    private int notificationId = 0;
    private NotificationManager notificationManager;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, "Performing long running task in scheduled job");
        // TODO(developer): add long running task here.
        if (jobParameters.getExtras()!=null){
            FcmData fcmData = (FcmData) jobParameters.getExtras().getSerializable(Constants.FCM_DATA);
            if (fcmData!=null){
                sendNotification(fcmData);
            }
        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param fcmData FCM message body received.
     */
    private void sendNotification(FcmData fcmData) {

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.FCM_DATA,fcmData);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_stat_parveen_app_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                                R.mipmap.ic_notification_big))
                        .setContentTitle(fcmData.getTitle())
                        .setContentText(fcmData.getDescription())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (fcmData.getImage_url() != null) {
            Bitmap bitmap = getBitmapfromUrl(fcmData.getImage_url()); //obtain the image
            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                    .setSummaryText(fcmData.getDescription())
                    .bigPicture(bitmap));
        }

        notificationId++;
        getNotificationManager().notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    private NotificationManager getNotificationManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        String channelId = getString(R.string.default_notification_channel_id);
        CharSequence channelName = getString(R.string.channel_name);
        String channelDescription = getString(R.string.channel_description);
        NotificationChannel notificationChannel;
        notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationChannel.setDescription(channelDescription);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(R.color.colorAccentinitial);
        notificationChannel.enableVibration(true);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build();
        notificationChannel.setSound(defaultSoundUri,audioAttributes);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}