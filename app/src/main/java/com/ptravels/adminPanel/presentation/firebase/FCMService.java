package com.ptravels.adminPanel.presentation.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ptravels.R;
import com.ptravels.adminPanel.data.model.FcmData;
import com.ptravels.global.Constants;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.home.presentation.RatingReviewActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FCMService extends FirebaseMessagingService {

    private static final String TAG = FCMService.class.getSimpleName();
    private static int REQUEST_OFFERS = 1001;
    private static int REQUEST_REVIEW = 2001;
    private int notificationId = 0;
    private NotificationManager notificationManager;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (remoteMessage.getData().containsKey("offer_id")) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                FcmData fcmData = new FcmData(remoteMessage.getData().get("title"),
                        remoteMessage.getData().get("description"),
                        remoteMessage.getData().get("offer_id"),
                        remoteMessage.getData().get("image_url"));
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FCM_DATA, fcmData);
//                scheduleJob(bundle);
                createOffersNotification(fcmData);
            } else {
                FcmData fcmData = new FcmData(remoteMessage.getData().get("from_city"),
                        remoteMessage.getData().get("to_city"),
                        remoteMessage.getData().get("travel_date"),
                        remoteMessage.getData().get("pnr"),
                        remoteMessage.getData().get("device_token"));
                createRatingNotification(fcmData);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     * @param bundle
     */
    private void scheduleJob(Bundle bundle) {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        /*Job myJob = dispatcher.newJobBuilder()
                .setService(FCMJobService.class)
                .setExtras(bundle)
                .setTag("parveen_offers_fcm_job")
                .build();
        dispatcher.schedule(myJob);*/

        Job myJob = dispatcher.newJobBuilder()
                // the JobService that will be called
                .setService(FCMJobService.class)
                // uniquely identifies the job
                .setTag("parveen_offers_fcm_job")
                // one-off job
                .setRecurring(false)
                // don't persist past a device reboot
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                // start between 0 and 60 seconds from now
//                .setTrigger(Trigger.executionWindow(0, 60))
                // don't overwrite an existing job with the same tag
                .setReplaceCurrent(false)
                // retry with exponential backoff
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                // constraints that need to be satisfied for the job to run
                /*.setConstraints(
                        // only run on an unmetered network
                        Constraint.ON_UNMETERED_NETWORK,
                        // only run when the device is charging
                        Constraint.DEVICE_CHARGING
                )*/
                .setExtras(bundle)
                .build();

        dispatcher.mustSchedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param fcmData FCM message body received.
     */
    private void createOffersNotification(FcmData fcmData) {

        StringBuilder content = new StringBuilder();
        if (fcmData.getOffer_id()!=null && !fcmData.getOffer_id().isEmpty()){
            content.append("Use code:").append(fcmData.getOffer_id());
            content.append(" | ");
        }

        if (fcmData.getDescription()!=null && !fcmData.getDescription().isEmpty()){
            content.append(fcmData.getDescription());
        }

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupOffersChannel();
        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.FCM_DATA,fcmData);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_OFFERS++ /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.offer_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setColor(getResources().getColor(R.color.colorAccent))
                        .setSmallIcon(getSmallIcon())
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_notification_big))
                        .setContentTitle(fcmData.getTitle())
                        .setContentText(content.toString())
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle(fcmData.getTitle())
//                                .setSummaryText(fcmData.getDescription()+" | Use Code: "+fcmData.getOffer_id())
                                .bigText(content.toString()))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        if (fcmData.getImage_url() != null && !fcmData.getImage_url().isEmpty()) {
            Bitmap imageBitmap = getBitmapfromUrl(fcmData.getImage_url()); //obtain the image
            if (imageBitmap!=null) {
                notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                        .setBigContentTitle(fcmData.getTitle())
                        .setSummaryText(content.toString())
                        .bigPicture(imageBitmap)
                        .bigLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_notification_big)));
            }
        }

        notificationId++;
        getNotificationManager().notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    private void createRatingNotification(FcmData fcmData) {

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupReviewNotificationChannel();
        }

        String travelTime = Utils.formatDateString("yyyy-MM-dd HH:mm:ss",
                "dd MMM, yyyy h:mm a",fcmData.getTravel_date());
        String content = "Rate your travel experience from "+fcmData.getFrom_city()+" to "+fcmData.getTo_city()+ " on \'"+travelTime
                +"\'\nPNR : "+fcmData.getPnr();
        Intent intent = new Intent(this, RatingReviewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK
                /*| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS*/);
        intent.putExtra(Constants.FCM_DATA, fcmData);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_REVIEW++ /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String channelId = getString(R.string.review_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setColor(getResources().getColor(R.color.colorAccent))
                        .setSmallIcon(getSmallIcon())
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_notification_big))
                        .setContentTitle("Review your journey")
                        .setContentText(content)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .setBigContentTitle("Review your journey")
//                                .setSummaryText(content)
                                .bigText(content))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        notificationId++;
        getNotificationManager().notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    private NotificationManager getNotificationManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupReviewNotificationChannel() {
        String channelId = getString(R.string.review_channel_id);

        if (Utils.getUtils().isNotificationChannelCreated(this,channelId))
            return;

        CharSequence channelName = "Travel Review";
        String channelDescription = "Push notification to get \'Parveen Travels\' journey reviews from user.";
        NotificationChannel notificationChannel;
        notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationChannel.setDescription(channelDescription);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(R.color.booking_green);
        notificationChannel.enableVibration(true);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build();
        notificationChannel.setSound(defaultSoundUri,audioAttributes);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupOffersChannel() {
        String channelId = getString(R.string.offer_channel_id);

        if (Utils.getUtils().isNotificationChannelCreated(this,channelId))
            return;

        CharSequence channelName = "Parveen Offers";
        String channelDescription = "Push notifications to provide offers from Parveen Travels";
        NotificationChannel notificationChannel;
        notificationChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        notificationChannel.setDescription(channelDescription);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(R.color.colorAccentinitial);
        notificationChannel.enableVibration(true);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT)
                .build();
        notificationChannel.setSound(defaultSoundUri,audioAttributes);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private int getSmallIcon(){
        return (Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP)
                ? R.mipmap.ic_stat_parveen_app_icon : R.mipmap.ic_launcher;
    }
}