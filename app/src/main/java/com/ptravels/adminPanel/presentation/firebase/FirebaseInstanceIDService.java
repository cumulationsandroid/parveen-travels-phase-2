package com.ptravels.adminPanel.presentation.firebase;

import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.ptravels.ParveenApp;
import com.ptravels.adminPanel.data.model.request.SendRefreshTokenRequest;
import com.ptravels.adminPanel.domain.SendTokenUseCase;
import com.ptravels.adminPanel.presentation.AdminAPIsPresenter;
import com.ptravels.adminPanel.presentation.AdminAPIsPresenterImpl;
import com.ptravels.adminPanel.presentation.AdminPanelAPIsService;
import com.ptravels.global.Constants;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = FirebaseInstanceIDService.class.getSimpleName();
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        startService(new Intent(this, AdminPanelAPIsService.class)
                .putExtra(Constants.API_TYPE, Constants.SEND_TOKEN_TO_SERVER)
                .putExtra(Constants.FCM_TOKEN, token));
    }
}