package com.ptravels.bookTicket.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentGatewayOffers {

    @SerializedName("offer")
    @Expose
    private String offer;
    @SerializedName("paymentGateway")
    @Expose
    private String paymentGateway;

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }


}
