package com.ptravels.bookTicket.data.model;

import com.ptravels.global.ResponseError;

/**
 * Created by Amit Tumkur on 03-01-2018.
 */

public class BlockTicketResponse {
    private Data data;
    private String success;
    private ResponseError error;
    private String PAYTM_ID;

    public String getPAYTM_ID() {
        return PAYTM_ID;
    }

    public void setPAYTM_ID(String PAYTM_ID) {
        this.PAYTM_ID = PAYTM_ID;
    }

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
