package com.ptravels.bookTicket.data.model;

/**
 * Created by Amit Tumkur on 27-02-2018.
 */

public class CouponData {
    private String statusCode;
    private String couponValue;
    private boolean flatFlag;
    private String statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public boolean isFlatFlag() {
        return flatFlag;
    }

    public void setFlatFlag(boolean flatFlag) {
        this.flatFlag = flatFlag;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
