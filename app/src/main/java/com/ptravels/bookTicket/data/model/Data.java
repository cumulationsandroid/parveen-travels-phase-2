package com.ptravels.bookTicket.data.model;

/**
 * Created by Amit Tumkur on 03-01-2018.
 */

public class Data {
    private String pnr;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }
}
