package com.ptravels.bookTicket.data.model;

import java.util.Map;

/**
 * Created by Amit Tumkur on 17-01-2018.
 */

public class ReceiveRequest {
    private String token;
    private String pnr;
    private Map<String,String> map;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
