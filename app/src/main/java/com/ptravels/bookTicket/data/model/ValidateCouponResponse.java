package com.ptravels.bookTicket.data.model;

import com.ptravels.global.ResponseError;

public class ValidateCouponResponse
{
    private String message;

    private CouponData data;
    private boolean success;
    private ResponseError error;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public CouponData getData ()
    {
        return data;
    }

    public void setData (CouponData data)
    {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}