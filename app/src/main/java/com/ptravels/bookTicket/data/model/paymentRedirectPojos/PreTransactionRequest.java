package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class PreTransactionRequest{
	private String pnr;
	private String paymentId;
	private String gatewayMode;

	public void setPnr(String pnr){
		this.pnr = pnr;
	}

	public String getPnr(){
		return pnr;
	}

	public void setPaymentId(String paymentId){
		this.paymentId = paymentId;
	}

	public String getPaymentId(){
		return paymentId;
	}

	public void setGatewayMode(String gatewayMode){
		this.gatewayMode = gatewayMode;
	}

	public String getGatewayMode(){
		return gatewayMode;
	}

	@Override
 	public String toString(){
		return 
			"PreTransactionRequest{" + 
			"pnr = '" + pnr + '\'' + 
			",paymentId = '" + paymentId + '\'' + 
			",gatewayMode = '" + gatewayMode + '\'' + 
			"}";
		}
}
