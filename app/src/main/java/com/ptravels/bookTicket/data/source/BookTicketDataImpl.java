package com.ptravels.bookTicket.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.bookTicket.data.PaymentGatewayOffers;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.ValidateCouponRequest;
import com.ptravels.bookTicket.data.model.ValidateCouponResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.domain.BookTicketDataInterface;

import java.util.List;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BookTicketDataImpl implements BookTicketDataInterface {

    @Override
    public Single<BlockTicketResponse> blockTicket(BlockTicketRequest request) {
        return RestClient.getApiService().blockTicket(ParveenApp.getAccessToken(), request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<GatewayResponse> getGateway() {
        /*RestClient.enableMockApi = true;
        return RestClient.getApiService().getMockPaymentGatewayDetails()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/

        return RestClient.getApiService().getPaymentGateways(ParveenApp.getAccessToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<PreTransactionResponse> getPreRedirectResponse(PreTransactionRequest request) {
        /*RestClient.enableMockApi = true;
        return RestClient.getApiService().getMockPreTransactionData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/

        return RestClient.getApiService().getPreTransactionRedirect(ParveenApp.getAccessToken(), request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<BookTicketResponse> bookTicket(BookTicketRequest request) {
        return RestClient.getApiService().bookTicket(ParveenApp.getAccessToken(), request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<ReceiveResponse> receivePaytmTransactInfo(ReceiveRequest request) {
        return RestClient.getApiService().receivePaytmInfo(request.getToken(), request.getPnr())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<ReceiveResponse> receivePayPalTransactInfo(ReceiveRequest request) {
        return RestClient.getApiService().receivePaypalInfo(request.getMap())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<ValidateCouponResponse> validateCouponCode(ValidateCouponRequest request) {
        return RestClient.getApiService().validateCoupon(ParveenApp.getAccessToken(),
                request.getCouponCode(),
                request.getUserMobile(),
                request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<List<PaymentGatewayOffers>> getPaymentGatewayOffers() {
        return RestClient.getCheckOffersService().getOffersToDisplay()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
