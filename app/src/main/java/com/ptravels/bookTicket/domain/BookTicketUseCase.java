package com.ptravels.bookTicket.domain;

import com.ptravels.global.UseCase;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;

import rx.Single;

/**
 * Created by Amit Tumkur on 06-01-2018.
 */

public class BookTicketUseCase extends UseCase<BookTicketRequest,BookTicketResponse> {
    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<BookTicketResponse> buildUseCase(BookTicketRequest bookTicketRequest) {
        return bookTicketDataInterface.bookTicket(bookTicketRequest);
    }
}
