package com.ptravels.bookTicket.domain;

import com.ptravels.global.UseCase;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by Amit Tumkur on 05-01-2018.
 */

public class GatewayUseCase extends UseCase<PathParamRequest,GatewayResponse> {

    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<GatewayResponse> buildUseCase(PathParamRequest requestObj) {
        return bookTicketDataInterface.getGateway();
    }
}
