package com.ptravels.bookTicket.domain;

import com.ptravels.bookTicket.data.PaymentGatewayOffers;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;
import com.ptravels.global.UseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.util.List;

import rx.Single;

/**
 * Created by Amit Tumkur on 05-01-2018.
 */

public class GetPaymentGatewayOffersUseCase extends UseCase<PathParamRequest, List<PaymentGatewayOffers>> {
    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<List<PaymentGatewayOffers>> buildUseCase(PathParamRequest paramRequest) {
        return bookTicketDataInterface.getPaymentGatewayOffers();
    }
}
