package com.ptravels.bookTicket.domain;

import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.ValidateCouponRequest;
import com.ptravels.bookTicket.data.model.ValidateCouponResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;
import com.ptravels.global.UseCase;

import rx.Single;

/**
 * Created by Amit Tumkur on 06-01-2018.
 */

public class ValidateCodeUseCase extends UseCase<ValidateCouponRequest,ValidateCouponResponse> {
    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();


    @Override
    public Single<ValidateCouponResponse> buildUseCase(ValidateCouponRequest validateCouponRequest) {
        return bookTicketDataInterface.validateCouponCode(validateCouponRequest);
    }
}
