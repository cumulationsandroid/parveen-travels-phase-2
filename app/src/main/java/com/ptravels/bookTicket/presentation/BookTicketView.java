package com.ptravels.bookTicket.presentation;

import com.ptravels.bookTicket.data.PaymentGatewayOffers;
import com.ptravels.bookTicket.data.model.ValidateCouponResponse;
import com.ptravels.global.LoadingView;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;

import java.util.List;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BookTicketView extends LoadingView {
    void showBlockTicketResult(BlockTicketResponse response);
    void provideGatewayData(GatewayResponse response);
    void showError(String error);
    void providePreTransactionData(PreTransactionResponse response);
    void showBookTicketResult(BookTicketResponse response);
    void receivePaytmInfo(ReceiveResponse response);
    void updateValidateCouponUI(ValidateCouponResponse response);
    void paymentGatewayOffers(List<PaymentGatewayOffers> offers);
}
