package com.ptravels.bookTicket.presentation;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ptravels.R;
import com.ptravels.adminPanel.data.model.FcmData;
import com.ptravels.global.Constants;
import com.ptravels.global.PaymentConstants;
import com.ptravels.global.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amit Tumkur on 09-04-2018.
 */

public class CCAvenueWebViewActivity extends AppCompatActivity {
    Intent mainIntent;
    String encVal;
    String vResponse;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private boolean isLoading;
    private android.support.v7.app.AlertDialog dialog;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_webview);
        mainIntent = getIntent();

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//get rsa key method
        get_RSA_key(mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE), mainIntent.getStringExtra(AvenuesParams.ORDER_ID));
    }



    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
//            LoadingDialog.showLoadingDialog(CCAvenueWebViewActivity.this, "Loading...");
            showLoading();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (!ServiceUtility.chkNull(vResponse).equals("")
                    && ServiceUtility.chkNull(vResponse).toString().indexOf("ERROR") == -1) {
                StringBuffer vEncVal = new StringBuffer("");
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, String.valueOf(mainIntent.getFloatExtra(AvenuesParams.AMOUNT,0))));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, mainIntent.getStringExtra(AvenuesParams.CURRENCY)));
                encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), vResponse);  //encrypt amount and currency
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
//            LoadingDialog.cancelLoading();
            hideLoading();

            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    Log.d("processHTML","html = "+html);
                    // process the html source code to get final status of transaction
                    String status = null;
                    if (html.indexOf("Failure") != -1) {
//                        status = "Transaction Declined!";
                        status = PaymentDetailsActivity.FAILURE;
                    } else if (html.indexOf("Success") != -1) {
//                        status = "Transaction Successful!";
                        status = PaymentDetailsActivity.SUCCESS;
                    } else if (html.indexOf("Aborted") != -1) {
//                        status = "Transaction Cancelled!";
                        status = PaymentDetailsActivity.CANCEL;
                    } else {
//                        status = "Status Not Known!";
                        status = PaymentDetailsActivity.FAILURE;
                    }
                    //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(CCAvenueWebViewActivity.this, PaymentDetailsActivity.class);
                    intent.putExtra(Constants.CCTXSTATUS, status);
                    setResult(PaymentDetailsActivity.CCAVENUE_TX_REQUEST_CODE,intent);
//                    PaymentDetailsActivity.ccavenueTxStatus = status;
                    finish();
                }
            }

            final WebView webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
//                    LoadingDialog.cancelLoading();
                    hideLoading();
                    if (url.indexOf("/ccavResponseHandler.php") != -1) {
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
//                    LoadingDialog.showLoadingDialog(CCAvenueWebViewActivity.this, "Loading...");
                    showLoading();
                }
            });


            try {
                String postData = AvenuesParams.ACCESS_CODE + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE), "UTF-8") + "&" + AvenuesParams.MERCHANT_ID + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.MERCHANT_ID), "UTF-8") + "&" + AvenuesParams.ORDER_ID + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.ORDER_ID), "UTF-8") + "&" + AvenuesParams.REDIRECT_URL + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.REDIRECT_URL), "UTF-8") + "&" + AvenuesParams.CANCEL_URL + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.CANCEL_URL), "UTF-8") + "&" + AvenuesParams.ENC_VAL + "=" + URLEncoder.encode(encVal, "UTF-8");
                webview.postUrl(PaymentConstants.TRANS_URL, postData.getBytes());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    public void get_RSA_key(final String ac, final String od) {
//        LoadingDialog.showLoadingDialog(this, "Loading...");
        showLoading();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainIntent.getStringExtra(AvenuesParams.RSA_KEY_URL),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(WebViewActivity.this,response,Toast.LENGTH_LONG).show();
//                        LoadingDialog.cancelLoading();
                        hideLoading();

                        if (response != null && !response.equals("")) {
                            vResponse = response;     ///save retrived rsa key
                            if (vResponse.contains("!ERROR!")) {
                                show_alert(vResponse);
                            } else {
                                new RenderView().execute();   // Calling async task to get display content
                            }


                        }
                        else
                        {
                            show_alert("No response");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        LoadingDialog.cancelLoading();
                        hideLoading();
                        //Toast.makeText(WebViewActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AvenuesParams.ACCESS_CODE, ac);
                params.put(AvenuesParams.ORDER_ID, od);
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void show_alert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(CCAvenueWebViewActivity.this).create();

        alertDialog.setTitle("Error!!!");
        if (msg.contains("\n"))
            msg = msg.replaceAll("\\\n", "");

        alertDialog.setMessage(msg);
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    private void showLoading() {
        isLoading = true;
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    private void hideLoading() {
        isLoading = false;
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    /*disable back function while showing loader*/
    @Override
    public void onBackPressed() {
        if (!isLoading) {
//            super.onBackPressed();
            showBackPressAlert("Abort booking?",getString(R.string.paymentBackPressAlertMsg));
        }
        else Toast.makeText(this, "Not allowed", Toast.LENGTH_SHORT).show();
    }

    private void showBackPressAlert(String title, String msg) {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog,null);
        builder.setView(customView);
        builder.setCancelable(false);
        TextView titleTv = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setVisibility(View.VISIBLE);
        firstBtn.setText("CANCEL");
        secondBtn.setText("OK");
        titleTv.setText(title);
        descrpt.setText(msg);

        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(CCAvenueWebViewActivity.this, PaymentDetailsActivity.class);
                intent.putExtra(Constants.CCTXSTATUS,PaymentDetailsActivity.CANCEL);
                setResult(PaymentDetailsActivity.CCAVENUE_TX_REQUEST_CODE,intent);
                finish();
            }
        });

        if (dialog == null || !dialog.isShowing())
            dialog = builder.create();
        dialog.show();

    }
}
