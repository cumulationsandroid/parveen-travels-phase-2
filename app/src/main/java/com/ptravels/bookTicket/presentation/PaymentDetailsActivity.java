package com.ptravels.bookTicket.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.PurchaseEvent;
import com.paytm.pgsdk.PaytmConstants;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.ptravels.BaseActivity;
import com.ptravels.BuildConfig;
import com.ptravels.ParveenApp;
import com.ptravels.PayPalCheckoutActivity;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.adminPanel.presentation.AdminPanelAPIsService;
import com.ptravels.bookTicket.data.PaymentGatewayOffers;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayData;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.ValidateCouponRequest;
import com.ptravels.bookTicket.data.model.ValidateCouponResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.Data;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.GatewayFormParam;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.domain.BlockTicketUseCase;
import com.ptravels.bookTicket.domain.BookTicketUseCase;
import com.ptravels.bookTicket.domain.GatewayUseCase;
import com.ptravels.bookTicket.domain.GetPaymentGatewayOffersUseCase;
import com.ptravels.bookTicket.domain.PreTransactionUseCase;
import com.ptravels.bookTicket.domain.ReceiveUseCase;
import com.ptravels.bookTicket.domain.ValidateCodeUseCase;
import com.ptravels.global.Constants;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.PaymentConstants;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.myBooking.data.model.PnrReview;
import com.ptravels.myBooking.presentation.TicketDetailsActivity;
import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.seatlayout.data.model.Passenger;
import com.ptravels.splash.presentation.SplashActivity;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PaymentDetailsActivity extends BaseActivity implements BookTicketView, CompoundButton.OnCheckedChangeListener {

    public static final int CCAVENUE_TX_REQUEST_CODE = 1254;
    public static final int PAYPAL_TX_REQUEST_CODE = 82;
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String CANCEL = "cancel";
    public static String ccavenueTxStatus;
    private RadioButton payWithPayTm;
    private RadioButton payWithPayPal;
    private RadioButton paynimo_btn;
    private RadioButton credit_btn;
    private RadioButton debit_btn;
    private RadioButton netbanking_btn;
    private RadioButton wallet_btn;
    private RadioButton emi_btn;
    private RadioButton imps_btn;
    private RadioButton cashcard_btn;

    private TextView tv_offer_paypal;
    private TextView tv_offer_paytm;
    private TextView tv_offer_ccavenue;
    private TextView tv_offer_paynimo;

    private int selected_payment_type = 0;
    private TextView referralAmountTv;
    private TextView basicAmntTextView, serviceTaxAmnt, totalAmntText;
    private LinearLayout ticketSummaryContainer;
    private int referralAmnt = 0;
    private float TOTAL_SERVICE_TAX = 0;
    private BookedSeatModel singleTripTicketData;
    private AppCompatButton payBtn;
    private BookTicketPresenterImpl bookTicketPresenter;
    private BlockTicketResponse blockTicketResponse;
    private boolean isTicketBlocked;
    private FrameLayout loaderLayout;
    private ImageView loader;
    /*Paytm*/
    private PaytmPGService paytmPGService;
    private String PAYTM_ID = "";
    private GatewayFormParam param;
    private Data paypalParam;
    private PnrReview pnrReview;
    private AppCompatButton applyCodeBtn;
    private TextInputEditText couponEditText;
    private List<String> addonsList = new ArrayList<>();
    private String discountNDCode;
    private TextView discountAlertTextView, discountAmntTextView;
    private AppCompatCheckBox ggCheckBox;
    private LinearLayout ggAmntLayout, discountLayout;
    private double discountAmnt;
    private float ggAmnt;
    private double basicAmnt;
    private double totalAmnt;
    private double totalGstAmnt;
    private String BOOKING_STATUS;
    private boolean isLoading;
    private RadioButton ccavenueBtn;
    private String PAYPAL_TX_DATA;
    private String pnrStr;
    private String paypalQueries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        if (SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this) != null) {
            singleTripTicketData = SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this);
        }

        initiateView();
    }

    public void initiateView() {
        TextView title = findViewById(R.id.title);
        title.setText("Summary");
        ticketSummaryContainer = findViewById(R.id.ticketSummaryContainer);
        credit_btn = findViewById(R.id.credit_card);
        debit_btn = findViewById(R.id.debit_card);
        netbanking_btn = findViewById(R.id.netbanking_card);
        wallet_btn = findViewById(R.id.wallet_card);
        emi_btn = findViewById(R.id.emi_card);
        imps_btn = findViewById(R.id.imps_card);
        cashcard_btn = findViewById(R.id.cash_card);
        basicAmntTextView = findViewById(R.id.fare);
        serviceTaxAmnt = findViewById(R.id.serviceTax);
        totalAmntText = findViewById(R.id.totalAmntTv);
        payBtn = findViewById(R.id.payBtn);
        payWithPayTm = findViewById(R.id.payWithPayTm);
        payWithPayPal = findViewById(R.id.payWithPayPal);

        tv_offer_paytm = findViewById(R.id.tv_offer_paytm);
        tv_offer_paypal = findViewById(R.id.tv_offer_paypal);
        tv_offer_ccavenue = findViewById(R.id.tv_offer_ccavenue);
        tv_offer_paynimo = findViewById(R.id.tv_offer_paynimo);

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        applyCodeBtn = findViewById(R.id.applyCodeBtn);
        couponEditText = findViewById(R.id.couponCodeEt);

        discountAlertTextView = findViewById(R.id.discountAlertText);
        discountAmntTextView = findViewById(R.id.discountAmntTv);
        ggCheckBox = findViewById(R.id.goGreenCb);
        ggAmntLayout = findViewById(R.id.ggAmntLayout);
        discountLayout = findViewById(R.id.discountAmntLayout);
        ccavenueBtn = findViewById(R.id.ccavenueRadioBtn);
        paynimo_btn = findViewById(R.id.paynimo_btn);

        payWithPayPal.setOnCheckedChangeListener(this);
        payWithPayTm.setOnCheckedChangeListener(this);
        ccavenueBtn.setOnCheckedChangeListener(this);
        paynimo_btn.setOnCheckedChangeListener(this);


        if (singleTripTicketData != null) {
            View ticketSummaryView = getLayoutInflater().inflate(R.layout.layout_ticket_summary, null);
            TextView fromTo = ticketSummaryView.findViewById(R.id.fromToValue);
            TextView travelDate = ticketSummaryView.findViewById(R.id.travelDate);
            TextView deptTime = ticketSummaryView.findViewById(R.id.departureTime);
            TextView emailTv = ticketSummaryView.findViewById(R.id.summary_email);
            TextView mobileNumber = ticketSummaryView.findViewById(R.id.summary_mobile_no);

            TextView boardingPtName = ticketSummaryView.findViewById(R.id.boardingPoint);
            CheckBox showPassengersCb = ticketSummaryView.findViewById(R.id.showPassengerSummaryCb);
            final LinearLayout passengersSummaryContainer = ticketSummaryView.findViewById(R.id.passengersSummeryLayout);

            fromTo.setText(singleTripTicketData.getFromStationName() + " to " + singleTripTicketData.getToStationName());
            travelDate.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, yyyy", singleTripTicketData.getTravelDate()));
            deptTime.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", singleTripTicketData.getBoardingPoint().getStationPointTime()));
            boardingPtName.setText(singleTripTicketData.getBoardingPoint().getStationPointName());
            boardingPtName.setSelected(true);

            emailTv.setText(singleTripTicketData.getContactEmailId());
            emailTv.setSelected(true);
            mobileNumber.setText(singleTripTicketData.getContactNumber());
            mobileNumber.setSelected(true);

            showPassengersCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        passengersSummaryContainer.setVisibility(View.VISIBLE);
                    } else {
                        passengersSummaryContainer.setVisibility(View.GONE);
                    }

                }
            });
            showPassengersCb.setChecked(false);
            passengersSummaryContainer.setVisibility(View.VISIBLE);

            ggCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int visibility = b ? View.VISIBLE : View.GONE;
                    ggAmntLayout.setVisibility(visibility);
                    ggAmnt = b ? 10 : 0;
//                    totalAmnt = (float) singleTripTicketData.getTotalPrice() + ggAmnt - discountAmnt;
                    totalAmnt = (basicAmnt + totalGstAmnt) + ggAmnt - discountAmnt;
//                    totalAmntText.setText(String.valueOf(totalAmnt)+" \u20B9" );
                    totalAmntText.setText(BigDecimal.valueOf(totalAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
                }
            });

            if (singleTripTicketData.getPassengerList().size() > 0) {

                for (int i = 0; i < singleTripTicketData.getPassengerList().size(); i++) {
                    View passengerSummaryView = getLayoutInflater().inflate(R.layout.layout_passengers_summary, null);
                    TextView name = passengerSummaryView.findViewById(R.id.passName);
                    TextView seatNo = passengerSummaryView.findViewById(R.id.passSeatNo);
//                    TextView age = (TextView) passengerSummaryView.findViewById(R.id.passAge);
//                    TextView gender = (TextView) passengerSummaryView.findViewById(R.id.passGender);

                    name.setSelected(true);
                    name.setText(singleTripTicketData.getPassengerList().get(i).getName()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getGender()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getAge());
                    seatNo.setText("Seat " + singleTripTicketData.getPassengerList().get(i).getSeatNbr());
//                    age.setText(singleTripTicketData.getPassengerList().get(i).getAge());
//                    gender.setText(singleTripTicketData.getPassengerList().get(i).getGender());
                    passengersSummaryContainer.addView(passengerSummaryView);
                }
            }
//            service_name.setText(singleTripTicketData.getServiceNumber());
            ticketSummaryContainer.addView(ticketSummaryView);
        }


        /*if (userDetail != null && userDetail.getResponse() != null && userDetail.getResponse().getUser_detail() != null) {
            referralAmnt = userDetail.getResponse().getUser_detail().getReferral_amount();
            if (referralAmnt > 0) {
                referralAmountTv.setVisibility(View.VISIBLE);
                referralAmountTv.setText("₹ " + referralAmnt + "" + " Reduced from referral amount! ");

                if ((Integer.valueOf(totalAmntString)) <= referralAmnt) {
                    int tonew = (Integer.valueOf(totalAmntString)) / 2;
                    referralAmnt = ((tonew));
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                } else {
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                }
            }
        }*/

        basicAmnt = singleTripTicketData.getBookedSeatTotalCost();
        totalGstAmnt = singleTripTicketData.getTotalServiceTax();
        totalAmnt = basicAmnt + totalGstAmnt;
//        payBtn.setText("Pay amount : \u20B9 " + /*singleTripTicketData.getBookedSeatTotalCost()*/finalTotalAmount);

//        basicAmntTextView.setText(basicAmnt+ " \u20B9");
        basicAmntTextView.setText(BigDecimal.valueOf(basicAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");

//        serviceTaxAmnt.setText(totalGstAmnt+ " \u20B9");
        serviceTaxAmnt.setText(BigDecimal.valueOf(totalGstAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");

//        totalAmntText.setText(totalAmnt+" \u20B9" );
        totalAmntText.setText(BigDecimal.valueOf(totalAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
        Log.d("TOTAL_SERVICE_TAX---", TOTAL_SERVICE_TAX + "");


        rxSingleClickListener(payBtn);
        rxSingleClickListener(applyCodeBtn);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (ggCheckBox.isChecked()) {
            ggAmntLayout.setVisibility(View.VISIBLE);
            ggAmnt = 10;
            totalAmnt = (basicAmnt + totalGstAmnt) + ggAmnt - discountAmnt;
//            totalAmntText.setText(String.valueOf(totalAmnt)+" \u20B9" );
            totalAmntText.setText(BigDecimal.valueOf(totalAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
        }

        pnrReview = new PnrReview();
        pnrReview.setAccessToken(ParveenApp.getAccessToken());
        pnrReview.setFrmCity(singleTripTicketData.getFromStationName());
        pnrReview.setToCity(singleTripTicketData.getToStationName());
        pnrReview.setTravelDate(singleTripTicketData.getTravelDate() + " " + singleTripTicketData.getBoardingPoint().getStationPointTime());
        pnrReview.setEmail(singleTripTicketData.getContactEmailId());
        pnrReview.setMobile(singleTripTicketData.getContactNumber());
        pnrReview.setReviewStatus(PnrReview.PENDING);
        pnrReview.setSeats(String.valueOf(singleTripTicketData.getPassengerList().size()));

//        getPaymentGateways();

        getPaymentGatewayOffers();

        payWithPayPal.setChecked(true);

    }

    private void getPaymentGateways() {
        bookTicketPresenter = new BookTicketPresenterImpl(this);
        bookTicketPresenter.getGatewayData(new GatewayUseCase(), null);
    }

    private void getPaymentGatewayOffers() {
        if (bookTicketPresenter == null)
            bookTicketPresenter = new BookTicketPresenterImpl(this);
        bookTicketPresenter.getPaymentGatewayOffers(new GetPaymentGatewayOffersUseCase(), null);
    }

    private void blockTicket() {
        BlockTicketRequest request = new BlockTicketRequest();
        request.setEmailId(singleTripTicketData.getContactEmailId());
        request.setMobileNumber(singleTripTicketData.getContactNumber());
        request.setBoardingPointId(String.valueOf(singleTripTicketData.getBoardingPoint().getStationPointId()));
        request.setFromStation(singleTripTicketData.getFromStationId());
        request.setToStation(singleTripTicketData.getToStationId());
        request.setTravelDate(singleTripTicketData.getTravelDate());
        request.setScheduleId(singleTripTicketData.getScheduleId());
        request.setPassengerDetails(singleTripTicketData.getPassengerList());

        if (ggCheckBox.isChecked())
            if (!addonsList.contains("GG|GG"))
                addonsList.add("GG|GG");

        for (Passenger passenger : singleTripTicketData.getPassengerList()) {
            if (passenger.getGender().equalsIgnoreCase("F")) {
                if (!addonsList.contains("FD|FD"))
                    addonsList.add("FD|FD");
                break;
            }
        }

        if (addonsList.size() > 0 /*&& discountNDCode!=null*/)
            request.setAddonList(addonsList);

        bookTicketPresenter = new BookTicketPresenterImpl(this, new BlockTicketUseCase());
        bookTicketPresenter.getBlockTicketResult(request);
    }

    @Override
    public void showBlockTicketResult(BlockTicketResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        blockTicketResponse = response;

        if (blockTicketResponse.getSuccess().equalsIgnoreCase("true") && blockTicketResponse.getData() != null) {
            if (!blockTicketResponse.getData().getPnr().isEmpty()) {
                isTicketBlocked = true;
                /*if (payWithPayTm.isChecked()) {
                    initPreTransaction(blockTicketResponse);
                }*/
                initPreTransaction(blockTicketResponse);
            }
        } else if (blockTicketResponse.getSuccess().equalsIgnoreCase("false")) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(PaymentDetailsActivity.this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }

            if (errMsg.equalsIgnoreCase("null") || errMsg.contains("null")) {
                Toast.makeText(this, "Something went wrong, please try again later", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    private void initPreTransaction(BlockTicketResponse blockTicketResponse) {

        singleTripTicketData.setBlockedSeatPnr(blockTicketResponse.getData().getPnr());

        PreTransactionRequest request = new PreTransactionRequest();
        request.setPnr(blockTicketResponse.getData().getPnr());
        pnrStr = blockTicketResponse.getData().getPnr();
        if (payWithPayTm.isChecked()) {
            request.setGatewayMode("CC");
            request.setPaymentId("paytm");
        } else if (payWithPayPal.isChecked()) {
            request.setGatewayMode("paypal");
            request.setPaymentId("paypal");
        } /*else if (ccavenueBtn.isChecked()){
            request.setGatewayMode("DC");
            request.setPaymentId("ccavenue");
        }*/
        bookTicketPresenter.getPreTransactionData(new PreTransactionUseCase(), request);
    }

    @Override
    public void provideGatewayData(GatewayResponse response) {
        if (response == null)
            return;
        if (response.getSuccess().equalsIgnoreCase("true")) {
            if (response.getData() != null) {
                List<GatewayData> dataList = response.getData();
                for (GatewayData data : dataList) {
                    if (data.getPaymentGatewayProviderCode().equalsIgnoreCase("paytm")) {
                        payWithPayTm.setVisibility(View.VISIBLE);
                        payWithPayTm.setChecked(true);
                    } else if (data.getPaymentGatewayProviderCode().equalsIgnoreCase("paypal")) {
                        payWithPayPal.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else if (!response.getSuccess().equalsIgnoreCase("false")) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void showError(String error) {
        addonsList.clear();
        discountNDCode = null;
        if (!isFinishing()) {

            if (error.contains("Invalid Coupon Code")) {
                discountAmnt = 0;
                discountAlertTextView.setVisibility(View.VISIBLE);
                discountAlertTextView.setText(error);
                discountLayout.setVisibility(View.GONE);
                totalAmnt = (float) singleTripTicketData.getTotalPrice() + ggAmnt - discountAmnt;
//                totalAmntText.setText(String.valueOf(totalAmnt)+" \u20B9");
                totalAmntText.setText(BigDecimal.valueOf(totalAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");

                /*This if loop added to reflect the GST calculations after adding a valid coupon and then the invalid coupon */
                if (totalGstAmnt != 0) {
                    serviceTaxAmnt.setText(BigDecimal.valueOf(singleTripTicketData.getTotalServiceTax()).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
                    totalGstAmnt = singleTripTicketData.getTotalServiceTax();
                }

            }

            Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
            if (error.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void providePreTransactionData(PreTransactionResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.isSuccess()) {
            if ((response.getData() != null && response.getData().getTransactionId() != null) || (response.getPaymentRequestData() != null && response.getPaymentRequestData().getTransactionId() != null)) {
                String transactionId;
                try {
                    transactionId = response.getData().getTransactionId();
                } catch (Exception e) {
                    transactionId = null;
                }
                if (transactionId == null) {
                    transactionId = response.getPaymentRequestData().getTransactionId();
                }

                if (blockTicketResponse.getData().getPnr().equals(transactionId)) {
                    if (payWithPayTm.isChecked() &&
                            response.getData().getGatewayCode().equals("paytm")
                            && response.getData().getGatewayModeDetailCode().equals("CC")) {
                        param = response.getData().getGatewayFormParam();
                        if (param == null)
                            return;
                        try {
                            totalAmntText.setText(param.getTXN_AMOUNT() + " \u20B9");
                            initPayTmTransaction();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (payWithPayPal.isChecked()
                            && response.getPaymentRequestData().getGatewayModeCode().equals("paypal")
                            && response.getPaymentRequestData().getGatewayModeDetailCode().equals("paypal")) {
                        paypalParam = response.getData();
                        if (paypalParam == null)
                            return;
                        try {
                            totalAmntText.setText(paypalParam.getAmount() + " \u20B9");
                            initPayPalTransaction();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }  /*else if (response.getData().getGatewayCode().equals("ccavenue")
                            && response.getData().getGatewayModeDetailCode().equals("DC")
                            && ccavenueBtn.isChecked()) {
                        initCCAvenueTransaction(response.getData().getAmount());
                    }*/
                }
            }
        } else if (response.isError()) {
            Log.d("PreTransactionData", "error");
            Toast.makeText(this, "Transaction failure, try again later", Toast.LENGTH_SHORT).show();
            logCustomBookingEvent(PnrReview.BOOKING_FAILED);
            sendPnrToAdminPortal(PnrReview.BOOKING_FAILED);
            startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    private void initCCAvenueTransaction(float amount) {
        Intent intent = new Intent(this, CCAvenueWebViewActivity.class);
        intent.putExtra(AvenuesParams.ACCESS_CODE, PaymentConstants.accessCode);
        intent.putExtra(AvenuesParams.MERCHANT_ID, PaymentConstants.merchantId);
        intent.putExtra(AvenuesParams.ORDER_ID, blockTicketResponse.getData().getPnr());
        intent.putExtra(AvenuesParams.CURRENCY, "INR");
        intent.putExtra(AvenuesParams.AMOUNT, amount);
        intent.putExtra(AvenuesParams.REDIRECT_URL, PaymentConstants.redirectUrl);
        intent.putExtra(AvenuesParams.CANCEL_URL, PaymentConstants.redirectUrl);
        intent.putExtra(AvenuesParams.RSA_KEY_URL, PaymentConstants.rsaKeyUrl);
        startActivityForResult(intent, CCAVENUE_TX_REQUEST_CODE);
    }

    @Override
    public void showBookTicketResult(BookTicketResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.isSuccess()) {
            if (blockTicketResponse.getData().getPnr().equals(response.getData().getPnr())) {
                Toast.makeText(this, "Ticket Booked successfully", Toast.LENGTH_SHORT).show();
                singleTripTicketData.setBlockedSeatPnr(response.getData().getPnr());
                logPurchaseEvent(singleTripTicketData);
//                logCustomBookingEvent(PnrReview.BOOKED);
                sendPnrToAdminPortal(PnrReview.BOOKED);

                startActivity(new Intent(this, TicketDetailsActivity.class)
                        .putExtra(ParveenDBHelper.PNR, singleTripTicketData.getBlockedSeatPnr())
                        .putExtra(Constants.FROM_ACTIVITY, PaymentDetailsActivity.class.getSimpleName())
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        } else if (!response.isSuccess()) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();

            logCustomBookingEvent(PnrReview.BOOKING_FAILED);
            sendPnrToAdminPortal(PnrReview.BOOKING_FAILED);

            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }

            showPnrFailedAlert();
        }
    }

    private void logPurchaseEvent(BookedSeatModel singleTripTicketData) {
        try {
            BigDecimal itemPrice = new BigDecimal(singleTripTicketData.getTotalPrice(), MathContext.DECIMAL64);
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemId(singleTripTicketData.getBlockedSeatPnr())
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(singleTripTicketData.getFromStationName() + " - " + singleTripTicketData.getToStationName())
                    .putItemPrice(itemPrice)
                    .putCustomAttribute("Seats", singleTripTicketData.getSelectedSeatList().size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logCustomBookingEvent(String status) {
        try {
            /*Answers.getInstance().logCustom(new CustomEvent("Booking status")
                    .putCustomAttribute("PNR", singleTripTicketData.getBlockedSeatPnr())
                    .putCustomAttribute("Status", status)
                    .putCustomAttribute("From", singleTripTicketData.getFromStationName())
                    .putCustomAttribute("To", singleTripTicketData.getToStationName())
                    .putCustomAttribute("Price", singleTripTicketData.getTotalPrice()));*/

            // TODO: Use your own attributes to track content views in your app
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putContentName("Booking status")
                    .putContentType(status)
                    .putContentId(singleTripTicketData.getBlockedSeatPnr())
                    .putCustomAttribute("From", singleTripTicketData.getFromStationName())
                    .putCustomAttribute("To", singleTripTicketData.getToStationName())
                    .putCustomAttribute("Price", singleTripTicketData.getTotalPrice()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receivePaytmInfo(ReceiveResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.getMessage().equalsIgnoreCase("Sucess") && /*!response.isError() &&*/ response.isStatus()) {
            Toast.makeText(PaymentDetailsActivity.this, "Payment Transaction is Successful", Toast.LENGTH_LONG).show();
            initBookTicket();
        } else if (response.getMessage().equalsIgnoreCase("Failure") && !response.isStatus()) {
//            Toast.makeText(this, "error : " + response.getMessage()+" for "+singleTripTicketData.getBlockedSeatPnr(), Toast.LENGTH_SHORT).show();

            if (BOOKING_STATUS != null) {
                if (!BOOKING_STATUS.equals(PnrReview.PAYMENT_CANCELLED)) {
                    showPnrFailedAlert();
                    if (!BOOKING_STATUS.equals(PnrReview.PAYMENT_FAILED)) {
                        BOOKING_STATUS = PnrReview.BOOKING_FAILED;
                        logCustomBookingEvent(PnrReview.BOOKING_FAILED);
                        sendPnrToAdminPortal(PnrReview.BOOKING_FAILED);
                    }
                }

                if (BOOKING_STATUS.equals(PnrReview.PAYMENT_CANCELLED)) {
                    Toast.makeText(this, "Booking cancelled", Toast.LENGTH_SHORT).show();
                }
            }

            startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    private void showPnrFailedAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Your ticket booking failed. Please note the reference number for any queries.\n" + singleTripTicketData.getBlockedSeatPnr());
        builder.setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        if (!isFinishing())
            alertDialog.show();
    }

    @Override
    public void updateValidateCouponUI(ValidateCouponResponse response) {
        discountAlertTextView.setVisibility(View.VISIBLE);
        discountLayout.setVisibility(View.VISIBLE);
        String couponValue = response.getData().getCouponValue();
        if (response.getData().isFlatFlag()) {
            discountAlertTextView.setText(couponValue + " Rs off on basic amount");
            Toast.makeText(this, couponValue + " Rs off on basic amount", Toast.LENGTH_SHORT).show();
            discountAmnt = Double.valueOf(couponValue);
//            discountAmntTextView.setText(" - "+String.valueOf(discountAmnt)+" \u20B9");
            discountAmntTextView.setText(" - " + BigDecimal.valueOf(discountAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
        } else if (!response.getData().isFlatFlag()) {
            discountAlertTextView.setText(couponValue + " % off on basic amount");
            Toast.makeText(this, couponValue + " % off on basic amount", Toast.LENGTH_SHORT).show();
            discountAmnt = basicAmnt * (Double.valueOf(couponValue) / 100);
//            discountAmntTextView.setText(" - "+String.valueOf(discountAmnt)+" \u20B9");
            discountAmntTextView.setText(" - " + BigDecimal.valueOf(discountAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
        }

        double sgst = (basicAmnt - discountAmnt) * (2.5 / 100);    /*2.5 %*/

        /* This If loop is added to make sure the GST is not reflected where there is no GST amount associated the bus
         * Non AC busses have no GST.
         * If we dont have this if loop, GST will simply comeup in display ( users are not charged however )
         */
        if (totalGstAmnt != 0) {
            totalGstAmnt = 2 * sgst;
//        serviceTaxAmnt.setText(totalGstAmnt+" \u20B9");
            serviceTaxAmnt.setText(BigDecimal.valueOf(totalGstAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");
        }
//        singleTripTicketData.setTotalServiceTax(totalGstAmnt);
//        singleTripTicketData.setTotalPrice((basicAmnt-discountAmnt)+totalGstAmnt);
        totalAmnt = (basicAmnt + totalGstAmnt) + ggAmnt - discountAmnt;
//        totalAmntText.setText(String.valueOf(totalAmnt)+" \u20B9" );
        totalAmntText.setText(BigDecimal.valueOf(totalAmnt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + " \u20B9");

        /*allowing only 1 coupon code now*/
        addonsList.clear();
        addonsList.add("ND|" + discountNDCode);
//        addonsList.add("GG|GG");
//        addonsList.add("CD|CD");
//        addonsList.add("FD|FD");
    }

    @Override
    public void paymentGatewayOffers(List<PaymentGatewayOffers> offers) {

        tv_offer_paypal.setVisibility(View.GONE);
        tv_offer_paytm.setVisibility(View.GONE);
        tv_offer_ccavenue.setVisibility(View.GONE);
        tv_offer_paynimo.setVisibility(View.GONE);
        if (offers == null) {
            return;
        }

        for (int i = 0; i < offers.size(); i++) {
            PaymentGatewayOffers temp = offers.get(i);
            if (temp.getPaymentGateway().equalsIgnoreCase("paypal")
                    && temp.getOffer() != null
                    && temp.getOffer().length() > 0) {
                tv_offer_paypal.setVisibility(View.VISIBLE);
                tv_offer_paypal.setText(temp.getOffer());
            } else if (temp.getPaymentGateway().equalsIgnoreCase("paytm")
                    && temp.getOffer() != null
                    && temp.getOffer().length() > 0) {
                tv_offer_paytm.setVisibility(View.VISIBLE);
                tv_offer_paytm.setText(temp.getOffer());
            } else if (temp.getPaymentGateway().equalsIgnoreCase("ccavenue")
                    && temp.getOffer() != null
                    && temp.getOffer().length() > 0) {
                tv_offer_ccavenue.setVisibility(View.VISIBLE);
                tv_offer_ccavenue.setText(temp.getOffer());
            } else if (temp.getPaymentGateway().equalsIgnoreCase("paynimo")
                    && temp.getOffer() != null
                    && temp.getOffer().length() > 0) {
                tv_offer_paynimo.setVisibility(View.VISIBLE);
                tv_offer_paynimo.setText(temp.getOffer());
            }
        }
    }

    private void initPayPalTransaction() {
        String url = paypalParam.getGatewayUrl();
        if (true) {
            startActivityForResult(new Intent(this, PayPalCheckoutActivity.class)
                    .setData(Uri.parse(url))
                    .putExtra(PayPalCheckoutActivity.EXTRA_PNR, pnrStr), PAYPAL_TX_REQUEST_CODE);
            return;
        }
        String packageName = "com.android.chrome";
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        // check if chrome is installed if installed always open in chrome
        // so we can have OneTouch Feature !
        if (Utils.isPackageInstalled(packageName, getApplicationContext())) {
            customTabsIntent.intent.setPackage(packageName);
        }
        customTabsIntent.intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        customTabsIntent.intent.setData(Uri.parse(url));
        startActivityForResult(customTabsIntent.intent, PAYPAL_TX_REQUEST_CODE);
    }

    private void initPayTmTransaction() {
        paytmPGService = null;
//        paytmPGService = PaytmPGService.getStagingService();
        paytmPGService = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put(PaymentConstants.ORDER_ID, /*pnr*/param.getORDER_ID());
        paramMap.put(PaymentConstants.MID, param.getMID());
        paramMap.put(PaymentConstants.CUST_ID, param.getCUST_ID());
        paramMap.put(PaymentConstants.CHANNEL_ID, param.getCHANNEL_ID());
        paramMap.put(PaymentConstants.INDUSTRY_TYPE_ID, param.getINDUSTRY_TYPE_ID());
        paramMap.put(PaymentConstants.WEBSITE, param.getWEBSITE());
        paramMap.put(PaymentConstants.TXN_AMOUNT, param.getTXN_AMOUNT()/*"3"*/);
        paramMap.put(PaymentConstants.REQUEST_TYPE, param.getREQUEST_TYPE());
        paramMap.put(PaymentConstants.EMAIL, param.getEMAIL());
        paramMap.put(PaymentConstants.MOBILE_NO, param.getMOBILE_NO());

//        paramMap.put(PaymentConstants.CALLBACK_URL, Constants.paytm_callback_url);
        paramMap.put(PaymentConstants.CALLBACK_URL, param.getCALLBACK_URL());

        paramMap.put(PaymentConstants.CHECKSUMHASH, param.getCHECKSUMHASH());

        PaytmOrder Order = new PaytmOrder(paramMap);
        paytmPGService.initialize(Order, null);


        paytmPGService.startPaymentTransaction(this, false, false,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.e("someUIErrorOccurred", inErrorMessage);
                    }

                    @Override
                    public void onTransactionResponse(Bundle bundle) {
//                        Log.e("LOG", "Payment Transaction is successful " + bundle);

                        if (bundle != null) {
                            if (BuildConfig.DEBUG) {
                                Log.d("onTransactionResponse", "bundle = " + bundle.toString());
                            }
                            if (isValidPayment(bundle, param)) {
                                PAYTM_ID = bundle.getString(PaytmConstants.TRANSACTION_ID);
                                Log.e("PaymentId by PayTm=", PAYTM_ID);
                                requestReceiveJson();
                            } else {
                                String str = bundle.getString(PaytmConstants.RESPONSE_MSG);
                                Toast.makeText(PaymentDetailsActivity.this, "" + str, Toast.LENGTH_LONG).show();
                                updatePaymentInfo(PnrReview.PAYMENT_FAILED);
                            }
                        }
                    }

                    @Override
                    public void networkNotAvailable() { // If network is not // available, then this // method gets called.
                        Toast.makeText(PaymentDetailsActivity.this, "Check your network connection!!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        Log.e("AuthenticationFailed=", inErrorMessage);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();
                        updatePaymentInfo(PnrReview.PAYMENT_FAILED);
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                        Log.e("onErrorLoadingWebPage=", inErrorMessage + ",url = " + inFailingUrl);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();
                        updatePaymentInfo(PnrReview.PAYMENT_FAILED);
                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        Toast.makeText(PaymentDetailsActivity.this, "Transaction Cancelled", Toast.LENGTH_LONG).show();
                        updatePaymentInfo(PnrReview.PAYMENT_CANCELLED);
                    }

                    @Override
                    public void onTransactionCancel(String s, Bundle bundle) {
                        Log.e("LOG", "Payment Transaction Cancelled " + s);
                        Toast.makeText(PaymentDetailsActivity.this, "Payment Transaction Cancelled, " + s, Toast.LENGTH_LONG).show();
                        paytmPGService = null;
                        updatePaymentInfo(PnrReview.PAYMENT_FAILED);
                    }

                });
    }

    private void requestReceiveJson() {
        if (blockTicketResponse == null || blockTicketResponse.getData() == null || blockTicketResponse.getData().getPnr() == null) {
            Toast.makeText(this, "Booking failed, Please try again later", Toast.LENGTH_SHORT).show();
            Log.d("requestReceiveJson", "pnr null");
            return;
        }

        if (payWithPayPal.isChecked() && paypalQueries != null) {
            ReceiveRequest request = new ReceiveRequest();
            request.setMap(PayPalCheckoutActivity.getQueryMap(paypalQueries));
            bookTicketPresenter.receivePaypalInfo(new ReceiveUseCase(), request);
        } else {
            ReceiveRequest request = new ReceiveRequest();
            request.setPnr(blockTicketResponse.getData().getPnr());
            request.setToken(ParveenApp.getAccessToken());
            bookTicketPresenter.receivePaytmInfo(new ReceiveUseCase(), request);
        }
    }

    private void initBookTicket() {
        BookTicketRequest request = new BookTicketRequest();
        request.setPnr(blockTicketResponse.getData().getPnr());
        bookTicketPresenter.getBookTicketResult(new BookTicketUseCase(), request);
    }

    @Override
    public void showLoading() {
        isLoading = true;
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        isLoading = false;
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    private boolean isValidPayment(Bundle bundle, GatewayFormParam param) {
        return (bundle.getString(PaytmConstants.STATUS).equals(PaymentConstants.TXN_SUCCESS));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("paymentdetail screen", "onDestroy");
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        Utils.getUtils().closeKeyboard(PaymentDetailsActivity.this, view);
                        switch (view.getId()) {
                            case R.id.payBtn:

                                if (!payWithPayTm.isChecked() && !payWithPayPal.isChecked() /*&& !ccavenueBtn.isChecked()*/) {
                                    Toast.makeText(PaymentDetailsActivity.this, "Select payment mode", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                if ((blockTicketResponse == null || blockTicketResponse.getData() == null || blockTicketResponse.getData().getPnr() == null)
                                        && !isTicketBlocked) {
                                    blockTicket();
                                } else {
                                    if (payWithPayTm.isChecked()) {
                                        if (param == null) {
                                            initPreTransaction(blockTicketResponse);
                                        } else {
                                            try {
                                                initPayTmTransaction();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    } else if (payWithPayPal.isChecked()) {
                                        if (param == null) {
                                            initPreTransaction(blockTicketResponse);
                                        } else {
                                            try {
                                                initPayPalTransaction();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }

                                pnrReview.setPrice(String.valueOf(totalAmnt));
                                pnrReview.setBookingDate(Utils.getUtils().getCurrentBookingDate("yyyy-MM-dd HH:mm:ss"));
                                break;

                            case R.id.applyCodeBtn:
                                couponEditText.setError(null);
                                couponEditText.clearFocus();
                                if (couponEditText.getText().toString().trim().isEmpty()) {
                                    couponEditText.setError("Invalid coupon code");
                                    couponEditText.requestFocus();
                                    return;
                                }

                                discountAmnt = 0;
                                discountLayout.setVisibility(View.GONE);
                                discountAlertTextView.setVisibility(View.GONE);
                                validateCoupon(couponEditText.getText().toString().trim());
                                break;
                        }
                    }
                });
    }

    private void validateCoupon(String code) {
        discountNDCode = code;
        ValidateCouponRequest request = new ValidateCouponRequest();
        request.setFromStationCode(singleTripTicketData.getFromStationId());
        request.setToStationCode(singleTripTicketData.getToStationId());
        request.setScheduleCode(singleTripTicketData.getScheduleId());
        request.setTravelDate(singleTripTicketData.getTravelDate());
        request.setUserMobile(singleTripTicketData.getContactNumber());
        request.setCouponCode(code);

        if (bookTicketPresenter == null)
            bookTicketPresenter = new BookTicketPresenterImpl(this);
        bookTicketPresenter.validateCouponCode(new ValidateCodeUseCase(), request);
    }

    private void updatePaymentInfo(String bookingStatus) {
        BOOKING_STATUS = bookingStatus;
        logCustomBookingEvent(bookingStatus);
        sendPnrToAdminPortal(bookingStatus);
        requestReceiveJson();
    }

    private void sendPnrToAdminPortal(String bookingStatus) {
        try {
            pnrReview.setBookingStatus(bookingStatus);
            pnrReview.setPnr(singleTripTicketData.getBlockedSeatPnr());
            ParveenDBHelper parveenDBHelper = ParveenDBHelper.getParveenDBHelper(this);
            parveenDBHelper.addPNRToDb(pnrReview);

            startService(new Intent(this, AdminPanelAPIsService.class)
                    .putExtra(Constants.API_TYPE, Constants.SEND_BOOKING_DATA)
                    .putExtra(Constants.PNR_REVIEW_MODEL, pnrReview));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ccavenueTxStatus == null)
            return;
        if (ccavenueTxStatus.equals("Transaction Successful!")) {
            requestReceiveJson();
        } else {
            requestReceiveJson();
            Toast.makeText(this, "" + ccavenueTxStatus, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CCAVENUE_TX_REQUEST_CODE && requestCode == CCAVENUE_TX_REQUEST_CODE) {
            if (data != null && data.getStringExtra(Constants.CCTXSTATUS) != null) {
                switch (data.getStringExtra(Constants.CCTXSTATUS)) {
                    case SUCCESS:
                        requestReceiveJson();
                        break;
                    case FAILURE:
                        BOOKING_STATUS = PnrReview.PAYMENT_FAILED;
                        updatePaymentInfo(PnrReview.PAYMENT_FAILED);
                        break;
                    case CANCEL:
                        BOOKING_STATUS = PnrReview.PAYMENT_CANCELLED;
                        updatePaymentInfo(PnrReview.PAYMENT_CANCELLED);
                        break;
                }
            }
        } else if (requestCode == PAYPAL_TX_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                paypalQueries = data.getStringExtra(PayPalCheckoutActivity.EXTRA_DATA);
                PAYPAL_TX_DATA = "";
                if (paypalQueries == null && paypalQueries.length() == 0) {
                    updatePaymentInfo(PnrReview.PAYMENT_CANCELLED);
                } else {
                    requestReceiveJson();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.e("paymentExample", "The user canceled.");
                updatePaymentInfo(PnrReview.PAYMENT_CANCELLED);
            } else {
                Log.e("paymentExample", "Unknown error");
                updatePaymentInfo(PnrReview.PAYMENT_FAILED);
            }
        }
    }

    /*disable back function while showing loader*/
    @Override
    public void onBackPressed() {
        if (!isLoading)
            super.onBackPressed();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            return;
        }
        if (buttonView.getId() == payWithPayTm.getId()) {
            payWithPayPal.setChecked(false);
            paynimo_btn.setChecked(false);
            ccavenueBtn.setChecked(false);
        } else if (buttonView.getId() == payWithPayPal.getId()) {
            payWithPayTm.setChecked(false);
            paynimo_btn.setChecked(false);
            ccavenueBtn.setChecked(false);
        } else if (buttonView.getId() == paynimo_btn.getId()) {
            payWithPayTm.setChecked(false);
            payWithPayPal.setChecked(false);
            ccavenueBtn.setChecked(false);
        } else if (buttonView.getId() == ccavenueBtn.getId()) {
            payWithPayTm.setChecked(false);
            paynimo_btn.setChecked(false);
            payWithPayPal.setChecked(false);
        }
    }
}
