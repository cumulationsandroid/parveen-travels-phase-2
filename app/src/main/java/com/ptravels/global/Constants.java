package com.ptravels.global;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Constants {
    public final static String BASE_URL_IP = "http://192.168.0.118:8080/parveen-travels/api/app/";
    public final static String BASE_URL = "http://139.59.30.0:8080/parveen-travels/api/app/";
    public final static String PRODUCTION_BASE_URL = "https://parveentravels.com/parveen-travels/api/app/";
    public static final String FIRE_BASE_URL = "https://fir-parveentravels.firebaseio.com/";
    public static final String DEMO_BASE_URL = "http://json-generator.appspot.com/api/json/get/";
    public final static String ADMIN_PANEL_BASE_URL = "http://35.165.93.101/parveen-travels/";
    public final static String ADMIN_PANEL_PROD_URL = "http://mobileapp.parveentravels.com/api/";
    public static final String HOME_IMAGE_DETAILS = "http://35.165.93.101/parveen-travels/app/home-image";
    public static final String FROM_STATION_ID = "fromStationId";
    public static final String TO_STATION_ID = "toStationId";
    public static final String TRAVEL_DATE = "travelDate";
    public static final String EMAIL_ID = "emailId";
    public static final String USER_PREF = "userPref";
    public static final String FNAME = "fname";
    public static final String USERNAME = "username";
    public static final String MOBILE = "mobile";
    public static final String PWD = "pwd";
    public static final String ENABLED = "enabled";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String USER_DATA = "userData";
    public static final String FROM_STATION_NAME = "fromStationName";
    public static final String TO_STATION_NAME = "toStationName";

    public static final String PAYPAL_REDIRECT_URL_RELEASE = "https://www.parveentravels.com/parveen-travels/api/app/paypal/receive";
    public static final String PAYPAL_REDIRECT_URL_DEBUG = "http://139.59.30.0:8080/parveen-travels/api/app/paypal/receive";

    public static final String TIME_DETAILS = "timeline_details";

    public static final String GUEST_USER_EMAIL = "guest@parveentravels.com";
    public static final String GUEST_USER_PASSWORD = "123456";
    //    public static final String GUEST_USER_EMAIL = "amit.test@test.com";
//    public static final String GUEST_USER_PASSWORD = "password1";
    public static final String FILTERS_LIST = "filtersList";
    public static final String TICKET_BOOKED = "BOOKED";
    public static final String TICKET_CANCELLED = "CANCELLED";
    public static final String TICKET_COMPLETED = "COMPLETED";
    public static final String MY_BOOKING_DATA = "my_booking_data";
    public static final String STATION_ID = "stationID";
    public static final String SCHEDULE_ID = "scheduleId";
    public static final String STEERING = "steering";
    public static final String SEATMAP_MODEL = "seatMapModel";
    public static final String BOOKED_SEAT_DATA = "booked_seat_data";
    public static final String BUS_TYPE = "bus_type";

    public static final String DEFAULT_PASSWORD = "123456";
    public static final String TICKET_PREF = "ticketPref";
    public static final String SINGLE_TRIP_TICKET = "singleTripTicket";

    public static final String PRE_SELECTED = "isStationPreSelected";
    public static final String paytm_callback_url = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
//    public static final String paytm_production_callback_url = "https://secure.paytm.in/paytmchecksum/paytmCallback.jsp";

    public static final String paytm_production_callback_url = "https://securegw.paytm.in/theia/paytmCallback?";
    public static final String paytm_production_callback_url_2 = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    public static final String NO_INTERNET = "Please check your internet";
    public static final String NETWORK_ERROR="Network error. please try again later";
    public static final String GUEST_USER_LOGIN = "GUEST_USER_LOGIN";
    public static final String SIGN_IN_DATA = "sign_in_data";
    public static final String DEFAULT_MOBILE_NUMBER = "9999999999";

    public static final String HOME_SCREEN_IMAGE = "https://www.parveentravels.com/parveen-travels/assets/parveen/images/new/home/02_02.jpg";
    public static final String NETWORK_CHANGED = "networkChanged";
    public static final String BUS_STATION_RESPONSE="bus_station_response";
    public static final String LOAD_FRAGMENT = "loadFragment";
    public static final String UNKNOWN_ERROR="Something went wrong! Please Try again later";
    public static final String HISTORY_TICKET = "historyTicket";
    public static final String HOME_IMAGE_PATH = "homeImagePath";
    public static final String HOME_IMAGE_VERSION = "homeImageVersion";
    public static final String API_TYPE = "apiType";
    public static final String SEND_BOOKING_DATA = "sendBookData";
    public static final String PNR_REVIEW_MODEL = "pnrReviewModel";
    public static final String SEND_DB_BOOKING_DATA = "sendDbBookingData";
    public static final String UPDATE_BOOKING_DATA = "updateBooking";
    public static final String FCM_TOKEN = "fcmToken";
    public static final String SEND_TOKEN_TO_SERVER = "sendToken";
    public static final String FCM_DATA = "fcmData";
    public static final String JTRACK_URL = "http://jtrack.in:1832/parveenintercity?api=22acfe38a3d125e0323e51d4be0c7756";
    public static final String FROM_ACTIVITY = "fromActivity";
    public static final String SHOW_CANCEL_BTN = "showCancelBtn";

    public static final String GOOGLE_DIRECTIONS_URL = "https://maps.googleapis.com/maps/";
    public static final String CCTXSTATUS = "cctxstatus";
}
