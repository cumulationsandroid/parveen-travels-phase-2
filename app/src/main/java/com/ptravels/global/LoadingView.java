package com.ptravels.global;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface LoadingView {
    void showLoading();
    void hideLoading();
}
