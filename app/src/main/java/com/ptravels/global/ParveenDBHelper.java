package com.ptravels.global;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.ptravels.ParveenApp;
import com.ptravels.adminPanel.data.model.request.SendBookingRequest;
import com.ptravels.myBooking.data.model.PnrReview;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by AmitT on 07-06-2017.
 */

public class ParveenDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ParveenDb.db";
    private static final String TABLE_REVIEWS = "Reviews";

    //    private static final String KEY_ID = "id";
    public static final String PNR = "PNR";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String FROM_CITY = "frmCity";
    private static final String TO_CITY = "toCity";
    private static final String TRAVEL_DATE = "travelDate";
    private static final String BOOKING_DATE = "bookingDate";
    private static final String USER_EMAIL = "email";
    private static final String USER_MOBILE = "mobile";
    public static final String BOOKING_STATUS = "bookStatus";
    private static final String REVIEW_STATUS = "reviewStatus";
    private static final String SERVER_PRESENT = "serverPresent";
    private static final String PRICE = "price";
    private static final String SEATS = "seats";
    private static final String SERVER_BOOKING_ID = "bookingId";

    private SQLiteDatabase parveenSqLiteDatabase;
    private static ParveenDBHelper parveenDBHelper;

    private ParveenDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        parveenSqLiteDatabase = this.getWritableDatabase();
    }

    public static ParveenDBHelper getParveenDBHelper(Context context){
        if (parveenDBHelper == null)
            parveenDBHelper = new ParveenDBHelper(context);

        return parveenDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_REVIEWS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_REVIEWS
                + "("
                + PNR + " TEXT PRIMARY KEY," /*comma is important*/
                + ACCESS_TOKEN + " TEXT,"
                + FROM_CITY + " TEXT,"
                + TO_CITY + " TEXT,"
                + TRAVEL_DATE + " TEXT,"
                + BOOKING_DATE + " TEXT,"
                + USER_EMAIL + " TEXT,"
                + USER_MOBILE + " TEXT,"
                + BOOKING_STATUS + " TEXT,"
                + SERVER_PRESENT + " TEXT,"
                + REVIEW_STATUS + " TEXT,"
                + PRICE + " TEXT,"
                + SEATS + " TEXT,"
                + SERVER_BOOKING_ID + " TEXT"
                + ")";

        sqLiteDatabase.execSQL(CREATE_REVIEWS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_REVIEWS);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public int getParveenTableCount() {
        Cursor c = parveenSqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_REVIEWS, null);
        return c.getCount();
    }

    public void addPNRToDb(PnrReview pnrReview) {
        ContentValues values = new ContentValues();
        values.put(PNR, pnrReview.getPnr());
        values.put(ACCESS_TOKEN, pnrReview.getAccessToken());
        values.put(FROM_CITY, pnrReview.getFrmCity());
        values.put(TO_CITY, pnrReview.getToCity());
        values.put(TRAVEL_DATE, pnrReview.getTravelDate());
        values.put(BOOKING_DATE, pnrReview.getBookingDate());
        values.put(USER_EMAIL, pnrReview.getEmail());
        values.put(USER_MOBILE, pnrReview.getMobile());
        values.put(BOOKING_STATUS, pnrReview.getBookingStatus());
        values.put(PRICE, pnrReview.getPrice());
        values.put(SEATS, pnrReview.getSeats());
        values.put(SERVER_PRESENT, pnrReview.isServerPresent());
        values.put(REVIEW_STATUS, pnrReview.getReviewStatus());
//        values.put(SERVER_BOOKING_ID, pnrReview.getBookingId());

        // Inserting or Updating Row
        /*Note : SQLiteDatabase.CONFLICT_IGNORE comment is supposed to function the same way as you had observed.
                While trying to insert, if there is no conflicting row, then it will insert a new row with the given
                values and return the id of the newly inserted row. On the other hand if there is already a conflicting
                row (with same unique key), then the incoming values will be ignored and the existing row will be retained
                and then the return value will be -1 to indicate a conflict scenario.
                To know further details and to understand how to handle this return value
                * */
        int id = (int) parveenSqLiteDatabase.insertWithOnConflict(TABLE_REVIEWS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            Log.d("addPNRToDb","PNR already exist "+pnrReview.getPnr());
//            parveenSqLiteDatabase.update(TABLE_REVIEWS, values, PNR + "=?", new String[]{pnrReview.getPnr()});
        }
    }

    public void updatePnrSentStatusToDb(String pnr,String bookingId) {
        ContentValues values = new ContentValues();
        values.put(SERVER_PRESENT, true);
        values.put(SERVER_BOOKING_ID, bookingId);
        int status = parveenSqLiteDatabase.update(TABLE_REVIEWS, values, PNR + "=?", new String[]{pnr});
        Log.d("updatePnrBookingStatus",""+status);
    }

    public void updatePnrReviewStatusToDb(String pnr,String status) {
        ContentValues values = new ContentValues();
        values.put(REVIEW_STATUS, status);
        parveenSqLiteDatabase.update(TABLE_REVIEWS, values, PNR + "=?", new String[]{pnr});
    }

    public void updatePnrBookingStatusToDb(String pnr,String status) {
        ContentValues values = new ContentValues();
        values.put(BOOKING_STATUS, status);
        parveenSqLiteDatabase.update(TABLE_REVIEWS, values, PNR + "=?", new String[]{pnr});
    }

    public List<SendBookingRequest> getServerPendingPNRs(){
        List<SendBookingRequest> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor!=null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    if (cursor.getString(cursor.getColumnIndex(SERVER_PRESENT))!=null
                            && !cursor.getString(cursor.getColumnIndex(SERVER_PRESENT)).isEmpty()
                            && cursor.getString(cursor.getColumnIndex(SERVER_PRESENT)).equalsIgnoreCase("0"/*false*/)) {
                        SendBookingRequest request = new SendBookingRequest();
                        request.setPnr(cursor.getString(cursor.getColumnIndex(PNR)));
                        request.setFrom_city(cursor.getString(cursor.getColumnIndex(FROM_CITY)));
                        request.setTo_city(cursor.getString(cursor.getColumnIndex(TO_CITY)));
                        request.setTravel_date(cursor.getString(cursor.getColumnIndex(TRAVEL_DATE)));
                        if (cursor.getString(cursor.getColumnIndex(BOOKING_DATE)) != null)
                            request.setBooking_date(cursor.getString(cursor.getColumnIndex(BOOKING_DATE)));
                        else
                            request.setBooking_date(cursor.getString(cursor.getColumnIndex(TRAVEL_DATE)));
                        request.setBooking_status(cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)));
                        request.setPrice(cursor.getString(cursor.getColumnIndex(PRICE)));
                        request.setSeats(cursor.getString(cursor.getColumnIndex(SEATS)));
                        request.setMobile_os("Android");
                        request.setDevice_id(ParveenApp.getMacAddress());
                        list.add(request);
                    }
                } while (cursor.moveToNext());
            }

            if (!cursor.isClosed())
                cursor.close();
        }

        return list;
    }

    public List<PnrReview> getCompletedReviewPendingPNRs(){
        List<PnrReview> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_REVIEWS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor!=null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    String trlDate = cursor.getString(cursor.getColumnIndex(TRAVEL_DATE));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    try {
                        Date pnrDate = sdf.parse(trlDate);
                        Date currdate = Calendar.getInstance().getTime();

                        /*For demo purpose*/
                        long hrsInMillis = (12)*60*60*1000;
                        long lesserDiff = pnrDate.getTime() - currdate.getTime();
                        long greaterDiff = currdate.getTime() - pnrDate.getTime();

                        /*if ((pnrDate.compareTo(currdate) < 0 || diff <= hrsInMillis)
                                && cursor.getString(cursor.getColumnIndex(REVIEW_STATUS)).equals(PnrReview.PENDING)
                                && (cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)).equalsIgnoreCase(PnrReview.BOOKED)
                                || cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)).equalsIgnoreCase(PnrReview.COMPLETED))) {*/
                        if ((pnrDate.compareTo(currdate) < 0 && greaterDiff >= hrsInMillis)
                                && cursor.getString(cursor.getColumnIndex(REVIEW_STATUS)).equals(PnrReview.PENDING)
                                && (cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)).equalsIgnoreCase(PnrReview.BOOKED)
                                || cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)).equalsIgnoreCase(PnrReview.COMPLETED))) {
                            PnrReview pnrReview = new PnrReview();
                            pnrReview.setPnr(cursor.getString(cursor.getColumnIndex(PNR)));
                            pnrReview.setFrmCity(cursor.getString(cursor.getColumnIndex(FROM_CITY)));
                            pnrReview.setToCity(cursor.getString(cursor.getColumnIndex(TO_CITY)));
                            pnrReview.setTravelDate(cursor.getString(cursor.getColumnIndex(TRAVEL_DATE)));
                            pnrReview.setBookingDate(cursor.getString(cursor.getColumnIndex(BOOKING_DATE)));
                            pnrReview.setBookingStatus(cursor.getString(cursor.getColumnIndex(BOOKING_STATUS)));
                            pnrReview.setPrice(cursor.getString(cursor.getColumnIndex(PRICE)));
                            pnrReview.setSeats(cursor.getString(cursor.getColumnIndex(SEATS)));
                            pnrReview.setReviewStatus(cursor.getString(cursor.getColumnIndex(REVIEW_STATUS)));
                            list.add(pnrReview);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (cursor.moveToNext());
            }

            if (!cursor.isClosed())
                cursor.close();
        }

        return list;
    }

    public boolean isPnrServerPresent(String pnr){
        String[] columnsToCheck = {
                SERVER_PRESENT
        };

        String whereClause = PNR + " = ?";
        String[] whereArgs = {pnr};

        Cursor cursor = parveenSqLiteDatabase.query(
                TABLE_REVIEWS,
                columnsToCheck,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    String serverPresent = cursor.getString(cursor.getColumnIndex(SERVER_PRESENT));
                    Log.d("ParveenDBHelper", "serverPresent: " + serverPresent);
                    return serverPresent != null && serverPresent.equals("1");
                }
            }
            if (!cursor.isClosed())
                cursor.close();
        }
        return false;
    }

    public void closeParveenDb() {
        if (parveenSqLiteDatabase != null && parveenSqLiteDatabase.isOpen())
            parveenSqLiteDatabase.close();
    }
}
