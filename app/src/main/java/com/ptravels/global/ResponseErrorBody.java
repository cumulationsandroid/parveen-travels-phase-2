package com.ptravels.global;

/**
 * Created by Amit Tumkur on 12-01-2018.
 */

public class ResponseErrorBody {
    private boolean success;
    private ResponseError error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }
}
