package com.ptravels.global;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.signup.data.model.SignupRequest;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public abstract class UseCase<RequestObj,ResponseOnj> {
    public abstract Single<ResponseOnj> buildUseCase(RequestObj obj);

    public Single<ResponseOnj> execute(RequestObj requestObj) {
        return buildUseCase(requestObj);
    }
}
