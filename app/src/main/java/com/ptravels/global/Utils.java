package com.ptravels.global;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;


import com.ptravels.ParveenApp;
import com.ptravels.R;
import com.ptravels.stationsList.data.model.response.Station;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.ResponseBody;

/**
 * Created by Anirudh Uppunda on 11/5/17.
 */

public class Utils {
    private static Utils utils;
    public HashMap<String, String> fiveToElevenMap = new HashMap<>();
    public HashMap<String, String> elevenToFiveMap = new HashMap<>();
    private AlertDialog enableNetworkDialog;
    private String macAddress;
    private HashMap<String,String> stationsMap;

    private Utils() {
    }

    public static Utils getUtils() {
        if (utils == null)
            utils = new Utils();
        return utils;
    }

    public static boolean isPackageInstalled(String packageName,Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("firing ", "Fire in the hole");
            return false;
        }
    }

    public void initDeptMaps() {
        fiveToElevenMap.put("05", "AM");
        fiveToElevenMap.put("06", "AM");
        fiveToElevenMap.put("07", "AM");
        fiveToElevenMap.put("08", "AM");
        fiveToElevenMap.put("09", "AM");
        fiveToElevenMap.put("10", "AM");
        fiveToElevenMap.put("11", "AM");

        elevenToFiveMap.put("11", "PM");
        elevenToFiveMap.put("12", "PM");
        elevenToFiveMap.put("00", "AM");
        elevenToFiveMap.put("01", "PM");
        elevenToFiveMap.put("02", "PM");
        elevenToFiveMap.put("03", "PM");
        elevenToFiveMap.put("04", "PM");
        elevenToFiveMap.put("05", "PM");
    }

    public static boolean isOnline() {
        return isOnlineUsingCOntext();
    }

    public static boolean isOnlineUsingCOntext() {
        ConnectivityManager cm = (ConnectivityManager) ParveenApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

    public List<String> getNext5Days(String currentDate, int numDays) {

        List<String> next5Days = new ArrayList<>();
        if (currentDate != null) {
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat returnDateFormat = new SimpleDateFormat("EEE, dd MMM", Locale.getDefault());
            SimpleDateFormat TodayTomFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

            Calendar todayCalendar = Calendar.getInstance();

            String today = currentDateFormat.format(todayCalendar.getTime());
            todayCalendar.add(Calendar.DATE, 1);
            String tomorrow = currentDateFormat.format(todayCalendar.getTime());
//          String dt = "2008-12-11";  // currentDate
            for (int i = 0; i < numDays; i++) {
            /*Fri, 22 Nov*/
                Calendar c = Calendar.getInstance();


                try {
                    c.setTime(currentDateFormat.parse(currentDate));
                    c.add(Calendar.DATE, i);  // number of days to add

                    String day;

                    if (today.equalsIgnoreCase(currentDateFormat.format(c.getTime()))) {
                        day = "Today, " + TodayTomFormat.format(c.getTime());
                    } else if (tomorrow.equalsIgnoreCase(currentDateFormat.format(c.getTime()))) {
                        day = "Tom, " + TodayTomFormat.format(c.getTime());
                    } else {
                        day = returnDateFormat.format(c.getTime());

                    }

                    Log.i("getNext5Days", day);
                    next5Days.add(day);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return next5Days;
    }

    public List<String> getNext5DaysForIntent(String currentDate, int numDays) {
        List<String> next5Days = new ArrayList<>();
//          String dt = "2008-12-11";  // currentDate
        for (int i = 0; i < numDays; i++) {
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(currentDateFormat.parse(currentDate));
                c.add(Calendar.DATE, i);  // number of days to add

                String day = currentDateFormat.format(c.getTime());
//                currentDate = day;
                Log.i("getNext5DaysForIntent", day);
                next5Days.add(day);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return next5Days;
    }

    public String getCurrentDateSelected() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public int toPixels(Context context, int dp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public boolean isValidPhoneNumber(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    public void showLoader(ImageView loadingBar, TextView loadingText, Context context) {
        loadingBar.setVisibility(View.VISIBLE);
        if (loadingText != null)
            loadingText.setVisibility(View.VISIBLE);
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        rotation.setFillAfter(true);
        loadingBar.startAnimation(rotation);
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        appCompatActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void closeLoader(ImageView loadingBar, TextView loadingText, Context context) {
        loadingBar.setVisibility(View.GONE);
        if (loadingText != null)
            loadingText.setVisibility(View.GONE);
        loadingBar.clearAnimation();
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        appCompatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    static public String formatDateString(String initialFormat, String finalFormat, String dateString) {
        try {
            SimpleDateFormat initialDateFormat = new SimpleDateFormat(initialFormat);
            Date date = initialDateFormat.parse(dateString);
            SimpleDateFormat updatedDateFormat = new SimpleDateFormat(finalFormat);
            return updatedDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getDeptTimeInMillis(String travelDate, String travelDateFormat,
                                           String deptTime, String deptTimeFormat) {

        String toParse = travelDate + " " + deptTime; // Results in "22/06/2016 10:12 AM"
        SimpleDateFormat formatter = new SimpleDateFormat(travelDateFormat + " " + deptTimeFormat, Locale.US); // I assume d-M, you may refer to M-d for month-day instead.
        Date date = null;
        long millis = 0;
        try {
            date = formatter.parse(toParse);
            millis = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        System.out.println("Today Date Is ==" + date + "");
//        System.out.println("Today Date Is millis==" + millis + "");
        return millis;
    }

    public String initOrderId() {
        Random r = new Random(System.currentTimeMillis());
        String orderId = "ORDER" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000) + System.currentTimeMillis() + "";
        return orderId;
    }

    public void closeKeyboard(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((view == null) ? null : view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public static void printHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.ptravels",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public long getArrivalTimeMillis(String travelDate, String travelDateFormat,
                                     String arrivalTime, String arrivalFormat,
                                     String departureTime) {

        long millis = 0;
        try {
            String newTravelDate = travelDate;
            if ((arrivalTime.contains("am") || arrivalTime.contains("AM"))
                    && (!(departureTime.contains("am") || departureTime.contains("AM")))) {
                // travelDate = 2018-01-12
                String[] strings = travelDate.split("-");
                newTravelDate = strings[0] + "-" + strings[1] + "-" + (Integer.valueOf(strings[2]) + 1);
            }

            String toParse = newTravelDate + " " + arrivalTime; // Results in "2-5-2012 20:43"
            SimpleDateFormat formatter = new SimpleDateFormat(travelDateFormat + " " + arrivalFormat, Locale.getDefault()); // I assume d-M, you may refer to M-d for month-day instead.
            Date date = null;
            try {
                date = formatter.parse(toParse);
                millis = date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Today Date Is millis==" + millis + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return millis;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public void showEnableInternetDialog(final Context context) {
        /*Note : Use android.support.v7.app.AlertDialog to fix
        *java.lang.IllegalStateException: You need to use a Theme.AppCompat theme (or descendant) with this activity
        * */
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(context.getString(R.string.title));

        // set dialog message
        alertDialogBuilder
                .setMessage(context.getString(R.string.enableNet))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        enableNetworkDialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent);

                    }
                })
                /*.setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        enableNetworkDialog.dismiss();
//                        ((AppCompatActivity) context).finish();
                    }
                })*/;

        // create alert dialog
        if (enableNetworkDialog != null && enableNetworkDialog.isShowing())
            enableNetworkDialog.dismiss();
        enableNetworkDialog = alertDialogBuilder.create();
        if (context instanceof AppCompatActivity && !((AppCompatActivity) context).isFinishing())
            enableNetworkDialog.show();
    }

    public void dismissNetworkDialog() {
        if (enableNetworkDialog != null && enableNetworkDialog.isShowing())
            enableNetworkDialog.dismiss();
    }

    public boolean saveImageLocal(ResponseBody responseBody, String url) {
        Log.d("saveImageLocal", "Got the body for the image");
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
//          File songFolder = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/SirenaGaana/" + gaanaSongObject.getTrackId());
            File imageFolder = new File(ParveenApp.getContext().getFilesDir() + "/Images");
            if (!imageFolder.exists())
                imageFolder.mkdirs();

            String imageName = url.substring(url.lastIndexOf("/") + 1);
            File imageFile = new File(imageFolder, "Home_" + imageName);
            Log.d("saveImageLocal", "File = " + imageFile);
            if (!imageFile.exists())
                imageFile.createNewFile();

            Log.d("saveImageLocal", "File Size=" + responseBody.contentLength());

            inputStream = responseBody.byteStream();
            fileOutputStream = new FileOutputStream(imageFile);
            byte data[] = new byte[4096];
            int count;
            long progress = 0;
            while ((count = inputStream.read(data)) != -1) {
                fileOutputStream.write(data, 0, count);
                progress += count;
                Log.d("saveImageLocal", "Progress: " + progress + "/" + responseBody.contentLength() + " >> " + (float) progress / responseBody.contentLength());
            }
            fileOutputStream.flush();
            Log.d("saveImageLocal", "Home image uri = " + imageFile.getAbsolutePath());
            SharedPrefUtils.getSharedPrefUtils().saveHomeImageFilePath(ParveenApp.getContext(), imageFile.getAbsolutePath());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("saveImageLocal", "Failed to save the file!");
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
                if (fileOutputStream != null)
                    fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public String getCurrentBookingDate(String format) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public String getMacAddr() {
        if (macAddress != null && !macAddress.isEmpty())
            return macAddress;

        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    // res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                macAddress = res1.toString();
                return macAddress;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public Bitmap getScaledMarkerBitmap(Context context, int drawable, int height, int width) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(drawable);
        Bitmap b = bitmapDrawable.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isNotificationChannelCreated(Context context, @NonNull String channelId) {
        if (!TextUtils.isEmpty(channelId)) {
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = manager.getNotificationChannel(channelId);
            return channel!=null;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isNotificationChannelEnabled(Context context, @NonNull String channelId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!TextUtils.isEmpty(channelId)) {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel channel = manager.getNotificationChannel(channelId);
                return channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
            }
            return false;
        } else {
            return NotificationManagerCompat.from(context).areNotificationsEnabled();
        }
    }

    public HashMap<String,String> getStationsMap(Context context){
        if (stationsMap!=null && stationsMap.size()>0)
            return stationsMap;

        if (stationsMap == null)
            stationsMap = new HashMap<>();

        StationsListResponse response = SharedPrefUtils.getSharedPrefUtils().getBusStation(context);
        if (response!=null){
            for (Station station : response.getData().getStations()){
                stationsMap.put(station.getId(),station.getName());
            }
        }

        return stationsMap;
    }

    public String getStationName(Context context,String stationId){
        HashMap<String,String> map = getStationsMap(context);
        return map.get(stationId);
    }
}
