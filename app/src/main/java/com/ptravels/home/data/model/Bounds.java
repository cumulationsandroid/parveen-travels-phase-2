package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Bounds {

    private Northeast northeast;
    private Southwest southwest;

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }
}
