package com.ptravels.home.data.model;

import java.util.List;

import okhttp3.Route;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Directions {
    private List<GeocodedWaypoint> geocodedWaypoints = null;
    private List<com.ptravels.home.data.model.Route> routes = null;
    private String status;

    public List<GeocodedWaypoint> getGeocodedWaypoints() {
        return geocodedWaypoints;
    }

    public void setGeocodedWaypoints(List<GeocodedWaypoint> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
    }

    public List<com.ptravels.home.data.model.Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<com.ptravels.home.data.model.Route> routes) {
        this.routes = routes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
