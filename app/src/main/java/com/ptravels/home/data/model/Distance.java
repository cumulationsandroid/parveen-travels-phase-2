package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Distance {
    private String text;
    private Integer value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
