package com.ptravels.home.data.model;

import java.util.List;

public class FeedbackOptionsResponse
{
    private String status;

    private List<FeedbackRecords> records;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<FeedbackRecords> getRecords ()
    {
        return records;
    }

    public void setRecords (List<FeedbackRecords> records)
    {
        this.records = records;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", records = "+records+"]";
    }
}