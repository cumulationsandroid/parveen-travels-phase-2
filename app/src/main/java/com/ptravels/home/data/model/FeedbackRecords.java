package com.ptravels.home.data.model;

/**
 * Created by Amit Tumkur on 22-02-2018.
 */

public class FeedbackRecords {
    private String feedback_name;

    private String feedback_id;

    public String getFeedback_name() {
        return feedback_name;
    }

    public void setFeedback_name(String feedback_name) {
        this.feedback_name = feedback_name;
    }

    public String getFeedback_id() {
        return feedback_id;
    }

    public void setFeedback_id(String feedback_id) {
        this.feedback_id = feedback_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [feedback_name = " + feedback_name + ", feedback_id = " + feedback_id + "]";
    }
}
