package com.ptravels.home.data.model;

/**
 * Created by Amit Tumkur on 22-02-2018.
 */

public class FeedbackResponseRecords {
    private String review_id;

    public String getReview_id ()
    {
        return review_id;
    }

    public void setReview_id (String review_id)
    {
        this.review_id = review_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [review_id = "+review_id+"]";
    }
}
