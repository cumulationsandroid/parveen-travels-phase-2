package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class GeocodedWaypoint {
    private String geocoderStatus;
    private String placeId;
    private List<String> types = null;

    public String getGeocoderStatus() {
        return geocoderStatus;
    }

    public void setGeocoderStatus(String geocoderStatus) {
        this.geocoderStatus = geocoderStatus;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}
