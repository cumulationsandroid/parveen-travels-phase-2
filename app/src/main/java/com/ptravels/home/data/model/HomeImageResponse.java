package com.ptravels.home.data.model;

public class HomeImageResponse{
	private Records records;
	private String status;

	public void setRecords(Records records){
		this.records = records;
	}

	public Records getRecords(){
		return records;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}
