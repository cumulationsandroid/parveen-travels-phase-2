package com.ptravels.home.data.model;

/**
 * Created by Amit Tumkur on 07-03-2018.
 */

public class LiveTrackData {
    private String created_date;

    private String lon_message;

    private String distance_travelled;

    private String vehicle_number;

    private String speed;

    private String lat_message;

    private String pre_lon_message;

    private String pre_lat_message;

    private String device_id;

    private String io_state;

    private String vehicle_id;

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getLon_message() {
        return lon_message;
    }

    public void setLon_message(String lon_message) {
        this.lon_message = lon_message;
    }

    public String getDistance_travelled() {
        return distance_travelled;
    }

    public void setDistance_travelled(String distance_travelled) {
        this.distance_travelled = distance_travelled;
    }

    public String getVehicle_number() {
        return vehicle_number;
    }

    public void setVehicle_number(String vehicle_number) {
        this.vehicle_number = vehicle_number;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getLat_message() {
        return lat_message;
    }

    public void setLat_message(String lat_message) {
        this.lat_message = lat_message;
    }

    public String getPre_lon_message() {
        return pre_lon_message;
    }

    public void setPre_lon_message(String pre_lon_message) {
        this.pre_lon_message = pre_lon_message;
    }

    public String getPre_lat_message() {
        return pre_lat_message;
    }

    public void setPre_lat_message(String pre_lat_message) {
        this.pre_lat_message = pre_lat_message;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getIo_state() {
        return io_state;
    }

    public void setIo_state(String io_state) {
        this.io_state = io_state;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [created_date = " + created_date + ", lon_message = " + lon_message + ", distance_travelled = " + distance_travelled + ", vehicle_number = " + vehicle_number + ", speed = " + speed + ", lat_message = " + lat_message + ", pre_lon_message = " + pre_lon_message + ", pre_lat_message = " + pre_lat_message + ", device_id = " + device_id + ", io_state = " + io_state + ", vehicle_id = " + vehicle_id + "]";
    }
}
