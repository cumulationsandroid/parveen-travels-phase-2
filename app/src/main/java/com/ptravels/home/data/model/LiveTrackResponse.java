package com.ptravels.home.data.model;

import java.util.List;

public class LiveTrackResponse
{
    private String message;

    private int status;

    private List<LiveTrackData> data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public int getStatus ()
    {
        return status;
    }

    public void setStatus (int status)
    {
        this.status = status;
    }

    public List<LiveTrackData> getData ()
    {
        return data;
    }

    public void setData (List<LiveTrackData> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }
}