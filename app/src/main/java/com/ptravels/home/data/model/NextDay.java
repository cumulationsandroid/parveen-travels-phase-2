package com.ptravels.home.data.model;

/**
 * Created by Amit Tumkur on 11-12-2017.
 */

public class NextDay {
    private boolean isSelected;
    private String nextDayText;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getNextDayText() {
        return nextDayText;
    }

    public void setNextDayText(String nextDayText) {
        this.nextDayText = nextDayText;
    }
}
