package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Northeast {
    private Double lat;
    private Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

}
