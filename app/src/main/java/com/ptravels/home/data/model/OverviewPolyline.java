package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class OverviewPolyline {
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
