package com.ptravels.home.data.model;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Polyline {
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
