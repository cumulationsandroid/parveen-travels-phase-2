package com.ptravels.home.data.model;

public class Records{
	private String updated_on;
	private String img_url;
	private String version_id;
	private String added_on;

	public void setUpdated_on(String updated_on){
		this.updated_on = updated_on;
	}

	public String getUpdated_on(){
		return updated_on;
	}

	public void setImg_url(String img_url){
		this.img_url = img_url;
	}

	public String getImg_url(){
		return img_url;
	}

	public void setVersion_id(String version_id){
		this.version_id = version_id;
	}

	public String getVersion_id(){
		return version_id;
	}

	public void setAdded_on(String added_on){
		this.added_on = added_on;
	}

	public String getAdded_on(){
		return added_on;
	}
}
