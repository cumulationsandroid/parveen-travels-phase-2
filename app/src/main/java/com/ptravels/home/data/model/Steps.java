package com.ptravels.home.data.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class Steps {
    private Distance distance;
    private Duration duration;
    private EndLocation end_location;
    private String htmlInstructions;
    private Polyline polyline;
    private StartLocation start_location;
    private String travelMode;
    private String maneuver;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public EndLocation getEndLocation() {
        return end_location;
    }

    public void setEndLocation(EndLocation endLocation) {
        this.end_location = endLocation;
    }

    public String getHtmlInstructions() {
        return htmlInstructions;
    }

    public void setHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    public StartLocation getStartLocation() {
        return start_location;
    }

    public void setStartLocation(StartLocation startLocation) {
        this.start_location = startLocation;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public String getManeuver() {
        return maneuver;
    }

    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }
}
