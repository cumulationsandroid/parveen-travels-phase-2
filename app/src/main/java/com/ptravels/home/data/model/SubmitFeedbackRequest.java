package com.ptravels.home.data.model;

import java.util.List;

public class SubmitFeedbackRequest {
    private String pnr;

    private List<Feedback_id> feedback_id;

    private String rating;

    private String comment;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<Feedback_id> getFeedback_id() {
        return feedback_id;
    }

    public void setFeedback_id(List<Feedback_id> feedback_id) {
        this.feedback_id = feedback_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ClassPojo [pnr = " + pnr + ", feedback_id = " + feedback_id + ", rating = " + rating + ", comment = " + comment + "]";
    }
}