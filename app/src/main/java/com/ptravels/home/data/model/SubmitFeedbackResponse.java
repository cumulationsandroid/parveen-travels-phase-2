package com.ptravels.home.data.model;

public class SubmitFeedbackResponse
{
    private String status;

    private FeedbackResponseRecords records;

    private String description;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public FeedbackResponseRecords getRecords ()
    {
        return records;
    }

    public void setRecords (FeedbackResponseRecords records)
    {
        this.records = records;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", records = "+records+"]";
    }
}