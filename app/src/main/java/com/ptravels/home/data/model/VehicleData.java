package com.ptravels.home.data.model;

/**
 * Created by Amit Tumkur on 07-03-2018.
 */

public class VehicleData {
    private String statusCode;

    private String[] driverName;

    private String vehicleNumber;

    private String driverMobile;

    private String statusMessage;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String[] getDriverName() {
        return driverName;
    }

    public void setDriverName(String[] driverName) {
        this.driverName = driverName;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", driverName = " + driverName + ", vehicleNumber = " + vehicleNumber + ", driverMobile = " + driverMobile + ", statusMessage = " + statusMessage + "]";
    }
}
