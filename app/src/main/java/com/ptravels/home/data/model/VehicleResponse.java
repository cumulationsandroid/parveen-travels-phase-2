package com.ptravels.home.data.model;

public class VehicleResponse
{
    private String message;

    private VehicleData data;

    private boolean success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public VehicleData getData ()
    {
        return data;
    }

    public void setData (VehicleData data)
    {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+", success = "+success+"]";
    }
}