package com.ptravels.home.data.source;

import com.ptravels.REST.RestClient;
import com.ptravels.home.data.model.Directions;
import com.ptravels.home.domain.DirectionsInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class DirectionsDataSourceImpl implements DirectionsInterface {
    @Override
    public Single<Directions> getDirections(String units, String origin, String destination, String mode, String key) {
        return RestClient.getDirectionsApi()
                .getDirections(units, origin, destination, mode, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
