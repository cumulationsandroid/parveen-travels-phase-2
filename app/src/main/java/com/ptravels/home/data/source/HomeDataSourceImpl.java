package com.ptravels.home.data.source;

import com.ptravels.REST.RestClient;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.HomeImageResponse;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.model.VehicleResponse;
import com.ptravels.home.domain.HomeDataInterface;

import okhttp3.ResponseBody;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public class HomeDataSourceImpl implements HomeDataInterface {

    @Override
    public Single<ResponseBody> getHomeImage(String imageUrl) {
        return RestClient.getApiServiceForAdminPanel().getHomeImage(imageUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<HomeImageResponse> getHomeImageDetails() {
        RestClient.setDisableCache(true);
        return RestClient.getApiServiceForAdminPanel().getImageDetails()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<FeedbackOptionsResponse> getFeedbackoptions() {
        return RestClient.getApiServiceForAdminPanel().getFeedbackOptions()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<SubmitFeedbackResponse> submitFeedback(SubmitFeedbackRequest submitFeedbackRequest) {
        return RestClient.getApiServiceForAdminPanel().sendFeedback(submitFeedbackRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<LiveTrackResponse> getLiveTrackData(String trackUrl) {
        RestClient.setDisableCache(true);
        return RestClient.getApiService().getLiveTracks(trackUrl)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<VehicleResponse> getVehicleData(String token, String pnr) {
        return RestClient.getApiService().getVehicleInfo(token,pnr)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
