package com.ptravels.home.domain;

import com.ptravels.home.data.model.Directions;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public interface DirectionsInterface {
    Single<Directions> getDirections(String units, String origin, String destination, String mode, String key);
}
