package com.ptravels.home.domain;

import com.ptravels.global.UseCase;
import com.ptravels.home.data.model.Directions;
import com.ptravels.home.data.model.DirectionsRequest;
import com.ptravels.home.data.source.DirectionsDataSourceImpl;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class GetDirectionsUseCase extends UseCase<DirectionsRequest, Directions> {
    private DirectionsInterface directionsInterface = new DirectionsDataSourceImpl();
    @Override
    public Single<Directions> buildUseCase(DirectionsRequest directionsRequest) {
        return directionsInterface.getDirections(
                directionsRequest.getUnit(),
                directionsRequest.getOrigin(),
                directionsRequest.getDestination(),
                directionsRequest.getMode(),
                directionsRequest.getKey()
        );
    }
}
