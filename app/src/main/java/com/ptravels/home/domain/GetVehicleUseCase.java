package com.ptravels.home.domain;

import com.ptravels.ParveenApp;
import com.ptravels.global.UseCase;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.VehicleResponse;
import com.ptravels.home.data.source.HomeDataSourceImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public class GetVehicleUseCase extends UseCase<PathParamRequest, VehicleResponse> {
    private HomeDataInterface homeDataInterface = new HomeDataSourceImpl();

    @Override
    public Single<VehicleResponse> buildUseCase(PathParamRequest request) {
        return homeDataInterface.getVehicleData(ParveenApp.getAccessToken(),request.getPnr());
    }
}
