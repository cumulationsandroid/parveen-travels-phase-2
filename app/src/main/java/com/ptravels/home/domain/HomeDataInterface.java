package com.ptravels.home.domain;

import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.HomeImageResponse;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.model.VehicleResponse;

import okhttp3.ResponseBody;
import rx.Single;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public interface HomeDataInterface {
    Single<ResponseBody> getHomeImage(String imageUrl);
    Single<HomeImageResponse> getHomeImageDetails();
    Single<FeedbackOptionsResponse> getFeedbackoptions();
    Single<SubmitFeedbackResponse> submitFeedback(SubmitFeedbackRequest submitFeedbackRequest);
    Single<LiveTrackResponse> getLiveTrackData(String trackUrl);
    Single<VehicleResponse> getVehicleData(String token,String pnr);
}
