package com.ptravels.home.domain;

import com.ptravels.global.UseCase;
import com.ptravels.home.data.source.HomeDataSourceImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import okhttp3.ResponseBody;
import rx.Single;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public class ImageLoadUseCase extends UseCase<PathParamRequest, ResponseBody> {
    private HomeDataInterface homeDataInterface = new HomeDataSourceImpl();

    @Override
    public Single<ResponseBody> buildUseCase(PathParamRequest request) {
        return homeDataInterface.getHomeImage(request.getDynamicUrl());
    }
}
