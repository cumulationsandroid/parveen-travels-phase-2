package com.ptravels.home.domain;

import com.ptravels.global.UseCase;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.source.HomeDataSourceImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public class SubmitFeedbackUseCase extends UseCase<SubmitFeedbackRequest, SubmitFeedbackResponse> {
    private HomeDataInterface homeDataInterface = new HomeDataSourceImpl();

    @Override
    public Single<SubmitFeedbackResponse> buildUseCase(SubmitFeedbackRequest request) {
        return homeDataInterface.submitFeedback(request);
    }
}
