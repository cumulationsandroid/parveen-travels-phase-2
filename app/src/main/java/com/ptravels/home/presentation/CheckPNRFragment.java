package com.ptravels.home.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ptravels.R;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.myBooking.presentation.TicketDetailsActivity;

/**
 * Created by Amit Tumkur on 26-02-2018.
 */

public class CheckPNRFragment extends Fragment {
//    private TextInputLayout pnrInputLayout;
    private TextInputEditText pnrEditText;

    public CheckPNRFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_check_pnr,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        pnrInputLayout = view.findViewById(R.id.pnrLayout);
        pnrEditText = view.findViewById(R.id.pnrEt);

        view.findViewById(R.id.viewTicketBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pnrEditText.clearFocus();
//                pnrInputLayout.setErrorEnabled(false);
                pnrEditText.setError(null);
                if (pnrEditText.getText().toString().trim().isEmpty()){
//                    pnrInputLayout.setErrorEnabled(true);
                    pnrEditText.setError("PNR cannot be empty!!");
                    pnrEditText.setSelection(0);
                    pnrEditText.requestFocus();
                    return;
                }

                startActivity(new Intent(getActivity(), TicketDetailsActivity.class)
                        .putExtra(ParveenDBHelper.PNR, pnrEditText.getText().toString().trim().toUpperCase()));
//                getActivity().finish();
            }
        });

    }
}
