package com.ptravels.home.presentation;

import com.google.android.gms.maps.model.LatLng;
import com.ptravels.global.LoadingView;
import com.ptravels.home.data.model.Directions;

import java.util.List;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public interface DirectionsUiUpdateView extends LoadingView {
    void provideDirections(List<LatLng> directions, int distance);
    void ShowError(String error);
}
