package com.ptravels.home.presentation;

import com.ptravels.home.data.model.Directions;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public interface GetDirectionsPresenter {
    void getDirectionsData();
}
