package com.ptravels.home.presentation;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ptravels.BaseActivity;
import com.ptravels.ParveenApp;
import com.ptravels.R;
import com.ptravels.SearchBusses.presentation.SearchResultsActivity;
import com.ptravels.aboutUs.presentation.AboutUsActivity;
import com.ptravels.adminPanel.data.model.FcmData;
import com.ptravels.adminPanel.presentation.AdminPanelAPIsService;
import com.ptravels.global.Constants;
import com.ptravels.global.LoginActivity;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.home.data.model.NextDay;
import com.ptravels.home.domain.ImageDetailsUseCase;
import com.ptravels.home.domain.ImageLoadUseCase;
import com.ptravels.myBooking.data.model.PnrReview;
import com.ptravels.myBooking.presentation.FragmentHolderActivity;
import com.ptravels.myBooking.presentation.MyBookingActivity;
import com.ptravels.offers.presentation.OffersActivity;
import com.ptravels.signin.presentation.SigninActivity;
import com.ptravels.splash.presentation.SplashActivity;
import com.ptravels.stationsList.data.model.response.PathParamRequest;
import com.ptravels.stationsList.presentation.SearchActivity;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends BaseActivity implements View.OnClickListener, HomeUiUpdateView {
    private LinearLayout fromLayout, toLayout;
    private TextView fromLabel, toLabel, fromValue, toValue;
    private AppCompatButton findBusesBtn, roundTripButton;
    private String fromId, toId, dateSelected;
    private LinearLayout datePickerLayout;
    private TextView selectedDate;
    private RecyclerView dateBlocksRecyclerView;
    private SimpleDateFormat dateSelectFormat, dateFormatter;
    private HashMap<String, String> dateToSendMap = new HashMap<>();
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private FrameLayout loaderLayout;
    private ImageView loader, reverseSelection, closeIv;

    private ImageView bannerIv;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressedInterval;
    private Toast backToast;
    private AlertDialog ratingDialog;
    private AlertDialog offerDialog;
    private boolean showOfferDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_to);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        dateSelectFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        dateFormatter = new SimpleDateFormat("EEE, dd MMM", Locale.getDefault());
        initViews();
        try {
            initNavigationDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setClickListeners();

        if (getIntent() != null) {
            if (getIntent().hasExtra(Constants.FROM_STATION_ID)) {
                fromId = getIntent().getStringExtra(Constants.FROM_STATION_ID);
            }

            if (getIntent().hasExtra(Constants.TO_STATION_ID)) {
                toId = getIntent().getStringExtra(Constants.TO_STATION_ID);
            }

            if (getIntent().hasExtra(Constants.FROM_STATION_NAME)) {
                String fromPlace = getIntent().getStringExtra(Constants.FROM_STATION_NAME);
                fromValue.setVisibility(View.VISIBLE);
                fromValue.setText(fromPlace);
                fromValue.setTextColor(getResources().getColor(R.color.colorAccent));
            }

            if (getIntent().hasExtra(Constants.TO_STATION_NAME)) {
                String toPl = getIntent().getStringExtra(Constants.TO_STATION_NAME);
                toValue.setVisibility(View.VISIBLE);
                toValue.setText(toPl);
                toValue.setTextColor(getResources().getColor(R.color.colorAccent));

            }
        }
        backToast = Toast.makeText(this,R.string.close_app_alert,Toast.LENGTH_SHORT);

        if (getIntent().getExtras().getSerializable(Constants.FCM_DATA)!=null){
            showOfferDialog = true;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkPlayServices();
        sendFCMTokenFirstTime();
        checkHomeImageVersion();
        checkForPendingReview();
    }

    private void checkHomeImageVersion(){
        HomePresenter homePresenter = new HomePresenterImpl(this);
        homePresenter.getHomeImageData(new ImageDetailsUseCase(),null);
    }

    private void loadBannerImage(String imageUrl) {
        String filePath = SharedPrefUtils.getSharedPrefUtils().getHomeImagePath(this);
        if (filePath != null) {
            Picasso.with(this)
                    .load(new File(filePath))
                    .error(R.drawable.home_banner).placeholder(R.drawable.home_banner)
                    .into(bannerIv);
        }
        HomePresenter homePresenter = new HomePresenterImpl(this);
        PathParamRequest request = new PathParamRequest();
        request.setDynamicUrl(imageUrl);
        homePresenter.downloadHomeImage(new ImageLoadUseCase(),request);

    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fromLayout = findViewById(R.id.fromLayout);
        toLayout = findViewById(R.id.toLayout);
        fromLabel = findViewById(R.id.fromLabelText);
        toLabel = findViewById(R.id.goingToLabelText);
        fromValue = findViewById(R.id.fromValue);
        toValue = findViewById(R.id.goingToValue);
        findBusesBtn = findViewById(R.id.findBusesBtn);
        datePickerLayout = findViewById(R.id.datePickerLayout);
        selectedDate = findViewById(R.id.selectedDateText);
        dateBlocksRecyclerView = findViewById(R.id.horizontal_recycler_view);
        closeIv = findViewById(R.id.closeIv);
        bannerIv = findViewById(R.id.banner);

        roundTripButton = findViewById(R.id.roundTripButton);

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        reverseSelection = findViewById(R.id.reverseSelectionIv);

        /* this to help scrolling down in small screen devices */
        final NestedScrollView scrollView = findViewById(R.id.scrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

        refreshTheHorizontalDateView(dateSelectFormat.format(Calendar.getInstance().getTime()));
    }

    public void initNavigationDrawer() throws Exception {

        final boolean loggedIn = SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this);
        NavigationView navigationView = findViewById(R.id.righNavigationView);

        /*Enabling marquee for Navigation Menu items*/
        final TextView signUpLogin = navigationView.getMenu().getItem(0).getActionView().findViewById(R.id.menuTitle);
        if (loggedIn && !SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(this)) {
            signUpLogin.setText(SharedPrefUtils.getSharedPrefUtils().getUserEmailID(HomeActivity.this));
        } else {
            signUpLogin.setText("Sign Up / Log in");
        }
        ImageView signupIcon = navigationView.getMenu().getItem(0).getActionView().findViewById(R.id.menuIcon);
        signupIcon.setImageResource(R.drawable.ic_sign_up_log_in);

        TextView bookings = navigationView.getMenu().getItem(1).getActionView().findViewById(R.id.menuTitle);
        bookings.setText("My Bookings");
        ImageView bookingsIcon = navigationView.getMenu().getItem(1).getActionView().findViewById(R.id.menuIcon);
        bookingsIcon.setImageResource(R.drawable.ic_my_bookings);

        TextView liveTracking = navigationView.getMenu().getItem(2).getActionView().findViewById(R.id.menuTitle);
        liveTracking.setText("Live tracking");
        ImageView trackIcon = navigationView.getMenu().getItem(2).getActionView().findViewById(R.id.menuIcon);
        trackIcon.setImageResource(R.drawable.live_tracking);

        TextView offers = navigationView.getMenu().getItem(3).getActionView().findViewById(R.id.menuTitle);
        offers.setText("Offers");
        ImageView offersIcon = navigationView.getMenu().getItem(3).getActionView().findViewById(R.id.menuIcon);
        offersIcon.setImageResource(R.drawable.offers);

        final TextView referEarn = navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.menuTitle);
        referEarn.setText("Refer & Earn");
        ImageView referEarnIcon = navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.menuIcon);
        referEarnIcon.setImageResource(R.drawable.refer_earn);

        final TextView chekcPn = navigationView.getMenu().getItem(5).getActionView().findViewById(R.id.menuTitle);
        chekcPn.setText("Check PNR / Live Tracking");
        ImageView checkPnrIcon = navigationView.getMenu().getItem(5).getActionView().findViewById(R.id.menuIcon);
        checkPnrIcon.setImageResource(R.drawable.check_pnr);

        chekcPn.postDelayed(new Runnable() {
            @Override
            public void run() {
                chekcPn.setSelected(true);
            }
        },1500);

        final TextView call = navigationView.getMenu().getItem(6).getActionView().findViewById(R.id.menuTitle);
        call.setText("Call Support");
        ImageView callIcon = navigationView.getMenu().getItem(6).getActionView().findViewById(R.id.menuIcon);
        callIcon.setImageResource(R.drawable.ic_call_support);
//        whatIsHnModeTv.setPadding(100,0,0,0);

        TextView about = navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.menuTitle);
        about.setText("About Us");
        ImageView abtIcon = navigationView.getMenu().getItem(7).getActionView().findViewById(R.id.menuIcon);
        abtIcon.setImageResource(R.drawable.ic_about_us);

        navigationView.getMenu().getItem(8).getActionView().setVisibility(View.VISIBLE);
        TextView logout = navigationView.getMenu().getItem(8).getActionView().findViewById(R.id.menuTitle);
        logout.setText(R.string.logout);
        ImageView logoutIcon = navigationView.getMenu().getItem(8).getActionView().findViewById(R.id.menuIcon);
        logoutIcon.setImageResource(R.drawable.ic_logout);

        if (loggedIn && !SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(this)) {
            navigationView.getMenu().getItem(1).setVisible(true);
            navigationView.getMenu().getItem(8).setVisible(true);
        } else {
            navigationView.getMenu().getItem(1).setVisible(false);
            navigationView.getMenu().getItem(8).setVisible(false);
        }

        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(4).setVisible(false);

        signUpLogin.postDelayed(new Runnable() {
            @Override
            public void run() {
                signUpLogin.setSelected(true);
            }
        }, 1500);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.signUpLogin:
                        if (!(loggedIn && !SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(HomeActivity.this))) {
                            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                        }
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.myBookings:
                        startActivity(new Intent(HomeActivity.this, MyBookingActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.offers:
                        startActivity(new Intent(HomeActivity.this, OffersActivity.class));
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.checkPnr:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, FragmentHolderActivity.class)
                                .putExtra(Constants.LOAD_FRAGMENT, FragmentHolderActivity.CHECK_PNR_FRAGMENT));
                        break;

                    case R.id.liveTracking:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, LiveTrackBusActivity.class));
                        break;

                    case R.id.callSupport:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, FragmentHolderActivity.class)
                                .putExtra(Constants.LOAD_FRAGMENT, FragmentHolderActivity.CALL_SUPPORT_FRAGMENT));
                        break;
                    case R.id.aboutUs:
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, AboutUsActivity.class));
                        break;
                    case R.id.logout:
                        LoginManager.getInstance().logOut();
                        SharedPrefUtils.getSharedPrefUtils().clearUserData(HomeActivity.this);
                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, SplashActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                        break;
                }
                return true;
            }
        });
//        View header = navigationView.getHeaderView(0);
        drawerLayout = findViewById(R.id.drawer_layout);


        navigationView.getHeaderView(0).findViewById(R.id.closeIv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
            }
        });

        navigationView.getHeaderView(0).findViewById(R.id.hamburgerIv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
            }
        });
    }


    private void populateHashMap(List<String> next5DaysList, List<String> next5DaysListForIntent) {
        for (int i = 0; i < next5DaysList.size(); i++) {
            dateToSendMap.put(next5DaysList.get(i), next5DaysListForIntent.get(i));
        }
    }

    private void setClickListeners() {
        fromLayout.setOnClickListener(this);
        toLayout.setOnClickListener(this);
        findBusesBtn.setOnClickListener(this);
        roundTripButton.setOnClickListener(this);
        datePickerLayout.setOnClickListener(this);
        reverseSelection.setOnClickListener(this);
        findViewById(R.id.hamburgerMenuIv).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.fromLayout:
                if (!SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this)) {
                    Toast.makeText(this, "Not Logged In", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, SigninActivity.class));
                    return;
                }

                Intent searchFromIntent = new Intent(this, SearchActivity.class);
                searchFromIntent.putExtra(Constants.FROM_STATION_ID, fromId)
                        .putExtra(Constants.TO_STATION_ID, toId)
                        .putExtra(Constants.PRE_SELECTED, Constants.FROM_STATION_NAME)
                        .putExtra(Constants.FROM_STATION_NAME, fromValue.getText().toString())
                        .putExtra(Constants.TO_STATION_NAME, toValue.getText().toString());
                startActivity(searchFromIntent);

                break;
            case R.id.toLayout: {
                if (!SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this)) {
                    Toast.makeText(this, "Not Logged In", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, SigninActivity.class));
                    return;
                }
                Intent searchToIntent = new Intent(this, SearchActivity.class);
                searchToIntent.putExtra(Constants.FROM_STATION_ID, fromId)
                        .putExtra(Constants.TO_STATION_ID, toId)
                        .putExtra(Constants.PRE_SELECTED, Constants.TO_STATION_NAME)
                        .putExtra(Constants.FROM_STATION_NAME, fromValue.getText().toString())
                        .putExtra(Constants.TO_STATION_NAME, toValue.getText().toString());
                startActivity(searchToIntent);
            }
            break;
            case R.id.findBusesBtn:

                if (dateSelected == null || dateSelected.isEmpty()) {
                    Toast.makeText(this, "Select Travel Date", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (fromId == null || fromValue.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Select From place", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (toId == null || toValue.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Select To place", Toast.LENGTH_SHORT).show();
                    return;
                }

//                showLoadingAndExit();
                showLoading();
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(HomeActivity.this, SearchResultsActivity.class)
                                .putExtra(Constants.FROM_STATION_ID, fromId)
                                .putExtra(Constants.TO_STATION_ID, toId)
                                .putExtra(Constants.FROM_STATION_NAME, fromValue.getText().toString())
                                .putExtra(Constants.TO_STATION_NAME, toValue.getText().toString())
                                .putExtra(Constants.TRAVEL_DATE, dateSelected));

                        logSearchEventToFabric();
                    }
                }, 500);
                break;

            case R.id.datePickerLayout:
                showDatePickerDialog();
                break;

            case R.id.hamburgerMenuIv:
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
                break;

            case R.id.reverseSelectionIv:
                if (fromId == null || toId == null) {
                    Toast.makeText(this, "Please select From and To Cities", Toast.LENGTH_SHORT).show();
                } else
                    interchangeFromAndToCity();
                break;


            case R.id.roundTripButton:
                Toast.makeText(this, "Round Trip not available", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void logSearchEventToFabric() {
        try {
            Answers.getInstance().logSearch(new SearchEvent()
                    .putQuery(fromValue.getText().toString()+" - "+toValue.getText().toString()));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        showOfferDialog = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        fromValue.postDelayed(new Runnable() {
            @Override
            public void run() {
                fromValue.setSelected(true);
            }
        }, 1500);

        toValue.postDelayed(new Runnable() {
            @Override
            public void run() {
                toValue.setSelected(true);
            }
        }, 1500);

        hideLoading();
        if (showOfferDialog)
            showOfferCode();
    }

    private void showOfferCode() {
        if (getIntent()!=null
                && getIntent().getExtras()!=null
                && getIntent().getExtras().getSerializable(Constants.FCM_DATA)!=null){
            showOfferCodeAlert((FcmData) getIntent().getExtras().getSerializable(Constants.FCM_DATA));
        }
    }

    private void sendFCMTokenFirstTime() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("token",""+token);
        if (token!=null){
            String savedToken = SharedPrefUtils.getSharedPrefUtils().getFcmToken(this);
            if (savedToken==null || savedToken.isEmpty() || !savedToken.equals(token)){
                startService(new Intent(this, AdminPanelAPIsService.class)
                        .putExtra(Constants.API_TYPE, Constants.SEND_TOKEN_TO_SERVER)
                        .putExtra(Constants.FCM_TOKEN, token));
            }
        }
    }

    private void checkForPendingReview() {
        List<PnrReview> pendingPnrs = ParveenApp.getCompletedPNRsList();
        if (pendingPnrs != null && pendingPnrs.size() > 0) {
            final PnrReview pnrReview = pendingPnrs.get(0);

//            View parentLayout = findViewById(R.id.rootLayout);

        /*final PnrReview pnrReview = new PnrReview();
        pnrReview.setPnr("BSPT418485934");
        pnrReview.setFrmCity("Bangalore");
        pnrReview.setToCity("Chennai");
        pnrReview.setTravelDate("2018-03-06");
        pnrReview.setBookingDate("2018-02-28");
        pnrReview.setBookingStatus(PnrReview.COMPLETED);
        pnrReview.setPrice("1890");
        pnrReview.setSeats("3");
        pnrReview.setReviewStatus(PnrReview.PENDING);*/
            String reviewMsg = "Please review and give rating for your journey from " + pnrReview.getFrmCity() + " to " +
                    pnrReview.getToCity() + " on " + Utils.formatDateString("yyyy-MM-dd","dd MMM, yyyy",pnrReview.getTravelDate());

            /*Snackbar snackbar = Snackbar
                    .make(parentLayout, reviewMsg, Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(HomeActivity.this, RatingReviewActivity.class)
                                    .putExtra(Constants.PNR_REVIEW_MODEL, pnrReview));
                        }
                    });

            View snackbarView = snackbar.getView();
            TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setMaxLines(3);
            snackbar.show();*/

            if (!ParveenApp.isRatingDialogShown())
                showRatingAlert(reviewMsg,pnrReview);
        }
    }

    private void interchangeFromAndToCity() {
        String temp = fromId;
        fromId = toId;
        toId = temp;

        temp = fromValue.getText().toString();
        fromValue.setText(toValue.getText());
        toValue.setText(temp);
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = 0, month = 0, day = 0;
        if (selectedDate.getText().toString().isEmpty()) {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            try {
                if (dateToSendMap.containsKey(selectedDate.getText().toString())) {
                    SimpleDateFormat textDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String requiredDateString = dateToSendMap.get(selectedDate.getText().toString());
                    calendar.setTime(textDateFormat.parse(requiredDateString));
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                } else {
                    if (dateSelected != null) {
                        calendar.setTime(dateSelectFormat.parse(dateSelected));
                        year = calendar.get(Calendar.YEAR);
                        month = calendar.get(Calendar.MONTH);
                        day = calendar.get(Calendar.DAY_OF_MONTH);
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar clickedCalendar = Calendar.getInstance();
                clickedCalendar.set(i, i1, i2);
                selectedDate.setText(dateFormatter.format(clickedCalendar.getTime()));
                dateSelected = dateSelectFormat.format(clickedCalendar.getTime());
                refreshTheHorizontalDateView(dateSelected);
            }
        }, year, month, day);

        datePickerDialog.show();
        /*disabling past dates*/
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
    }

    public void refreshTheHorizontalDateView(String currentDate) {

        List<String> next5DaysList = Utils.getUtils().getNext5Days(currentDate, 5);
        List<String> next5DaysListForIntent = Utils.getUtils().getNext5DaysForIntent(currentDate, 5);

        populateHashMap(next5DaysList, next5DaysListForIntent);

        List<NextDay> nextDayList = new ArrayList<>();
        for (String nextDayValue : next5DaysList) {
            NextDay nextDay = new NextDay();
            nextDay.setNextDayText(nextDayValue);
            nextDayList.add(nextDay);
        }

        //selcting the first day to true automatically
        nextDayList.get(0).setSelected(true);

        DateBlocksListAdapter adapter = new DateBlocksListAdapter(this, nextDayList);
        dateBlocksRecyclerView.setAdapter(adapter);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(this);
        horizontalLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        dateBlocksRecyclerView.setLayoutManager(horizontalLayoutManager);

    }

    public void updateSelectedDate(String dateClicked) {
        selectedDate.setText(dateClicked);
        dateSelected = dateToSendMap.get(dateClicked);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            initNavigationDrawer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT) || drawerLayout.isDrawerOpen(Gravity.END)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            if (mBackPressedInterval + TIME_INTERVAL > System.currentTimeMillis()) {
                if (backToast!=null) {
                    backToast.cancel();
                }
                super.onBackPressed();
                return;
            } else {
                if (backToast!=null)
                    backToast.show();
            }
            mBackPressedInterval = System.currentTimeMillis();
        }
    }


    public void showLoadingAndExit() {
        Toast.makeText(this,"Fetching Bus List for You!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader,null,this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader,null,this);
    }

    @Override
    public void provideImageUrl(String imageUrl) {
        loadBannerImage(imageUrl);
    }

    @Override
    public void provideImageFilePath(String filePath) {
        if (filePath != null){
            Picasso.with(this)
                    .load(new File(filePath))
                    .error(R.drawable.home_banner).placeholder(R.drawable.home_banner)
                    .into(bannerIv);
        }
    }

    @Override
    public void showError(String error) {
        if (error == null)
            return;
        if (error.equalsIgnoreCase("Same version")){
            String path = SharedPrefUtils.getSharedPrefUtils().getHomeImagePath(this);
            Log.d("showError","path = "+path);
            Picasso.with(this)
                    .load(new File(path))
                    .error(R.drawable.home_banner).placeholder(R.drawable.home_banner)
                    .into(bannerIv);
        } else
            Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
    }

    private void checkPlayServices(){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int success = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (success != ConnectionResult.SUCCESS) {
            googleApiAvailability.makeGooglePlayServicesAvailable(this);
        }
    }

    private void showRatingAlert(String msg, final PnrReview pnrReview) {
        if (ratingDialog != null && ratingDialog.isShowing())
            ratingDialog.dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog,null);
        builder.setView(customView);
//        builder.setTitle("Review your journey");
//        builder.setMessage(msg);
        /*builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ratingDialog.dismiss();
                startActivity(new Intent(HomeActivity.this, RatingReviewActivity.class)
                        .putExtra(Constants.PNR_REVIEW_MODEL, pnrReview));
                ParveenApp.setIsRatingDialogShown(true);
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ratingDialog.dismiss();
                ParveenApp.setIsRatingDialogShown(true);
            }
        });*/

        TextView title = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setText("Cancel");
        secondBtn.setText("OK");
        title.setText("Review your journey");
        descrpt.setText(msg);
        firstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.dismiss();
                ParveenApp.setIsRatingDialogShown(true);
            }
        });

        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingDialog.dismiss();
                startActivity(new Intent(HomeActivity.this, RatingReviewActivity.class)
                        .putExtra(Constants.PNR_REVIEW_MODEL, pnrReview));
                ParveenApp.setIsRatingDialogShown(true);
            }
        });

        if (ratingDialog == null || !ratingDialog.isShowing())
            ratingDialog = builder.create();
        ratingDialog.show();
    }

    private void showOfferCodeAlert(FcmData fcmData) {
        /*OffersCodeDialog offersCodeDialog = OffersCodeDialog.initOffersCodeDialog(fcmData);
        offersCodeDialog.show(getSupportFragmentManager(),OffersCodeDialog.class.getSimpleName());*/

        StringBuilder content = new StringBuilder();
        if (fcmData.getDescription()!=null && !fcmData.getDescription().isEmpty()){
            content.append(fcmData.getDescription());
            content.append("\n");
        }

        if (fcmData.getOffer_id()!=null && !fcmData.getOffer_id().isEmpty()){
            content.append("Code:").append(" ").append("\"").append(fcmData.getOffer_id()).append("\"");
        }

        if (offerDialog != null && offerDialog.isShowing())
            offerDialog.dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog,null);
        builder.setView(customView);
        builder.setCancelable(false);
        /*builder.setTitle("Offers"+fcmData.getTitle());
        builder.setMessage(content);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                offerDialog.dismiss();
                showOfferDialog = false;
            }
        });*/

        TextView title = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setVisibility(View.GONE);
        secondBtn.setText("OK");
        title.setText(fcmData.getTitle());
        descrpt.setText(content);
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offerDialog.dismiss();
                showOfferDialog = false;
            }
        });

        if (offerDialog == null || !offerDialog.isShowing())
            offerDialog = builder.create();
        offerDialog.show();

    }
}

