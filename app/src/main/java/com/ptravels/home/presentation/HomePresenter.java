package com.ptravels.home.presentation;

import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.domain.GetFeedbackOptionsUseCase;
import com.ptravels.home.domain.GetVehicleUseCase;
import com.ptravels.home.domain.ImageDetailsUseCase;
import com.ptravels.home.domain.ImageLoadUseCase;
import com.ptravels.home.domain.LiveTrackDataUseCase;
import com.ptravels.home.domain.SubmitFeedbackUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public interface HomePresenter {
    void downloadHomeImage(ImageLoadUseCase imageLoadUseCase, PathParamRequest request);
    void getHomeImageData(ImageDetailsUseCase imageDetailsUseCase, PathParamRequest request);
    void getFeedbackOptions(GetFeedbackOptionsUseCase getFeedbackOptionsUseCase,PathParamRequest request);
    void sendFeedback(SubmitFeedbackUseCase submitFeedbackUseCase, SubmitFeedbackRequest request);
    void getLiveTrackData(LiveTrackDataUseCase liveTrackDataUseCase, PathParamRequest request);
    void getVehicleData(GetVehicleUseCase getVehicleUseCase, PathParamRequest request);
}
