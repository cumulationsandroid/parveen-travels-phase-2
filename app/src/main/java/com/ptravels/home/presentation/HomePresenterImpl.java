package com.ptravels.home.presentation;

import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.global.Constants;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.HomeImageResponse;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.model.VehicleResponse;
import com.ptravels.home.domain.GetFeedbackOptionsUseCase;
import com.ptravels.home.domain.GetVehicleUseCase;
import com.ptravels.home.domain.ImageDetailsUseCase;
import com.ptravels.home.domain.ImageLoadUseCase;
import com.ptravels.home.domain.LiveTrackDataUseCase;
import com.ptravels.home.domain.SubmitFeedbackUseCase;
import com.ptravels.myBooking.data.model.PnrReview;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by Amit Tumkur on 19-02-2018.
 */

public class HomePresenterImpl implements HomePresenter {
    private HomeUiUpdateView homeUiUpdateView;
    private RatingUiUpdateView ratingUiUpdateView;
    private LiveTrackDataUiUpdateView liveTrackDataUiUpdateView;

    public HomePresenterImpl(LiveTrackDataUiUpdateView liveTrackDataUiUpdateView) {
        this.liveTrackDataUiUpdateView = liveTrackDataUiUpdateView;
    }

    public HomePresenterImpl(HomeUiUpdateView homeUiUpdateView) {
        this.homeUiUpdateView = homeUiUpdateView;
    }

    public HomePresenterImpl(RatingUiUpdateView ratingUiUpdateView) {
        this.ratingUiUpdateView = ratingUiUpdateView;
    }

    @Override
    public void downloadHomeImage(ImageLoadUseCase imageLoadUseCase, final PathParamRequest request) {
        if (Utils.isOnline()) {
            imageLoadUseCase.execute(request)
                    .subscribe(new SingleSubscriber<ResponseBody>() {
                        @Override
                        public void onSuccess(ResponseBody responseBody) {
                            if (Utils.getUtils().saveImageLocal(responseBody,request.getDynamicUrl())) {
                                String filePath = SharedPrefUtils.getSharedPrefUtils().getHomeImagePath(ParveenApp.getContext());
                                homeUiUpdateView.provideImageFilePath(filePath);
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            try {
                                Log.d("xyz",error.toString());
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    homeUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    homeUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    homeUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                homeUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            homeUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getHomeImageData(ImageDetailsUseCase imageDetailsUseCase, final PathParamRequest request) {
        if (Utils.isOnline()) {
            imageDetailsUseCase.execute(request)
                    .subscribe(new SingleSubscriber<HomeImageResponse>() {
                        @Override
                        public void onSuccess(HomeImageResponse homeImageResponse) {
                            if (homeImageResponse!=null){
                                if (homeImageResponse.getStatus().equalsIgnoreCase("success")){
                                    String oldVersion = SharedPrefUtils.getSharedPrefUtils().getHomeImageVersion(ParveenApp.getContext());
                                    String versionId = homeImageResponse.getRecords().getVersion_id();
                                    if (oldVersion == null || (Integer.valueOf(versionId)>Integer.valueOf(oldVersion))) {
                                        String imageUrl = homeImageResponse.getRecords().getImg_url();
                                        SharedPrefUtils.getSharedPrefUtils().updateHomeImageVersion(ParveenApp.getContext(), versionId);
                                        homeUiUpdateView.provideImageUrl(imageUrl);
                                    } else {
                                        homeUiUpdateView.showError("Same Version");
                                    }
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    homeUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    homeUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    homeUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                homeUiUpdateView.showError(/*Constants.UNKNOWN_ERROR*/e.getMessage());
                            }

                        }
                    });
        } else {
            homeUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getFeedbackOptions(GetFeedbackOptionsUseCase getFeedbackOptionsUseCase, PathParamRequest request) {
        ratingUiUpdateView.showLoading();
        if (Utils.isOnline()) {
            getFeedbackOptionsUseCase.execute(request)
                    .subscribe(new SingleSubscriber<FeedbackOptionsResponse>() {
                        @Override
                        public void onSuccess(FeedbackOptionsResponse response) {
                            ratingUiUpdateView.hideLoading();
                            if (response!=null){
                                if (response.getStatus().equalsIgnoreCase("success")){
                                    ratingUiUpdateView.provideFeedbackOptions(response);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            ratingUiUpdateView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    ratingUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    ratingUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    ratingUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                ratingUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            ratingUiUpdateView.hideLoading();
            ratingUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void sendFeedback(SubmitFeedbackUseCase submitFeedbackUseCase, final SubmitFeedbackRequest request) {
        ratingUiUpdateView.showLoading();
        if (Utils.isOnline()) {
            submitFeedbackUseCase.execute(request)
                    .subscribe(new SingleSubscriber<SubmitFeedbackResponse>() {
                        @Override
                        public void onSuccess(SubmitFeedbackResponse response) {
                            ratingUiUpdateView.hideLoading();
                            if (response!=null){
                                if (response.getStatus().equalsIgnoreCase("success")){
                                    ratingUiUpdateView.updateSubmitFeedbackResponse(response);
                                } else {
                                    String msg = null;
                                    if (response.getDescription()!=null)
                                        msg = response.getDescription();
                                    logRatingFailedEvent(request.getPnr(),msg);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            ratingUiUpdateView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    ratingUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    ratingUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    ratingUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                ratingUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            ratingUiUpdateView.hideLoading();
            ratingUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    private void logRatingFailedEvent(String pnr, String msg) {
        ContentViewEvent event = new ContentViewEvent();
        event.putContentId(pnr);
        event.putContentName("Rating feedback");
        event.putContentType("Rating failed");
        if (msg != null)
            event.putCustomAttribute("msg", msg);
        try {
            Answers.getInstance().logContentView(event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getLiveTrackData(LiveTrackDataUseCase liveTrackDataUseCase, final PathParamRequest request) {
        liveTrackDataUiUpdateView.showLoading();
        if (Utils.isOnline()) {
            liveTrackDataUseCase.execute(request)
                    .subscribe(new SingleSubscriber<LiveTrackResponse>() {
                        @Override
                        public void onSuccess(LiveTrackResponse trackResponse) {
                            try {
                                liveTrackDataUiUpdateView.hideLoading();
                                liveTrackDataUiUpdateView.provideLiveTrackingData(trackResponse);
                            }catch (Exception e){

                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            try {
                                liveTrackDataUiUpdateView.hideLoading();
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    liveTrackDataUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    liveTrackDataUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    liveTrackDataUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                liveTrackDataUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            liveTrackDataUiUpdateView.hideLoading();
            liveTrackDataUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getVehicleData(GetVehicleUseCase getVehicleUseCase, PathParamRequest request) {
        liveTrackDataUiUpdateView.showLoading();
        if (Utils.isOnline()) {
            getVehicleUseCase.execute(request)
                    .subscribe(new SingleSubscriber<VehicleResponse>() {
                        @Override
                        public void onSuccess(VehicleResponse vehicleResponse) {
                            liveTrackDataUiUpdateView.hideLoading();
                            if (vehicleResponse != null) {
                                if (vehicleResponse.getMessage().equalsIgnoreCase("Sucess")
                                        && vehicleResponse.isSuccess()) {
                                    if (vehicleResponse.getData().getStatusCode().equalsIgnoreCase("500")) {
                                        liveTrackDataUiUpdateView.showError(vehicleResponse.getData().getStatusMessage());
                                    } else if (vehicleResponse.getData().getStatusCode().equalsIgnoreCase("200")) {
                                        liveTrackDataUiUpdateView.provideVehicleData(vehicleResponse);
                                    }
                                } else if (vehicleResponse.getMessage().equalsIgnoreCase("Failure")
                                        && !vehicleResponse.isSuccess()) {
                                    liveTrackDataUiUpdateView.showError(vehicleResponse.getData().getStatusMessage());
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            liveTrackDataUiUpdateView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    liveTrackDataUiUpdateView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    liveTrackDataUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    liveTrackDataUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                liveTrackDataUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            liveTrackDataUiUpdateView.hideLoading();
            liveTrackDataUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }
}
