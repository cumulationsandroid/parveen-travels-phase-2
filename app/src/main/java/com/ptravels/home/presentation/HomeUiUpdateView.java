package com.ptravels.home.presentation;

import com.ptravels.global.LoadingView;

/**
 * Created by Amit Tumkur on 16-02-2018.
 */

public interface HomeUiUpdateView extends LoadingView{
    void provideImageUrl(String imageUrl);
    void provideImageFilePath(String filePath);
    void showError(String error);
}
