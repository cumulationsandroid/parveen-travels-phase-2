package com.ptravels.home.presentation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ptravels.R;
import com.ptravels.global.Constants;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.Utils;
import com.ptravels.home.data.model.LiveTrackData;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.VehicleResponse;
import com.ptravels.home.domain.GetVehicleUseCase;
import com.ptravels.home.domain.LiveTrackDataUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * This shows how to create a simple activity with a map and a marker on the map.
 */
public class LiveTrackBusActivity extends AppCompatActivity implements LiveTrackDataUiUpdateView, OnMapReadyCallback, DirectionsUiUpdateView {
    private static final int LIVE_TRACK_TIMER = 1000 * 35;    /*35 seconds*/
    static Marker busMarker;
    static float durationInMs = 0;
    static List<LatLng> latlngDirections = new ArrayList<>();
    static Queue<LatLng> queue;
    int temp = 0;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private TextView fromTv, toTv, currentPlaceTv, busNumberTv, pnrNoTv, driversNameTv, driverNumberTv;
    private HomePresenter homePresenter;
    private LinearLayout labelLayout;
    private GoogleMap googleMap;
    private String vehicle_number;
    private boolean showLoader = true;
    private Handler updateTrackDataHandler;
    private Runnable updateLiveDataRunnable = new Runnable() {
        @Override
        public void run() {
            if (isFinishing()) {
                updateTrackDataHandler.removeCallbacksAndMessages(null);
                return;
            }
            getLiveData();
//            updateTrackDataHandler.postDelayed(updateLiveDataRunnable,LIVE_TRACK_TIMER);
        }
    };
    private boolean isMarkerRotating;
    private AlertDialog liveTrackDialog;
    private GetDirectionsPresenter directionsPresenter;
    private LatLng currentLatLng;
    private LatLng prevLatLng;
    private String prevLatLngTemp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_track);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        labelLayout = findViewById(R.id.label);
        fromTv = findViewById(R.id.fromCity);
        toTv = findViewById(R.id.toCity);
        currentPlaceTv = findViewById(R.id.currentPlace);
        busNumberTv = findViewById(R.id.busNo);
        pnrNoTv = findViewById(R.id.pnr);
        driversNameTv = findViewById(R.id.driverName);
        driverNumberTv = findViewById(R.id.driverNumber);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        TextView title = findViewById(R.id.title);
        title.setText("Live Tracking");

        if (getIntent().getStringExtra(Constants.FROM_STATION_NAME) != null
                && getIntent().getStringExtra(Constants.TO_STATION_NAME) != null) {
            labelLayout.setVisibility(View.VISIBLE);
            fromTv.setText(getIntent().getStringExtra(Constants.FROM_STATION_NAME));
            toTv.setText(getIntent().getStringExtra(Constants.TO_STATION_NAME));
            pnrNoTv.setText("PNR - " + getIntent().getStringExtra(ParveenDBHelper.PNR));
        } else {
            labelLayout.setVisibility(View.GONE);
        }

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mapFragment.getMapAsync(this);
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we
     * just add a marker near Africa.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
//        map.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // TODO: rotate gesture wont be visible if you're not setting proper padding
        labelLayout.post(new Runnable() {
            @Override
            public void run() {
                googleMap.setPadding(0, labelLayout.getHeight() + 10, 0, 0);
            }
        });

        PathParamRequest request = new PathParamRequest();
        request.setPnr(getIntent().getStringExtra(ParveenDBHelper.PNR));

        googleMap.setMaxZoomPreference(18f);

        homePresenter = new HomePresenterImpl(this);
        homePresenter.getVehicleData(new GetVehicleUseCase(), request);

        updateTrackDataHandler = new Handler();
    }

    @Override
    public void showLoading() {
        if (!showLoader)
            return;
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        if (!showLoader)
            return;
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void provideLiveTrackingData(LiveTrackResponse liveTrackResponse) {
        if (vehicle_number == null) {
            /*for (LiveTrackData data : liveTrackResponse.getData()){
                if ("TN 21 AU 6700".equals(data.getVehicle_number())) {
                    updateBusMarker(data);
                }
            }

            updateTrackDataHandler = new Handler();
            updateTrackDataHandler.postDelayed(updateLiveDataRunnable,LIVE_TRACK_TIMER);
            showLoader = false;*/
            return;
        }
        if (liveTrackResponse.getStatus() == 1 &&
                liveTrackResponse.getMessage().equalsIgnoreCase("Details found")) {
            for (LiveTrackData data : liveTrackResponse.getData()) {
                if (vehicle_number.equals(data.getVehicle_number())) {
                    //updateBusMarker(data);
                    animateBusMarker(data);
                }
            }

            updateTrackDataHandler.postDelayed(updateLiveDataRunnable, LIVE_TRACK_TIMER);
            showLoader = false;
        }
    }

    private void animateBusMarker(LiveTrackData data) {
//        prevLatLngTemp = "";
//        Log.e("map", "temp is " + temp);
//        if (temp == 1) {
//            data.setLat_message("13.074490");
//            data.setLon_message("80.200604");
//        } else if (temp == 2) {
//            data.setPre_lat_message("13.074490");
//            data.setPre_lon_message("80.200604");
//            data.setLat_message("13.075112");
//            data.setLon_message("80.200371");
//        } else if (temp == 3) {
//            data.setPre_lat_message("13.075112");
//            data.setPre_lon_message("80.200371");
//            data.setLat_message("13.075812");
//            data.setLon_message("80.200441");
//        } else if (temp == 4) {
//            data.setPre_lat_message("13.075812");
//            data.setPre_lon_message("80.200441");
//            data.setLat_message("13.076008");
//            data.setLon_message("80.198856");
//            13.076008, 80.198856
//        }
//        temp++;

        if (!prevLatLngTemp.equals(data.getPre_lat_message())) {
            currentLatLng = new LatLng(Double.parseDouble(data.getLat_message()), Double.parseDouble(data.getLon_message()));
            prevLatLng = new LatLng(Double.parseDouble(data.getPre_lat_message()), Double.parseDouble(data.getPre_lon_message()));
            prevLatLngTemp = data.getPre_lat_message();
//            directionsPresenter = new getDirectionsPresenterImpl(this, new GetDirectionsUseCase(), prevLatLng, currentLatLng);
//            directionsPresenter.getDirectionsData();
//
            ArrayList<LatLng> latlngList = new ArrayList<LatLng>();
            latlngList.add(currentLatLng);
            float[] distance = new float[1];
            try {
                Location.distanceBetween(prevLatLng.latitude, prevLatLng.longitude, currentLatLng.latitude, currentLatLng.longitude, distance);
            } catch (Exception e) {
                e.printStackTrace();
                distance[0] = 0;
            }
            provideDirections(latlngList, (int) distance[0]);
        }
    }

    private void fillPolyline(List<LatLng> directions, int distance) {
        Log.d("DISTANCE", "DISTANCE: " + distance);

        if (busMarker == null) {
            googleMap.clear();
            busMarker = googleMap.addMarker(new MarkerOptions()
                    .position(prevLatLng)
                    //.title(data.getVehicle_number())
                    .icon(getBitmapDescriptor(R.drawable.ic_circle_border_24dp))
                    .flat(true)
                    .anchor(0.5f, 0.5f)
            );
        }

//        if (distance == -1 && (directions == null || directions.size() == 0)) {
//            queue = new LinkedList();
//            queue.add(currentLatLng);
//            busMarker.setPosition(currentLatLng);
//            zoomToLocation(currentLatLng);
//            MarkerAnimation.animateMarkerToGB(busMarker, currentLatLng, new Spherical(), googleMap);
//            return;
//        }
        /*else {
            //busMarker.setPosition(prevLatLng);
            //busMarker.setAnchor(.5f,.5f);
        //}*/
        if (distance > 1000) {
            busMarker.setPosition(currentLatLng);
            zoomToLocation(currentLatLng);
        } else {
            if (queue != null) {
                queue.clear();
            }
            queue = new LinkedList();
            queue.clear();
            if (latlngDirections != null) {
                latlngDirections.clear();
            }
            latlngDirections = directions;
            for (int i = 0; i < directions.size(); i++) {
                Log.d("DIRECTIONS", "LATLNG: " + directions.get(i).latitude + ", " + directions.get(i).longitude);
            }
            googleMap.addPolyline(new PolylineOptions()
                    .addAll(directions)
                    .width(5)
                    .color(Color.TRANSPARENT)
                    .geodesic(true));
            durationInMs = 30000 / directions.size();
            for (int i = 0; i < directions.size(); i++) {
                queue.add(directions.get(i));
            }
            queue.clear();
            zoomToLocation(prevLatLng);
            MarkerAnimation.animateMarkerToGB(busMarker, currentLatLng, new LatLngInterpolator.LinearFixed(), googleMap);
        }
    }

    @Override
    public void provideDirections(List<LatLng> directions, int distance) {
        fillPolyline(directions, distance);
    }

    @Override
    public void ShowError(String error) {
        Log.d("ERROR", error);
    }

    private void updateBusMarker(LiveTrackData data) {
        Log.d("updateBusMarker", "lat " + data.getLat_message() + ", long " + data.getLon_message());
        Log.d("updateBusMarker", "prevlat " + data.getPre_lat_message() + ", prevlong " + data.getPre_lon_message());
        LatLng currentLatLng = new LatLng(Double.parseDouble(data.getLat_message()), Double.parseDouble(data.getLon_message()));
        LatLng prevLatLng = new LatLng(Double.parseDouble(data.getPre_lat_message()), Double.parseDouble(data.getPre_lon_message()));

        Location prevLocation = new Location("");
        prevLocation.setLatitude(prevLatLng.latitude);
        prevLocation.setLongitude(prevLatLng.longitude);

        Location currentLocation = new Location("");
        currentLocation.setLatitude(currentLatLng.latitude);
        currentLocation.setLongitude(currentLatLng.longitude);

        float bearing = currentLocation.bearingTo(prevLocation);

        Bitmap markerBitmap = Utils.getUtils().getScaledMarkerBitmap(this, R.drawable.bus_topview, 50, 108);

        if (busMarker == null) {
            busMarker = googleMap.addMarker(new MarkerOptions()
                            .position(currentLatLng)
                            .title(data.getVehicle_number())
//                    .snippet(data.getCreated_date())
//                    .icon(BitmapDescriptorFactory.fromBitmap(markerBitmap))
                            .icon(getBitmapDescriptor(R.drawable.ic_circle_border_24dp))
                            .flat(true)
                            /*.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                                    .flat(true)*/
                            .anchor(0.5f, 0.5f)
//                    .rotation(bearing)
            );
        }

        busMarker.setPosition(currentLatLng);
        busMarker.setTitle(data.getVehicle_number());
//        busMarker.setSnippet(data.getCreated_date());
        busMarker.setAnchor(.5f, .5f);
//        busMarker.setRotation(bearing);
//        rotateMarker(busMarker,bearing);
        zoomToLocation(currentLatLng);
    }

    @Override
    public void provideVehicleData(VehicleResponse vehicleResponse) {
        setViews(vehicleResponse);
    }

    private void setViews(VehicleResponse vehicleResponse) {
        vehicle_number = vehicleResponse.getData().getVehicleNumber();
        busNumberTv.setText(vehicleResponse.getData().getVehicleNumber());

        StringBuilder driverNames = new StringBuilder();
        for (String name : vehicleResponse.getData().getDriverName()) {
            String[] nameParts = name.split("\\.");
            if (!nameParts[0].equalsIgnoreCase("UNKNOWNDRIVER")) {
                if (driverNames.toString().isEmpty())
                    driverNames.append(nameParts[0]);
                else
                    driverNames.append(",  ").append(nameParts[0]);
            }
        }

        if (driverNames.toString().isEmpty()) {
            driversNameTv.setVisibility(View.GONE);
        }

        driversNameTv.setText(driverNames.toString());
        driverNumberTv.setVisibility(View.VISIBLE);
        driverNumberTv.setText("M: " + vehicleResponse.getData().getDriverMobile());

        driversNameTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                driversNameTv.setSelected(true);
            }
        }, 1500);

        driverNumberTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                driverNumberTv.setSelected(true);
            }
        }, 1500);

        if (vehicle_number != null)
            getLiveData();
    }

    @Override
    public void showError(String error) {
        if (error.equalsIgnoreCase("Vehicle not assigned")) {
//            vehicle_number = "KA 01 AF 5199";
//            vehicle_number = "PY 01 CP 7932";
//            vehicle_number = "PY 01 CP 7934";
//            getLiveData();
        }
        showLiveTrackAlert(error);
    }

    private void getLiveData() {
        PathParamRequest request = new PathParamRequest();
        request.setDynamicUrl(Constants.JTRACK_URL + "&vehicle_number=" + vehicle_number);
        homePresenter.getLiveTrackData(new LiveTrackDataUseCase(), request);
    }

    private void zoomToLocation(LatLng currentLocation) {
//        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 500, null);

        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(currentLocation)
                        .zoom(MarkerAnimation.zoomLevel)
                        .build()));
    }


    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    private void showLiveTrackAlert(String liveTrackContent) {


        if (isFinishing()) {
            return;
        }
        if (liveTrackDialog != null && liveTrackDialog.isShowing())
            liveTrackDialog.dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog, null);
        builder.setView(customView);
        builder.setCancelable(false);
        TextView title = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setVisibility(View.GONE);
        secondBtn.setText("OK");
        title.setText("Live Tracking Error");
        descrpt.setText(liveTrackContent);
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveTrackDialog.dismiss();
                finish();
            }
        });

        if (liveTrackDialog == null || !liveTrackDialog.isShowing())
            liveTrackDialog = builder.create();
        if (!isFinishing())
            liveTrackDialog.show();

    }

    private BitmapDescriptor getBitmapDescriptor(int id) {
        Drawable vectorDrawable = this.getResources().getDrawable(id);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bm = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (updateTrackDataHandler != null)
            updateTrackDataHandler.postDelayed(updateLiveDataRunnable, LIVE_TRACK_TIMER);
    }

    @Override
    protected void onStop() {
        super.onStop();
        busMarker = null;
        updateTrackDataHandler.removeCallbacksAndMessages(null);
    }
}