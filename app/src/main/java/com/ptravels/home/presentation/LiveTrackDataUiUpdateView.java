package com.ptravels.home.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.LiveTrackResponse;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.data.model.VehicleResponse;

/**
 * Created by Amit Tumkur on 22-02-2018.
 */

public interface LiveTrackDataUiUpdateView extends LoadingView {
    void provideLiveTrackingData(LiveTrackResponse liveTrackResponse);
    void provideVehicleData(VehicleResponse vehicleResponse);
    void showError(String error);
}
