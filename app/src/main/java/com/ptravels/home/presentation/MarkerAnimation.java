package com.ptravels.home.presentation;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.NoSuchElementException;

import static com.ptravels.home.presentation.LiveTrackBusActivity.durationInMs;
import static com.ptravels.home.presentation.LiveTrackBusActivity.queue;

/**
 * Created by Anirudh Uppunda on 27/3/18.
 */

public class MarkerAnimation {
    private static final String TAG = "MarkerAnimation";
    public static Float zoomLevel = 16f;
    private static boolean canAnimate = true;

    static void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final LatLngInterpolator latLngInterpolator, final GoogleMap googleMap) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                zoomLevel = cameraPosition.zoom;
            }
        });
        canAnimate = true;

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                LatLng moveTo = latLngInterpolator.interpolate(v, startPosition, finalPosition);
                marker.setPosition(moveTo);
                if (canAnimate && !googleMap.getProjection().getVisibleRegion().latLngBounds.contains(moveTo)) {
                    zoomToLocation(googleMap, moveTo);
                }
                if (t < 1) {
                    handler.postDelayed(this, 16);
                } else {
                    try {
                        zoomToLocation(googleMap, finalPosition);
                        handler.removeCallbacks(this);
                        if (queue.size() == 0) {
                            Log.e(TAG, "Marker anim loop ended");
                            return;
                        }
                        Log.d("LAT LONG", "CURRENT LAT LONG: " + queue.element());
                        MarkerAnimation.animateMarkerToGB(marker, queue.remove(), new Spherical(), googleMap);
                    } catch (NoSuchElementException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static void zoomToLocation(GoogleMap googleMap, LatLng currentLocation) {
        canAnimate = false;
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(currentLocation)
                        .zoom(zoomLevel)
                        .build()), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                canAnimate = true;
            }

            @Override
            public void onCancel() {
                canAnimate = true;
            }
        });
    }
}
