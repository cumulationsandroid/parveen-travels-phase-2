package com.ptravels.home.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.R;
import com.ptravels.adminPanel.data.model.FcmData;
import com.ptravels.global.Constants;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.Utils;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.FeedbackRecords;
import com.ptravels.home.data.model.Feedback_id;
import com.ptravels.home.data.model.SubmitFeedbackRequest;
import com.ptravels.home.data.model.SubmitFeedbackResponse;
import com.ptravels.home.domain.GetFeedbackOptionsUseCase;
import com.ptravels.home.domain.SubmitFeedbackUseCase;
import com.ptravels.myBooking.data.model.PnrReview;
import com.ptravels.myBooking.presentation.TicketDetailsActivity;

import java.util.ArrayList;
import java.util.List;

public class RatingReviewActivity extends AppCompatActivity implements RatingUiUpdateView{
    private TextView reviewTitle;
    private RatingBar ratingBar;
    private String rating;
    private HomePresenter homePresenter;
    private TextInputLayout commentLayout;
    private TextInputEditText commentEditText;
    private List<Feedback_id> feedback_idList = new ArrayList<>();
    private PnrReview pnrReview;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private String pnr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_review);

        /*if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY
                && getIntent().getExtras()!=null && getIntent().getExtras().getSerializable(Constants.FCM_DATA)!=null){
            onBackPressed();
            return;
        }*/

        reviewTitle = findViewById(R.id.reviewTitle);
        ratingBar = findViewById(R.id.ratingBar);
//        commentLayout = findViewById(R.id.commentlayout);
        commentEditText = findViewById(R.id.commentEt);

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        TextView title = findViewById(R.id.title);
        title.setText("Journey Feedback");

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.d("rating",""+ratingBar.getRating());
                rating = String.valueOf(ratingBar.getRating());
            }
        });

        findViewById(R.id.submitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                commentLayout.setErrorEnabled(false);
                commentEditText.setError(null);
                commentEditText.clearFocus();
                /*if (commentEditText.getText().toString().isEmpty()){
                    commentEditText.setError("Comments are compulsory!!");
                    commentEditText.requestFocus();
                    return;
                }*/

                if (rating == null || Float.valueOf(rating)<=0){
                    Toast.makeText(RatingReviewActivity.this, "Please give rating", Toast.LENGTH_SHORT).show();
                    return;
                }

                validateAndSendFeedback();
            }
        });

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        fetchFeedBackOptions();
    }

    private void fetchFeedBackOptions() {
        homePresenter = new HomePresenterImpl(this);
        homePresenter.getFeedbackOptions(new GetFeedbackOptionsUseCase(),null);
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void provideFeedbackOptions(FeedbackOptionsResponse feedbackOptionsResponse) {
        if (isFinishing() || feedbackOptionsResponse == null)
            return;
        addFeedbackLayout(feedbackOptionsResponse);
    }

    private void addFeedbackLayout(FeedbackOptionsResponse feedbackOptionsResponse) {
        LinearLayout verticalLayout = findViewById(R.id.feedBackFieldsContainer);
        if (feedbackOptionsResponse.getStatus().equalsIgnoreCase("success")){
            if (feedbackOptionsResponse.getRecords()!=null){
                List<FeedbackRecords> feedbackRecords = feedbackOptionsResponse.getRecords();

                if (feedbackRecords!=null && feedbackRecords.size()>0){
                    for (FeedbackRecords feedbackRecords1:feedbackRecords){

                        View feedbackTypeView = getLayoutInflater().inflate(R.layout.layout_review_type_row,null);

                        TextView feedbackTypeName = feedbackTypeView.findViewById(R.id.reviewTypeName);
                        final AppCompatCheckBox thumbsUp = feedbackTypeView.findViewById(R.id.thumbsUp);
                        final AppCompatCheckBox thumbsDown = feedbackTypeView.findViewById(R.id.thumbsDown);

                        feedbackTypeName.setText(feedbackRecords1.getFeedback_name());
                        /*Setting feedback_id as tag for cb*/
                        thumbsUp.setTag(feedbackRecords1.getFeedback_id());
                        thumbsDown.setTag(feedbackRecords1.getFeedback_id());

                        thumbsUp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b){
                                    thumbsDown.setChecked(!b);
                                }
                            }
                        });

                        thumbsDown.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b){
                                    thumbsUp.setChecked(!b);
                                }
                            }
                        });
                        verticalLayout.addView(feedbackTypeView);
                    }
                }
            }
        }
    }

    @Override
    public void updateSubmitFeedbackResponse(SubmitFeedbackResponse submitFeedbackResponse) {
        if (isFinishing() || submitFeedbackResponse == null)
            return;
        if (submitFeedbackResponse.getStatus().equalsIgnoreCase("success")){
            Log.d("review id",submitFeedbackResponse.getRecords().getReview_id());
            Toast.makeText(this, "Feedback submitted successfully", Toast.LENGTH_SHORT).show();

            if (pnr!=null)
                ParveenDBHelper.getParveenDBHelper(this).updatePnrReviewStatusToDb(pnr,PnrReview.COMPLETED);

            /*startActivity(new Intent(RatingReviewActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();*/
            onBackPressed();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
    }

    private void validateAndSendFeedback() {
        feedback_idList.clear();
        LinearLayout feedbackFieldsLayout = findViewById(R.id.feedBackFieldsContainer);
        for (int i = 0; i < feedbackFieldsLayout.getChildCount(); i++) {
            View feedbackFieldView = feedbackFieldsLayout.getChildAt(i);
            TextView feedbackName = feedbackFieldView.findViewById(R.id.reviewTypeName);
            AppCompatCheckBox thumbsUp = feedbackFieldView.findViewById(R.id.thumbsUp);
            AppCompatCheckBox thumbsDown = feedbackFieldView.findViewById(R.id.thumbsDown);
            String feeBackValue = feedbackName.getText().toString().trim();

            Feedback_id feedback_id = new Feedback_id();
            feedback_id.setId(String.valueOf(thumbsUp.getTag()));
            String feedbVal = thumbsUp.isChecked()? "Y" : (thumbsDown.isChecked()?"N":"");
            feedback_id.setValue(feedbVal);
            if (!feedbVal.isEmpty())
                feedback_idList.add(feedback_id);
        }

        /*if (feedback_idList.size() == 0){
            Toast.makeText(RatingReviewActivity.this, "Please vote at least 1 field", Toast.LENGTH_SHORT).show();
            return;
        }*/

        if (getIntent()!=null) {
            if (getIntent().getSerializableExtra(Constants.PNR_REVIEW_MODEL)!=null) {
                pnrReview = (PnrReview) getIntent().getSerializableExtra(Constants.PNR_REVIEW_MODEL);
                pnr = pnrReview.getPnr();
            } else if (getIntent().getExtras()!=null && getIntent().getExtras().getSerializable(Constants.FCM_DATA)!=null){
                FcmData fcmData = (FcmData) getIntent().getExtras().getSerializable(Constants.FCM_DATA);
                pnr = fcmData.getPnr();
            }

            SubmitFeedbackRequest request = new SubmitFeedbackRequest();
            request.setPnr(pnr);
            request.setComment(commentEditText.getText().toString().trim());
            request.setRating(rating);
            request.setFeedback_id(feedback_idList);

            homePresenter.sendFeedback(new SubmitFeedbackUseCase(),request);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RatingReviewActivity.this, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }
}
