package com.ptravels.home.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.home.data.model.FeedbackOptionsResponse;
import com.ptravels.home.data.model.SubmitFeedbackResponse;

/**
 * Created by Amit Tumkur on 22-02-2018.
 */

public interface RatingUiUpdateView extends LoadingView {
    void provideFeedbackOptions(FeedbackOptionsResponse feedbackOptionsResponse);
    void updateSubmitFeedbackResponse(SubmitFeedbackResponse submitFeedbackResponse);
    void showError(String error);
}
