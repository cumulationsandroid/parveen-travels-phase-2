package com.ptravels.home.presentation;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.ptravels.global.Utils;
import com.ptravels.home.data.model.Directions;
import com.ptravels.home.data.model.DirectionsRequest;
import com.ptravels.home.domain.GetDirectionsUseCase;

import java.util.ArrayList;
import java.util.List;

import rx.SingleSubscriber;

/**
 * Created by Anirudh Uppunda on 28/3/18.
 */

public class getDirectionsPresenterImpl implements GetDirectionsPresenter {

    private DirectionsUiUpdateView directionsUiUpdateView;
    private GetDirectionsUseCase directionsUseCase;
    private LatLng currentLatLng;
    private LatLng previousLatLng;

    private List<LatLng> latlngList;

    public getDirectionsPresenterImpl(DirectionsUiUpdateView directionsUiUpdateView, GetDirectionsUseCase directionsUseCase, LatLng previousLatLng, LatLng currentLatLng) {
        this.directionsUiUpdateView = directionsUiUpdateView;
        this.directionsUseCase = directionsUseCase;
        this.previousLatLng = previousLatLng;
        this.currentLatLng = currentLatLng;
    }

    @Override
    public void getDirectionsData() {
        if (Utils.isOnline()) {
            directionsUseCase.execute(createDirectionRequest())
                    .subscribe(new SingleSubscriber<Directions>() {
                        @Override
                        public void onSuccess(Directions value) {
                            latlngList = new ArrayList<LatLng>();
                            if (value.getRoutes() == null || value.getRoutes().size() == 0) {
                                latlngList = new ArrayList<LatLng>();
                                //latlngList.add(previousLatLng);
                                latlngList.add(currentLatLng);
                                float[] distance = new float[1];
                                try {
                                    Location.distanceBetween(previousLatLng.latitude, previousLatLng.longitude, currentLatLng.latitude, currentLatLng.longitude, distance);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    distance[0] = 0;
                                }
                                directionsUiUpdateView.provideDirections(latlngList, (int) distance[0]);
                                return;
                            }
                            String encoder = value.getRoutes().get(0).getOverviewPolyline().getPoints();
                            latlngList = decodePoly(encoder);
                            directionsUiUpdateView.provideDirections(latlngList, value.getRoutes().get(0).getLegs().get(0).getDistance().getValue());
                        }

                        @Override
                        public void onError(Throwable error) {
                            directionsUiUpdateView.ShowError(error.getLocalizedMessage());
                        }
                    });
        }
    }

    private DirectionsRequest createDirectionRequest() {
        DirectionsRequest directionsRequest = new DirectionsRequest();
        directionsRequest.setMode("Driving");
        directionsRequest.setOrigin(previousLatLng.latitude + "," + previousLatLng.longitude);
        directionsRequest.setDestination(currentLatLng.latitude + "," + currentLatLng.longitude);
        directionsRequest.setUnit("metric");
        directionsRequest.setKey("AIzaSyDV3FSfzj9S3xdAnFIirQKBuayC4HsPFNc");
        return directionsRequest;
    }

    /**
     * Method to decode polyline points
     * Courtesy : jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}
