package com.ptravels.myBooking.data.model;

import java.io.Serializable;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class PnrReview implements Serializable{
    private String pnr;
    private String accessToken;
    private String frmCity;
    private String toCity;
    private String travelDate;
    private String bookingDate;
    private String email;
    private String mobile;
    private String bookingStatus;
    private String reviewStatus;
    private boolean serverPresent;
    private String price;
    private String seats;
    private String bookingId;

    public static final String BOOKED = "booked";
    public static final String BOOKING_CANCELLED = "booking_cancelled";
    public static final String PAYMENT_CANCELLED = "payment_cancelled";
    public static final String PAYMENT_FAILED = "payment_failed";
    public static final String BOOKING_FAILED = "booking_failed";
    public static final String COMPLETED = "completed";
    public static final String PENDING = "pending";

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public boolean isServerPresent() {
        return serverPresent;
    }

    public void setServerPresent(boolean serverPresent) {
        this.serverPresent = serverPresent;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setFrmCity(String frmCity) {
        this.frmCity = frmCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public String getPnr() {
        return pnr;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getFrmCity() {
        return frmCity;
    }

    public String getToCity() {
        return toCity;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }
}
