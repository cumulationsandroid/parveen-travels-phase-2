package com.ptravels.myBooking.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cumulations on 3/1/18.
 */

public class CancelTicketRequest
{
    @SerializedName("pnr")
    @Expose
    private String pnr;
    @SerializedName("seatIds")
    @Expose
    private List<String> seatIds = null;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public List<String> getSeatIds() {
        return seatIds;
    }

    public void setSeatIds(List<String> seatIds) {
        this.seatIds = seatIds;
    }

}
