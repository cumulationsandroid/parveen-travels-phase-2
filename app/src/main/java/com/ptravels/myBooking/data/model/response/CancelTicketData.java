package com.ptravels.myBooking.data.model.response;

import java.util.List;

public class CancelTicketData {
	private List<Integer> seatIds;
	private String pnr;

	public void setSeatIds(List<Integer> seatIds){
		this.seatIds = seatIds;
	}

	public List<Integer> getSeatIds(){
		return seatIds;
	}

	public void setPnr(String pnr){
		this.pnr = pnr;
	}

	public String getPnr(){
		return pnr;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"seatIds = '" + seatIds + '\'' + 
			",pnr = '" + pnr + '\'' + 
			"}";
		}
}