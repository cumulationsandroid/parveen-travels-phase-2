package com.ptravels.myBooking.data.model.response;

import com.ptravels.global.ResponseError;

public class HistoryResponse{
	private HistoryData data;
	private boolean success;
	private ResponseError error;

	public ResponseError getError() {
		return error;
	}

	public void setError(ResponseError error) {
		this.error = error;
	}

	public void setData(HistoryData data){
		this.data = data;
	}

	public HistoryData getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"HistoryResponse{" + 
			"data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			"}";
		}
}
