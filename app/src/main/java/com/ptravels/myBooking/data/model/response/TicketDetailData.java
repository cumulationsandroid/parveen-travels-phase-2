package com.ptravels.myBooking.data.model.response;

import java.util.List;

public class TicketDetailData {
	private String busDepTime;
	private TicketDetailDroppingPoint droppingPoint;
	private String mobile;
	private String operatorName;
	private String fromStation;
	private List<TicketDetailCancelCharge> cancelCharges;
	private String toStation;
	private String droppingPointTime;
	private String pnr;
	private String travelDate;
	private String boardingDate;
	private String ticketStatus;
	private List<TicketDetailPassengerDetail> passengerDetails;
	private TicketDetailBoardingPoint boardingPoint;
	private String busType;
	private List<String> cancelTerms;
	private String boardingPointTime;
	private String email;
	private String operatorMobile;

	public void setBusDepTime(String busDepTime){
		this.busDepTime = busDepTime;
	}

	public String getBusDepTime(){
		return busDepTime;
	}

	public void setDroppingPoint(TicketDetailDroppingPoint droppingPoint){
		this.droppingPoint = droppingPoint;
	}

	public TicketDetailDroppingPoint getDroppingPoint(){
		return droppingPoint;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setOperatorName(String operatorName){
		this.operatorName = operatorName;
	}

	public String getOperatorName(){
		return operatorName;
	}

	public void setFromStation(String fromStation){
		this.fromStation = fromStation;
	}

	public String getFromStation(){
		return fromStation;
	}

	public void setCancelCharges(List<TicketDetailCancelCharge> cancelCharges){
		this.cancelCharges = cancelCharges;
	}

	public List<TicketDetailCancelCharge> getCancelCharges(){
		return cancelCharges;
	}

	public void setToStation(String toStation){
		this.toStation = toStation;
	}

	public String getToStation(){
		return toStation;
	}

	public void setDroppingPointTime(String droppingPointTime){
		this.droppingPointTime = droppingPointTime;
	}

	public String getDroppingPointTime(){
		return droppingPointTime;
	}

	public void setPnr(String pnr){
		this.pnr = pnr;
	}

	public String getPnr(){
		return pnr;
	}

	public void setTravelDate(String travelDate){
		this.travelDate = travelDate;
	}

	public String getTravelDate(){
		return travelDate;
	}

	public void setBoardingDate(String boardingDate){
		this.boardingDate = boardingDate;
	}

	public String getBoardingDate(){
		return boardingDate;
	}

	public void setTicketStatus(String ticketStatus){
		this.ticketStatus = ticketStatus;
	}

	public String getTicketStatus(){
		return ticketStatus;
	}

	public void setPassengerDetails(List<TicketDetailPassengerDetail> passengerDetails){
		this.passengerDetails = passengerDetails;
	}

	public List<TicketDetailPassengerDetail> getPassengerDetails(){
		return passengerDetails;
	}

	public void setBoardingPoint(TicketDetailBoardingPoint boardingPoint){
		this.boardingPoint = boardingPoint;
	}

	public TicketDetailBoardingPoint getBoardingPoint(){
		return boardingPoint;
	}

	public void setBusType(String busType){
		this.busType = busType;
	}

	public String getBusType(){
		return busType;
	}

	public void setCancelTerms(List<String> cancelTerms){
		this.cancelTerms = cancelTerms;
	}

	public List<String> getCancelTerms(){
		return cancelTerms;
	}

	public void setBoardingPointTime(String boardingPointTime){
		this.boardingPointTime = boardingPointTime;
	}

	public String getBoardingPointTime(){
		return boardingPointTime;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setOperatorMobile(String operatorMobile){
		this.operatorMobile = operatorMobile;
	}

	public String getOperatorMobile(){
		return operatorMobile;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"busDepTime = '" + busDepTime + '\'' + 
			",droppingPoint = '" + droppingPoint + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",operatorName = '" + operatorName + '\'' + 
			",fromStation = '" + fromStation + '\'' + 
			",cancelCharges = '" + cancelCharges + '\'' + 
			",toStation = '" + toStation + '\'' + 
			",droppingPointTime = '" + droppingPointTime + '\'' + 
			",pnr = '" + pnr + '\'' + 
			",travelDate = '" + travelDate + '\'' + 
			",boardingDate = '" + boardingDate + '\'' + 
			",ticketStatus = '" + ticketStatus + '\'' + 
			",passengerDetails = '" + passengerDetails + '\'' + 
			",boardingPoint = '" + boardingPoint + '\'' + 
			",busType = '" + busType + '\'' + 
			",cancelTerms = '" + cancelTerms + '\'' + 
			",boardingPointTime = '" + boardingPointTime + '\'' + 
			",email = '" + email + '\'' + 
			",operatorMobile = '" + operatorMobile + '\'' + 
			"}";
		}
}