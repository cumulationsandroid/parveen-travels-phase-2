package com.ptravels.myBooking.domain;

import com.ptravels.global.UseCase;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.myBooking.data.source.MyBookingDataSourceImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by cumulations on 3/1/18.
 */

public class TicketDetailUseCase extends UseCase<PathParamRequest,TicketDetailResponse> {

  private MyBookingDataSourceImpl myBookingDataSource =new MyBookingDataSourceImpl();

    @Override
    public Single<TicketDetailResponse> buildUseCase(PathParamRequest request) {
        return myBookingDataSource.getTicketDetails(request);
    }
}
