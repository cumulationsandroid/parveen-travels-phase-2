package com.ptravels.myBooking.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.ParveenApp;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.adminPanel.presentation.AdminPanelAPIsService;
import com.ptravels.bookTicket.presentation.PaymentDetailsActivity;
import com.ptravels.global.Constants;
import com.ptravels.global.ParveenDBHelper;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.home.presentation.LiveTrackBusActivity;
import com.ptravels.myBooking.data.model.PnrReview;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.HistoryTicket;
import com.ptravels.myBooking.data.model.response.TicketDetailCancelCharge;
import com.ptravels.myBooking.data.model.response.TicketDetailData;
import com.ptravels.myBooking.data.model.response.TicketDetailPassengerDetail;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.myBooking.domain.CancelTicketUseCase;
import com.ptravels.myBooking.domain.TicketDetailUseCase;
import com.ptravels.seatlayout.presentation.BottomSheetDialogCancelTicketListener;
import com.ptravels.seatlayout.presentation.CancellationPolicyFragment;
import com.ptravels.splash.presentation.SplashActivity;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TicketDetailsActivity extends AppCompatActivity implements MyBookingView,BottomSheetDialogCancelTicketListener {
    private FrameLayout loaderLayout;
    private ImageView loader;
    private TextView tripTo,pnrNo,ticketId,seatNo,fromCity,toCity,brPt,drPt,
            travelDateTime, boardingTimeTv,busType,userName,phoneNo,bookingStatus,seatFare, cgst,sgst,totalFare, totalCancelFare;
    private TicketDetailData currentClickedTicket;
    private List<String> currentBookedTicketSeatIds = new ArrayList<>();
    private AlertDialog liveTrackDialog;
    private String liveTrackContent;
    private TextView liveTrackTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_details);
        initViews();
        loadTicketDetails();
    }

    private void loadTicketDetails() {
        if (getIntent()!=null){
            if (getIntent().getSerializableExtra(Constants.HISTORY_TICKET)!=null){
                HistoryTicket historyTicket = (HistoryTicket) getIntent().getSerializableExtra(Constants.HISTORY_TICKET);
                getTicketDetails(historyTicket);
            }

            if (getIntent().getStringExtra(ParveenDBHelper.PNR)!=null && !getIntent().getStringExtra(ParveenDBHelper.PNR).isEmpty()){
                getTicketDetails(getIntent().getStringExtra(ParveenDBHelper.PNR));
            }
        }
    }

    private void initViews() {
        TextView title = findViewById(R.id.title);
        tripTo = findViewById(R.id.tripToText);
        pnrNo = findViewById(R.id.pnrNo);
        ticketId = findViewById(R.id.ticketId);
        seatNo = findViewById(R.id.seatNoTv);
        fromCity = findViewById(R.id.fromCity);
        brPt = findViewById(R.id.boardingPoint);
        toCity = findViewById(R.id.toCity);
        drPt = findViewById(R.id.droppingPoint);
        travelDateTime = findViewById(R.id.travelDateTime);
        boardingTimeTv = findViewById(R.id.boardingTime);
        busType = findViewById(R.id.busType);
        userName = findViewById(R.id.userName);
        phoneNo = findViewById(R.id.phoneNo);
        bookingStatus = findViewById(R.id.bookingStatus);
        seatFare = findViewById(R.id.seatFare);
        cgst = findViewById(R.id.cgst);
        sgst = findViewById(R.id.sgst);
        totalFare = findViewById(R.id.totalFare);
        totalCancelFare = findViewById(R.id.totalCancelCharges);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);
        liveTrackTv = findViewById(R.id.liveTrackText);

        title.setText("Ticket Details");

        findViewById(R.id.cancelTicketBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showCancelTicketAlert();
                CancellationPolicyFragment cancellationPolicyFragment = CancellationPolicyFragment.showCancelBtn(true);
                cancellationPolicyFragment.show(getSupportFragmentManager(),CancellationPolicyFragment.class.getSimpleName());
                cancellationPolicyFragment.setDialogCancelTicketListener(TicketDetailsActivity.this);
            }
        });

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestClient.enableMockApi = false;
                onBackPressed();
            }
        });

        findViewById(R.id.info_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancellationPolicyFragment bottomSheetDialogFragment = new CancellationPolicyFragment();
                bottomSheetDialogFragment.show(getSupportFragmentManager(), CancellationPolicyFragment.class.getSimpleName());
            }
        });

        findViewById(R.id./*liveTrackButton*/liveTrackText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (liveTrackContent!=null && !liveTrackContent.isEmpty() && liveTrackContent.equals(getString(R.string.liveTrackNotAvailable))){
                    showLiveTrackAlert(liveTrackContent);
                    return;
                }
                if (currentClickedTicket!=null){
                    startActivity(new Intent(TicketDetailsActivity.this, LiveTrackBusActivity.class)
                            .putExtra(ParveenDBHelper.PNR, currentClickedTicket.getPnr())
                            .putExtra(Constants.FROM_STATION_NAME, currentClickedTicket.getFromStation())
                            .putExtra(Constants.TO_STATION_NAME, currentClickedTicket.getToStation()));
                }
            }
        });
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void updateMyBookingData(HistoryResponse historyResponse) {

    }

    @Override
    public void updateCanelTicketUi(CancelTicketResponse cancelTicketResponse) {
        if (cancelTicketResponse.isSuccess()) {
            if (cancelTicketResponse.getData().getPnr().equals(currentClickedTicket.getPnr())) {
                Toast.makeText(this, "Ticket cancelled successfully", Toast.LENGTH_SHORT).show();
                RestClient.enableMockApi = false;
                updateBookingStatusToAdminPanel();
                if (getIntent().getStringExtra(Constants.FROM_ACTIVITY)!=null
                        && getIntent().getStringExtra(Constants.FROM_ACTIVITY).equalsIgnoreCase(MyBookingActivity.class.getSimpleName())){
                    startActivity(new Intent(this, MyBookingActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                } else
                    finish();
            }
        } else if (cancelTicketResponse.getError() != null) {
            String error = RestClient.get200StatusErrors(cancelTicketResponse.getError());
            Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
            if (error.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void showError(String error) {
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
            startActivity(new Intent(this, SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else if (error.equalsIgnoreCase("ticket.invalid")){
            Toast.makeText(this, "Invalid PNR", Toast.LENGTH_LONG).show();
            onBackPressed();
        } else {
            Toast.makeText(this, ""+ error, Toast.LENGTH_SHORT).show();
            if (error.equals(Constants.UNKNOWN_ERROR)){
                onBackPressed();
            }
        }
    }

    @Override
    public void provideTicketDetails(TicketDetailResponse response) {
        if (!isFinishing()) {
            if (response == null)
                return;
            setViews(response);
        }
    }

    private void setViews(TicketDetailResponse response) {
        if (response == null || response.getData() == null) {
            return;
        }
        TicketDetailData data = response.getData();
        currentClickedTicket = data;

        PnrReview pnrReview = new PnrReview();
        pnrReview.setAccessToken(ParveenApp.getAccessToken());
        pnrReview.setBookingStatus(data.getTicketStatus());
        pnrReview.setReviewStatus(PnrReview.PENDING);
        pnrReview.setServerPresent(false);

        tripTo.setText("Trip to "+data.getToStation());
        pnrNo.setText("PNR No. : "+data.getPnr());
        pnrReview.setPnr(data.getPnr());

        ticketId.setText("Ticket ID : "+data.getPnr());

        fromCity.setText(data.getFromStation());
        toCity.setText(data.getToStation());
        pnrReview.setFrmCity(data.getFromStation());
        pnrReview.setToCity(data.getToStation());

        brPt.setText(data.getBoardingPoint().getStationPointName());
        drPt.setText(data.getDroppingPoint().getStationPointName());

//        String boardingTimeTv = Utils.formatDateString("HH:mm:ss", "h:mm a", data.getBusDepTime());
        String boardingTime = Utils.formatDateString("HH:mm:ss", "h:mm a", data.getBoardingPointTime());
        String boardingDate = Utils.formatDateString("yyyy-MM-dd", "MMM dd, yyyy", data.getBoardingDate());
        travelDateTime.setText(boardingDate/*+" - "+boardingTimeTv*/);
        boardingTimeTv.setText(boardingTime);
        pnrReview.setTravelDate(data.getTravelDate()+" "+data.getBusDepTime());

        busType.setText(data.getBusType());

        String primaryPassengerName = data.getPassengerDetails().get(0).getName();
        userName.setText(primaryPassengerName);
        phoneNo.setText(data.getMobile());
        pnrReview.setEmail(data.getEmail());
        pnrReview.setMobile(data.getMobile());

        bookingStatus.setText(data.getTicketStatus().toUpperCase());

        if (bookingStatus.getText().toString().equalsIgnoreCase("cancelled")
                || bookingStatus.getText().toString().equalsIgnoreCase("completed")) {
            findViewById(R.id.cancelTicketBtn).setVisibility(View.GONE);
//            findViewById(R.id.liveTrackButton).setVisibility(View.GONE);
        }

        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String tDate = data.getTravelDate()+" "+data.getBusDepTime();
        try {
            Date travelDate = sdf.parse(tDate);
            if (travelDate.compareTo(currentDate)<0){
                findViewById(R.id.cancelTicketBtn).setVisibility(View.GONE);
//                findViewById(R.id.liveTrackButton).setVisibility(View.VISIBLE);
            }
            long diff = currentDate.getTime() - travelDate.getTime();
            long dayInMillis = 24*60*60*1000;
            if (diff>dayInMillis){
                liveTrackContent = getString(R.string.liveTrackNotAvailable);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (getIntent().hasExtra(Constants.FROM_ACTIVITY)
                && getIntent().getStringExtra(Constants.FROM_ACTIVITY)!=null
                && !getIntent().getStringExtra(Constants.FROM_ACTIVITY).isEmpty()
                && getIntent().getStringExtra(Constants.FROM_ACTIVITY).equals(PaymentDetailsActivity.class.getSimpleName())){
            findViewById(R.id.cancelTicketBtn).setVisibility(View.GONE);
        }

        double totalSTax = 0, totalCancelAmnt = 0;
        for (TicketDetailCancelCharge detailCancelCharge : data.getCancelCharges()){
            totalCancelAmnt = totalCancelAmnt+detailCancelCharge.getCancelCharge();
            totalSTax = totalSTax+detailCancelCharge.getServiceTaxAmount();
        }

        /*if (data.getTicketStatus().equalsIgnoreCase("CANCELLED")){
            totalCancelFare.setVisibility(View.VISIBLE);
            totalCancelFare.setText(totalCancelAmnt+" \u20B9");
        } else {
            totalCancelFare.setVisibility(View.GONE);
        }*/

        totalCancelFare.setText(totalCancelAmnt+" \u20B9");

        StringBuilder seatNosBuilder = new StringBuilder();
        double totalSeatFareAmnt = 0;
        for (TicketDetailPassengerDetail passengerDetail : data.getPassengerDetails()){
            if (!passengerDetail.getTicketStatus().equalsIgnoreCase("CANCELLED")
                    && !currentBookedTicketSeatIds.contains(String.valueOf(passengerDetail.getSeatId())))
                currentBookedTicketSeatIds.add(String.valueOf(passengerDetail.getSeatId()));
            if (seatNosBuilder.toString().isEmpty())
                seatNosBuilder.append(passengerDetail.getSeatNbr());
            else
                seatNosBuilder.append(", ").append(passengerDetail.getSeatNbr());
            totalSeatFareAmnt = totalSeatFareAmnt + passengerDetail.getSeatFare();
        }

        double totalFareAmnt = totalSeatFareAmnt+totalSTax;

        seatNo.setText(seatNosBuilder.toString());
        pnrReview.setSeats(String.valueOf(data.getPassengerDetails().size()));

        seatFare.setText(String.valueOf(totalSeatFareAmnt)+" \u20B9");
        cgst.setText(String.valueOf(totalSTax/2)+" \u20B9");
        sgst.setText(String.valueOf(totalSTax/2)+" \u20B9");
        totalFare.setText(String.valueOf(totalFareAmnt)+" \u20B9");
//        totalCancelFare.setText(String.valueOf(totalCancelAmnt)+" \u20B9");
        pnrReview.setPrice(String.valueOf(totalFareAmnt));

        fromCity.postDelayed(new Runnable() {
            @Override
            public void run() {
                fromCity.setSelected(true);
            }
        },1500);

        toCity.postDelayed(new Runnable() {
            @Override
            public void run() {
                toCity.setSelected(true);
            }
        },1500);

        brPt.postDelayed(new Runnable() {
            @Override
            public void run() {
                brPt.setSelected(true);
            }
        },1500);

        drPt.postDelayed(new Runnable() {
            @Override
            public void run() {
                drPt.setSelected(true);
            }
        },1500);

        /*if (!ParveenDBHelper.getParveenDBHelper(this).isPnrServerPresent(pnrReview.getPnr())) {

            ParveenDBHelper.getParveenDBHelper(this).addPNRToDb(pnrReview);

            startService(new Intent(this, AdminPanelAPIsService.class)
                    .putExtra(Constants.API_TYPE, Constants.SEND_BOOKING_DATA)
                    .putExtra(Constants.PNR_REVIEW_MODEL, pnrReview));
        }*/
    }

    public void getTicketDetails(HistoryTicket ticket) {
        currentBookedTicketSeatIds.clear();
        PathParamRequest request = new PathParamRequest();
        request.setPnr(ticket.getPnr());
        request.setToken(ParveenApp.getAccessToken());
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
//        RestClient.enableMockApi = true;
        myBookingPresenter.getTicketDetails(new TicketDetailUseCase(), request);
    }

    public void getTicketDetails(String pnr) {
        currentBookedTicketSeatIds.clear();
        PathParamRequest request = new PathParamRequest();
        request.setPnr(pnr);
        request.setToken(ParveenApp.getAccessToken());
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
        myBookingPresenter.getTicketDetails(new TicketDetailUseCase(), request);
    }

    public void showCancelTicketAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please confirm to cancel the ticket.");
        builder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                RestClient.enableMockApi = true;
                cancelTicket();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void cancelTicket() {
        if (currentClickedTicket == null)
            return;
        CancelTicketRequest cancelTicketRequest = new CancelTicketRequest();
        cancelTicketRequest.setPnr(currentClickedTicket.getPnr());
        cancelTicketRequest.setSeatIds(currentBookedTicketSeatIds);
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
        myBookingPresenter.cancelTicket(new CancelTicketUseCase(), cancelTicketRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        RestClient.enableMockApi = false;
    }

    @Override
    public void onBackPressed() {
        RestClient.enableMockApi = false;
        if (getIntent().hasExtra(Constants.FROM_ACTIVITY)
                && getIntent().getStringExtra(Constants.FROM_ACTIVITY)!=null
            && !getIntent().getStringExtra(Constants.FROM_ACTIVITY).isEmpty()
            && getIntent().getStringExtra(Constants.FROM_ACTIVITY).equals(PaymentDetailsActivity.class.getSimpleName())){
            startActivity(new Intent(TicketDetailsActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void updateBookingStatusToAdminPanel(){
        if (ParveenDBHelper.getParveenDBHelper(this).isPnrServerPresent(currentClickedTicket.getPnr())) {
            startService(new Intent(this, AdminPanelAPIsService.class)
                    .putExtra(Constants.API_TYPE, Constants.UPDATE_BOOKING_DATA)
                    .putExtra(ParveenDBHelper.PNR, currentClickedTicket.getPnr())
                    .putExtra(ParveenDBHelper.BOOKING_STATUS, PnrReview.BOOKING_CANCELLED));
        }
    }

    @Override
    public void bottomSheetDialogCancelTicket() {
        cancelTicket();
    }

    private void showLiveTrackAlert(String liveTrackContent) {

        if (liveTrackDialog !=null && liveTrackDialog.isShowing())
            liveTrackDialog.dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View customView = getLayoutInflater().inflate(R.layout.custom_dialog,null);
        builder.setView(customView);
        builder.setCancelable(false);
        TextView title = customView.findViewById(R.id.dialogTitle);
        TextView descrpt = customView.findViewById(R.id.dialogDescription);
        AppCompatButton firstBtn = customView.findViewById(R.id.firstBtn);
        AppCompatButton secondBtn = customView.findViewById(R.id.secondBtn);

        firstBtn.setVisibility(View.GONE);
        secondBtn.setText("OK");
        title.setText("Live Tracking");
        descrpt.setText(liveTrackContent);
        secondBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveTrackDialog.dismiss();
            }
        });

        if (liveTrackDialog == null || !liveTrackDialog.isShowing())
            liveTrackDialog = builder.create();
        liveTrackDialog.show();

    }
}
