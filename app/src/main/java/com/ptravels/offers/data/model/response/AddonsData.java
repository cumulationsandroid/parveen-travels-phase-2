package com.ptravels.offers.data.model.response;

import java.util.List;

/**
 * Created by Amit Tumkur on 26-02-2018.
 */

public class AddonsData {
    private List<FemaleDiscount> FD;

    private List<CDiscount> CD;

    private List<NormalDiscount> ND;

    private String GG;

    public List<FemaleDiscount> getFD() {
        return FD;
    }

    public void setFD(List<FemaleDiscount> FD) {
        this.FD = FD;
    }

    public List<CDiscount> getCD() {
        return CD;
    }

    public void setCD(List<CDiscount> CD) {
        this.CD = CD;
    }

    public List<NormalDiscount> getND() {
        return ND;
    }

    public void setND(List<NormalDiscount> ND) {
        this.ND = ND;
    }

    public String getGG() {
        return GG;
    }

    public void setGG(String GG) {
        this.GG = GG;
    }
}
