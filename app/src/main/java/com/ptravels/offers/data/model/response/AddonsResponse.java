package com.ptravels.offers.data.model.response;

public class AddonsResponse
{
    private String message;

    private AddonsData data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public AddonsData getData ()
    {
        return data;
    }

    public void setData (AddonsData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}