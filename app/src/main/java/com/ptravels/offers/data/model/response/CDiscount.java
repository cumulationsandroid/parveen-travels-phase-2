package com.ptravels.offers.data.model.response;

/**
 * Created by Amit Tumkur on 26-02-2018.
 */

public class CDiscount {
    private String travelFlag;

    private String fromDate;

    private String addonType;

    private String toStationId;

    private String toDate;

    private String couponValue;

    private String flatFlag;

    private String fromStationId;

    public String getTravelFlag() {
        return travelFlag;
    }

    public void setTravelFlag(String travelFlag) {
        this.travelFlag = travelFlag;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getAddonType() {
        return addonType;
    }

    public void setAddonType(String addonType) {
        this.addonType = addonType;
    }

    public String getToStationId() {
        return toStationId;
    }

    public void setToStationId(String toStationId) {
        this.toStationId = toStationId;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(String couponValue) {
        this.couponValue = couponValue;
    }

    public String getFlatFlag() {
        return flatFlag;
    }

    public void setFlatFlag(String flatFlag) {
        this.flatFlag = flatFlag;
    }

    public String getFromStationId() {
        return fromStationId;
    }

    public void setFromStationId(String fromStationId) {
        this.fromStationId = fromStationId;
    }
}
