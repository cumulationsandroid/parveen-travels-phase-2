package com.ptravels.offers.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.offers.data.model.response.AddonsResponse;
import com.ptravels.offers.domain.OffersDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class OffersDataImpl implements OffersDataInterface {

    @Override
    public Single<AddonsResponse> getAddons() {
        return RestClient.getApiService().getAddonsList(ParveenApp.getAccessToken())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
