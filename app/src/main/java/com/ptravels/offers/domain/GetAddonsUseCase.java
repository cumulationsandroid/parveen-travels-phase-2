package com.ptravels.offers.domain;

import com.ptravels.global.UseCase;
import com.ptravels.offers.data.model.response.AddonsResponse;
import com.ptravels.offers.data.source.OffersDataImpl;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class GetAddonsUseCase extends UseCase<PathParamRequest,AddonsResponse> {
    private OffersDataInterface offersDataInterface = new OffersDataImpl();

    @Override
    public Single<AddonsResponse> buildUseCase(PathParamRequest request) {
        return offersDataInterface.getAddons();
    }
}
