package com.ptravels.offers.domain;

import com.ptravels.offers.data.model.response.AddonsResponse;

import rx.Single;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public interface OffersDataInterface {
    Single<AddonsResponse> getAddons();
}
