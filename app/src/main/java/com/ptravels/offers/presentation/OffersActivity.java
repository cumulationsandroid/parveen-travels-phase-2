package com.ptravels.offers.presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.R;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.offers.data.model.response.AddonsResponse;
import com.ptravels.offers.data.model.response.NormalDiscount;
import com.ptravels.offers.domain.GetAddonsUseCase;
import com.ptravels.splash.presentation.SplashActivity;

import java.util.ArrayList;

public class OffersActivity extends AppCompatActivity implements OffersUiUpdateView {
    private OffersListAdapter offersListAdapter;
    private RecyclerView recyclerView;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private TextView emptyTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);

        TextView title = findViewById(R.id.title);
        title.setText("Offers");
        recyclerView = findViewById(R.id.recyclerView);
        emptyTv = findViewById(R.id.emptyTv);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        offersListAdapter = new OffersListAdapter(this, new ArrayList<NormalDiscount>());
        recyclerView.setAdapter(offersListAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchAddonsList();
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void fetchAddonsList(){
        OffersPresenter offersPresenter = new OffersPresenterImpl(this);
        offersPresenter.getAddonsFromServer(new GetAddonsUseCase(),null);
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void provideAddonsList(AddonsResponse response) {
        if (isFinishing() || response == null)
            return;
        if (response.getMessage().equalsIgnoreCase("Sucess")){
            if (response.getData().getND()!=null){
                if (response.getData().getND().size() == 0){
                    emptyTv.setVisibility(View.VISIBLE);
                    return;
                }
                emptyTv.setVisibility(View.GONE);
                offersListAdapter.refreshItems(response.getData().getND());
            } else {
                emptyTv.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, "error: " + error, Toast.LENGTH_SHORT).show();
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
            startActivity(new Intent(this, SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }
}
