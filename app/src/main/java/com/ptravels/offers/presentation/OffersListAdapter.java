package com.ptravels.offers.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.offers.data.model.response.NormalDiscount;

import java.util.List;

public class OffersListAdapter extends RecyclerView.Adapter<OffersListAdapter.OffersListViewHolder> {

    private Context context;
    private List<NormalDiscount> normalDiscountList;

    public OffersListAdapter(Context context, List<NormalDiscount> normalDiscountList) {
        this.context = context;
        this.normalDiscountList = normalDiscountList;
    }

    @Override
    public OffersListViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_offers, parent, false);
        return new OffersListViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final OffersListViewHolder offersListViewHolder, int position) {
        NormalDiscount ND = normalDiscountList.get(position);
        if (!ND.isFlatFlag()) {
            offersListViewHolder.couponValueTv.setText("Up to " + ND.getCouponValue() + " % OFF");
        } else {
            offersListViewHolder.couponValueTv.setText("Up to " + ND.getCouponValue() + " Rs OFF");
        }
        offersListViewHolder.discountCodeTv.setText("Code - "+ND.getDiscountCode());

//        if (ND.isTravelFlag()) {
            offersListViewHolder.validDateTv.setVisibility(View.VISIBLE);
            String date = Utils.formatDateString("yyyy-MM-dd","dd MMM, yyyy", ND.getToDate());
            offersListViewHolder.validDateTv.setText("Valid up to "+date);
//        } else {
//            offersListViewHolder.validDateTv.setVisibility(View.GONE);
//        }

        offersListViewHolder.discountCodeTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                offersListViewHolder.discountCodeTv.setSelected(true);
            }
        },1500);

        offersListViewHolder.validDateTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                offersListViewHolder.validDateTv.setSelected(true);
            }
        },1500);

        if (ND.getFromStationId().equalsIgnoreCase("0"))
            offersListViewHolder.frmCityTv.setVisibility(View.GONE);
        if (ND.getToStationId().equalsIgnoreCase("0"))
            offersListViewHolder.toCityTv.setVisibility(View.GONE);

        offersListViewHolder.frmCityTv.setText("From : "+Utils.getUtils().getStationName(context,ND.getFromStationId()));
        offersListViewHolder.toCityTv.setText("To : "+Utils.getUtils().getStationName(context,ND.getToStationId()));
    }

    @Override
    public int getItemCount() {
        return normalDiscountList.size();
    }

    public static class OffersListViewHolder extends RecyclerView.ViewHolder {

        LinearLayout parentLayout;
        TextView couponValueTv, discountCodeTv, validDateTv, frmCityTv,toCityTv;

        public OffersListViewHolder(View itemView) {
            super(itemView);
            parentLayout = itemView.findViewById(R.id.parentLayout);
            couponValueTv =  itemView.findViewById(R.id.couponValueTv);
            discountCodeTv = itemView.findViewById(R.id.discountCodeTv);
            validDateTv = itemView.findViewById(R.id.validDateTv);
            frmCityTv = itemView.findViewById(R.id.frmCityTv);
            toCityTv = itemView.findViewById(R.id.toCityTv);
        }
    }

    public void refreshItems(List<NormalDiscount> normalDiscountList){
        this.normalDiscountList = normalDiscountList;
        notifyDataSetChanged();
    }

}



