package com.ptravels.offers.presentation;

import com.ptravels.offers.domain.GetAddonsUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public interface OffersPresenter {
    void getAddonsFromServer(GetAddonsUseCase getAddonsUseCase, PathParamRequest request);
}
