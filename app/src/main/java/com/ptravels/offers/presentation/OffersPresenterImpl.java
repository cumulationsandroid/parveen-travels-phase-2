package com.ptravels.offers.presentation;

import com.ptravels.global.Constants;
import com.ptravels.global.Utils;
import com.ptravels.offers.data.model.response.AddonsResponse;
import com.ptravels.offers.domain.GetAddonsUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

import static com.ptravels.REST.RestClient.getErrorMessage;

/**
 * Created by Amit Tumkur on 20-02-2018.
 */

public class OffersPresenterImpl implements OffersPresenter {
    private OffersUiUpdateView offersUiUpdateView;

    public OffersPresenterImpl(OffersUiUpdateView offersUiUpdateView) {
        this.offersUiUpdateView = offersUiUpdateView;
    }

    @Override
    public void getAddonsFromServer(GetAddonsUseCase getAddonsUseCase, PathParamRequest request) {
        offersUiUpdateView.showLoading();
        if (Utils.isOnline()) {
            getAddonsUseCase.execute(request)
                    .subscribe(new SingleSubscriber<AddonsResponse>() {
                        @Override
                        public void onSuccess(AddonsResponse value) {
                            offersUiUpdateView.hideLoading();
                            offersUiUpdateView.provideAddonsList(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            offersUiUpdateView.hideLoading();
                            try {
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    offersUiUpdateView.showError(getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    offersUiUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    offersUiUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                offersUiUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });

        } else {
            offersUiUpdateView.showError(Constants.NO_INTERNET);
        }
    }
}
