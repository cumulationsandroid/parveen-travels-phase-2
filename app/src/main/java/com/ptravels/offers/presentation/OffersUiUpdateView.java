package com.ptravels.offers.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.offers.data.model.response.AddonsResponse;

/**
 * Created by Amit Tumkur on 26-02-2018.
 */

public interface OffersUiUpdateView extends LoadingView{
    void provideAddonsList(AddonsResponse response);
    void showError(String error);
}
