package com.ptravels.seatlayout.data.model;

import com.ptravels.seatlayout.data.model.response.BoardingPoint;
import com.ptravels.seatlayout.data.model.response.DroppingPoint;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cumulations on 2/1/18.
 */

public class BookedSeatModel implements Serializable {
    private ArrayList<LayoutDetail> selectedSeatList;
    private double totalPrice;
    private BoardingPoint boardingPoint;
    private DroppingPoint droppingPoint;
    private String busType;
    private String fromStationName;
    private String toStationName;
    private String travelDate;//date pattern yyyy-MM-dd
    private double totalServiceTax;
    private double bookedSeatTotalCost;
    private String fromStationId;
    private String toStationId;
    private String scheduleId;
    private String blockedSeatPnr;

    public String getBlockedSeatPnr() {
        return blockedSeatPnr;
    }

    public void setBlockedSeatPnr(String blockedSeatPnr) {
        this.blockedSeatPnr = blockedSeatPnr;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getFromStationId() {
        return fromStationId;
    }

    public void setFromStationId(String fromStationId) {
        this.fromStationId = fromStationId;
    }

    public String getToStationId() {
        return toStationId;
    }

    public void setToStationId(String toStationId) {
        this.toStationId = toStationId;
    }

    public double getBookedSeatTotalCost() {
        return bookedSeatTotalCost;
    }

    public void setBookedSeatTotalCost(double bookedSeatTotalCost) {
        this.bookedSeatTotalCost = bookedSeatTotalCost;
    }

    public double getTotalServiceTax() {
        return totalServiceTax;
    }

    public void setTotalServiceTax(double totalServiceTax) {
        this.totalServiceTax = totalServiceTax;
    }
    private String contactEmailId;
    private String contactNumber;
    private List<Passenger> passengerList;

    public String getFromStationName() {
        return fromStationName;
    }

    public void setFromStationName(String fromStationName) {
        this.fromStationName = fromStationName;
    }

    public String getToStationName() {
        return toStationName;
    }

    public void setToStationName(String toStationName) {
        this.toStationName = toStationName;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public ArrayList<LayoutDetail> getSelectedSeatList() {
        return selectedSeatList;
    }

    public void setSelectedSeatList(ArrayList<LayoutDetail> selectedSeatList) {
        this.selectedSeatList = selectedSeatList;
    }

    public BoardingPoint getBoardingPoint() {
        return boardingPoint;
    }

    public void setBoardingPoint(BoardingPoint boardingPoint) {
        this.boardingPoint = boardingPoint;
    }

    public DroppingPoint getDroppingPoint() {
        return droppingPoint;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setDroppingPoint(DroppingPoint droppingPoint) {
        this.droppingPoint = droppingPoint;
    }

    public String getBusType() {
        return busType;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getContactEmailId() {
        return contactEmailId;
    }

    public void setContactEmailId(String contactEmailId) {
        this.contactEmailId = contactEmailId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public List<Passenger> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<Passenger> passengerList) {
        this.passengerList = passengerList;
    }
}
