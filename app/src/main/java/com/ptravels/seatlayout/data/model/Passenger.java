package com.ptravels.seatlayout.data.model;

import java.io.Serializable;

/**
 * Created by Amit Tumkur on 03-01-2018.
 */

public class Passenger implements Serializable{
    private String name;
    private String age;
    private String gender;
    private String seatId;
    private String seatNbr;
    private float serviceTax =0;
    private String availableFor;
    private String seatPrice;
    private String travellingDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    public String getSeatNbr() {
        return seatNbr;
    }

    public void setSeatNbr(String seatNbr) {
        this.seatNbr = seatNbr;
    }

    public float getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(float serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getAvailableFor() {
        return availableFor;
    }

    public void setAvailableFor(String availableFor) {
        this.availableFor = availableFor;
    }

    public String getSeatPrice() {
        return seatPrice;
    }

    public void setSeatPrice(String seatPrice) {
        this.seatPrice = seatPrice;
    }

    public String getTravellingDate() {
        return travellingDate;
    }

    public void setTravellingDate(String travellingDate) {
        this.travellingDate = travellingDate;
    }
}

