
package com.ptravels.seatlayout.data.model.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("layoutDetails")
    @Expose
    private List<LayoutDetail> layoutDetails = null;
    @SerializedName("totalSeatCount")
    @Expose
    private Integer totalSeatCount;
    @SerializedName("availableSeatCount")
    @Expose
    private Integer availableSeatCount;
    @SerializedName("maxRow")
    @Expose
    private Integer maxRow;
    @SerializedName("maxColumn")
    @Expose
    private Integer maxColumn;
    @SerializedName("tierCount")
    @Expose
    private Integer tierCount;
    @SerializedName("boardingPoints")
    @Expose
    private List<BoardingPoint> boardingPoints = null;
    @SerializedName("droppingPoints")
    @Expose
    private List<DroppingPoint> droppingPoints = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LayoutDetail> getLayoutDetails() {
        return layoutDetails;
    }

    public void setLayoutDetails(List<LayoutDetail> layoutDetails) {
        this.layoutDetails = layoutDetails;
    }

    public Integer getTotalSeatCount() {
        return totalSeatCount;
    }

    public void setTotalSeatCount(Integer totalSeatCount) {
        this.totalSeatCount = totalSeatCount;
    }

    public Integer getAvailableSeatCount() {
        return availableSeatCount;
    }

    public void setAvailableSeatCount(Integer availableSeatCount) {
        this.availableSeatCount = availableSeatCount;
    }

    public Integer getMaxRow() {
        return maxRow;
    }

    public void setMaxRow(Integer maxRow) {
        this.maxRow = maxRow;
    }

    public Integer getMaxColumn() {
        return maxColumn;
    }

    public void setMaxColumn(Integer maxColumn) {
        this.maxColumn = maxColumn;
    }

    public Integer getTierCount() {
        return tierCount;
    }

    public void setTierCount(Integer tierCount) {
        this.tierCount = tierCount;
    }

    public List<BoardingPoint> getBoardingPoints() {
        return boardingPoints;
    }

    public void setBoardingPoints(List<BoardingPoint> boardingPoints) {
        this.boardingPoints = boardingPoints;
    }

    public List<DroppingPoint> getDroppingPoints() {
        return droppingPoints;
    }

    public void setDroppingPoints(List<DroppingPoint> droppingPoints) {
        this.droppingPoints = droppingPoints;
    }

}
