package com.ptravels.seatlayout.presentation;

/**
 * Created by Amit Tumkur on 16-03-2018.
 */

public interface BottomSheetDialogCancelTicketListener {
    void bottomSheetDialogCancelTicket();
}
