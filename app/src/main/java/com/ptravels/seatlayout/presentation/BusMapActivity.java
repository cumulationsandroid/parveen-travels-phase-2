package com.ptravels.seatlayout.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.MainThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.bookTicket.presentation.BookTicketActivity;
import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.seatlayout.data.model.SeatMapModel;
import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BoardingPoint;
import com.ptravels.seatlayout.data.model.response.DroppingPoint;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;
import com.ptravels.seatlayout.domain.BusMapUseCase;
import com.ptravels.splash.presentation.SplashActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BusMapActivity extends BaseActivity implements BusMapView, View.OnClickListener {

    BusMapPresenterImpl busMapPresenter;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private BookedSeatModel bookedSeatModel = new BookedSeatModel();

    private TextView totalFareTv, selectedSeatsTv;
    private TextView boardingPointlabelTv;
    private TextView boardingPointTv;
    private TextView dropingPointTv, dropingPointLabelTv;
    private Button bookBt;
    private TextView ladiesSeatsInfoTv,timedetailsTv;
    private boolean onSaveInstanceStateCalled;
    private BoardingPointsFragment boardingPointsFragment;
    private DroppingPointsFragment droppingPointsFragment;
    private FareDeatilsFragment fareDeatilsFragment;
    private CancellationPolicyFragment cancellationPolicyFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_map);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTitle = findViewById(R.id.title);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
//        findViewById(R.id.boardingPoint).setOnClickListener(this);
//        findViewById(R.id.dropping_points).setOnClickListener(this);
        findViewById(R.id.faredeatils).setOnClickListener(this);
        findViewById(R.id.info_button).setOnClickListener(this);
        findViewById(R.id.backBtn).setOnClickListener(this);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);
        totalFareTv = findViewById(R.id.busmap_tv_totalfare);
        selectedSeatsTv = findViewById(R.id.busmap_tv_selectseats);
        boardingPointlabelTv = findViewById(R.id.busmap_tv_boardingpoint_label);
        boardingPointTv = findViewById(R.id.busmap_tv_boardingpoint);
        dropingPointLabelTv = findViewById(R.id.busmap_tv_dropingpoint_label);
        dropingPointTv = findViewById(R.id.busmap_tv_dropingpoint);
        bookBt = findViewById(R.id.busmap_bt_book);
        ladiesSeatsInfoTv = findViewById(R.id.ladiesSeatInfoTV);
        timedetailsTv=findViewById(R.id.timedetails);

        boardingPointTv.setSelected(true);
        dropingPointTv.setSelected(true);
        boardingPointTv.setVisibility(View.GONE);
        dropingPointTv.setVisibility(View.GONE);

//        bookBt.setOnClickListener(this);
        onSaveInstanceStateCalled = false;

        if (getIntent() != null) {
            String fromId = getIntent().getStringExtra(Constants.FROM_STATION_ID);
            String toId = getIntent().getStringExtra(Constants.TO_STATION_ID);
            String travelDate = getIntent().getStringExtra(Constants.TRAVEL_DATE);
            String schedId = getIntent().getStringExtra(Constants.SCHEDULE_ID);
            String frmName = getIntent().getStringExtra(Constants.FROM_STATION_NAME);
            String toName = getIntent().getStringExtra(Constants.TO_STATION_NAME);
            String timing_details=getIntent().getStringExtra(Constants.TIME_DETAILS);



            BusMapRequest request = new BusMapRequest();
            request.setToStation(toId);
            request.setFromStation(fromId);
            request.setTravelDate(travelDate);
            request.setScheduleId(schedId);
            busMapPresenter = new BusMapPresenterImpl(this, new BusMapUseCase());
            busMapPresenter.getBusMapLayout(request);

            bookedSeatModel.setScheduleId(schedId);
            bookedSeatModel.setFromStationId(fromId);
            bookedSeatModel.setToStationId(toId);
            bookedSeatModel.setFromStationName(frmName);
            bookedSeatModel.setToStationName(toName);
            bookedSeatModel.setTravelDate(travelDate);
            bookedSeatModel.setBusType(getIntent().getStringExtra(Constants.BUS_TYPE));

            toolbarTitle.setText(frmName + " - " + toName);
            timedetailsTv.setText(timing_details);
            toolbarTitle.setSelected(true);

        }

        rxSingleClickListener(findViewById(R.id.boardingPoint));
        rxSingleClickListener(findViewById(R.id.dropping_points));
        rxSingleClickListener(bookBt);

    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(View view) {
                onClick(view);
            }
        });
    }


    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
            startActivity(new Intent(this, SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    @Override
    public void showSeatMap(SeatMapModel seatMapModel) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(BusSeatMapFragment.class.getSimpleName());
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (fragment == null)
            ft.add(R.id.fragment_layout,BusSeatMapFragment.loadSeatMap(seatMapModel),BusSeatMapFragment.class.getSimpleName());
        else
            ft.replace(R.id.fragment_layout,BusSeatMapFragment.loadSeatMap(seatMapModel),BusSeatMapFragment.class.getSimpleName());

        ft.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.boardingPoint: {
                if (boardingPointsFragment == null)
                    boardingPointsFragment = new BoardingPointsFragment();
                int selectedBoardingPointId = -1;
                if (busMapPresenter.getBoardingPoints()!=null) {
                    if (bookedSeatModel.getBoardingPoint()!=null)
                        selectedBoardingPointId = bookedSeatModel.getBoardingPoint().getStationPointId();
                    boardingPointsFragment.provideData((ArrayList<BoardingPoint>) busMapPresenter.getBoardingPoints(), selectedBoardingPointId);
                    if (!onSaveInstanceStateCalled
                            && !boardingPointsFragment.isAdded()
                            && (boardingPointsFragment.getDialog()==null
                            || !boardingPointsFragment.getDialog().isShowing()))
                        boardingPointsFragment.show(getSupportFragmentManager(), "sometag");
                } else {
                    Toast.makeText(this, getResources().getString(R.string.no_internet_error), Toast.LENGTH_SHORT).show();
                }
                break;
            }

            case R.id.dropping_points: {
                if (droppingPointsFragment == null)
                    droppingPointsFragment = new DroppingPointsFragment();
                int selectedDropingPointId = -1;
                if (busMapPresenter.getDroppingPoints()!=null) {
                    if (bookedSeatModel.getDroppingPoint()!=null)
                        selectedDropingPointId = bookedSeatModel.getDroppingPoint().getStationPointId();
                    droppingPointsFragment.provideData((ArrayList<DroppingPoint>) busMapPresenter.getDroppingPoints(), selectedDropingPointId);
                    if (!onSaveInstanceStateCalled
                            && !droppingPointsFragment.isAdded()
                            && (droppingPointsFragment.getDialog()==null
                            || !droppingPointsFragment.getDialog().isShowing()))
                        droppingPointsFragment.show(getSupportFragmentManager(), "sometag");
                } else {
                    Toast.makeText(this, getResources().getString(R.string.no_internet_error), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.faredeatils: {
                if (fareDeatilsFragment == null)
                    fareDeatilsFragment = new FareDeatilsFragment();
                if (bookedSeatModel!=null)
                    fareDeatilsFragment.provideData(bookedSeatModel);
                if (!onSaveInstanceStateCalled
                        && !fareDeatilsFragment.isAdded()
                        && (fareDeatilsFragment.getDialog()==null
                        || !fareDeatilsFragment.getDialog().isShowing())) {
                    fareDeatilsFragment.show(getSupportFragmentManager(), FareDeatilsFragment.class.getSimpleName());
                }
                break;
            }

            case R.id.info_button: {
                if (cancellationPolicyFragment == null)
                    cancellationPolicyFragment = new CancellationPolicyFragment();
                if (!onSaveInstanceStateCalled
                        && !cancellationPolicyFragment.isAdded()
                        && (cancellationPolicyFragment.getDialog() == null
                        || !cancellationPolicyFragment.getDialog().isShowing()))
                    cancellationPolicyFragment.show(getSupportFragmentManager(), CancellationPolicyFragment.class.getSimpleName());
                break;
            }

            case R.id.busmap_bt_book: {
//				startActivity(new Intent(this, BookTicketActivity.class));
                if (isAllBookedSeatDataProvided()) {
                    Intent intent = new Intent(this, BookTicketActivity.class);
                    intent.putExtra(Constants.BOOKED_SEAT_DATA, bookedSeatModel);
                    startActivity(intent);
                }
                break;
            }

            case R.id.backBtn: {
                onBackPressed();
                break;
            }
        }
    }

    private boolean isAllBookedSeatDataProvided() {
        if (bookedSeatModel.getSelectedSeatList() == null || bookedSeatModel.getSelectedSeatList().size() == 0) {
            Toast.makeText(this, getResources().getString(R.string.no_select_seat), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (bookedSeatModel.getBoardingPoint() == null) {
            (findViewById(R.id.boardingPoint)).performClick();
            return false;
        }

        if (bookedSeatModel.getDroppingPoint() == null) {
            (findViewById(R.id.dropping_points)).performClick();
            return false;
        }
        return true;
    }

    public void updateUIForSelectedSeats(LinkedHashMap<String, LayoutDetail> selectedSeatMap) {
        double totalSeatFare = 0;
        double totalServiceTax = 0;
        StringBuilder seatsSelected = new StringBuilder();
        StringBuilder ladiesSeatSelected = new StringBuilder();
        for (LayoutDetail layoutDetail : selectedSeatMap.values()) {
            totalSeatFare =  totalSeatFare + layoutDetail.getFare();
            totalServiceTax = totalServiceTax + layoutDetail.getServiceTax();
            if (seatsSelected.toString().isEmpty()) {
                seatsSelected.append(layoutDetail.getSeatNumber());
            } else {
                seatsSelected.append(", " + layoutDetail.getSeatNumber());
            }

            if (layoutDetail.getAvailableFor().equalsIgnoreCase("FEMALE")){
                if (ladiesSeatSelected.toString().isEmpty()) {
                    ladiesSeatSelected.append(layoutDetail.getSeatNumber());
                } else {
                    ladiesSeatSelected.append(", " + layoutDetail.getSeatNumber());
                }
            }
        }
        bookedSeatModel.setSelectedSeatList(new ArrayList<LayoutDetail>(selectedSeatMap.values()));
        bookedSeatModel.setBookedSeatTotalCost(totalSeatFare);
        bookedSeatModel.setTotalPrice(totalSeatFare + totalServiceTax);
        bookedSeatModel.setTotalServiceTax(totalServiceTax);

        selectedSeatsTv.setText(seatsSelected.toString());
        if (bookedSeatModel.getTotalPrice() < 1)
            totalFareTv.setText("");
        else
            totalFareTv.setText("₹ " + bookedSeatModel.getTotalPrice());
        Log.e("!!seates selected", seatsSelected.toString());
        Log.e("!!total price", "₹ " + bookedSeatModel.getTotalPrice());

        /*if (ladiesSeatSelected.toString().isEmpty())
            ladiesSeatsInfoTv.setVisibility(View.GONE);
        else {
            ladiesSeatsInfoTv.setVisibility(View.VISIBLE);
            ladiesSeatsInfoTv.setText("Note : Seat Number " + ladiesSeatSelected + " available for ladies only");
        }*/
    }

    public void setBoardingPoint(BoardingPoint selectedBoardingPoint) {
        bookedSeatModel.setBoardingPoint(selectedBoardingPoint);
        updateUI();
        if (dropingPointTv.getText().toString().isEmpty()) {
            (findViewById(R.id.dropping_points)).performClick();
        }
    }

    public void setDropingPoint(DroppingPoint selectedDropingPoint) {
        bookedSeatModel.setDroppingPoint(selectedDropingPoint);
        updateUI();
    }

    public void updateUI() {
        if (bookedSeatModel.getBoardingPoint() != null) {
            boardingPointlabelTv.setSelected(true);
            boardingPointTv.setVisibility(View.VISIBLE);
            boardingPointTv.setText(bookedSeatModel.getBoardingPoint().getStationPointName());
        }

        if (bookedSeatModel.getDroppingPoint() != null) {
            dropingPointLabelTv.setSelected(true);
            dropingPointTv.setVisibility(View.VISIBLE);
            dropingPointTv.setText(bookedSeatModel.getDroppingPoint().getStationPointName());
        }

        if (bookedSeatModel.getTotalPrice() != 0) {
            totalFareTv.setText("₹ " + bookedSeatModel.getTotalPrice());
        }
    }

    /*Workaround for java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState*/
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        onSaveInstanceStateCalled = true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onSaveInstanceStateCalled = false;
    }
}
