package com.ptravels.seatlayout.presentation;

import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.SeatMapModel;
import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BoardingPoint;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;
import com.ptravels.seatlayout.data.model.response.DroppingPoint;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;
import com.ptravels.seatlayout.domain.BusMapUseCase;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by praveenkumar on 15/12/17.
 */

public class BusMapPresenterImpl implements BusMapPresenter {

    private final BusMapView busMapView;
    BusMapUseCase busMapUseCase;

    //    List<LayoutDetail> tierOnelist, tierTwoList;
    List<BoardingPoint> boardingPoints;
    List<DroppingPoint> droppingPoints;

    public BusMapPresenterImpl(BusMapView view, BusMapUseCase busMapUseCase) {
        this.busMapView = view;
        this.busMapUseCase = busMapUseCase;
    }

    @Override
    public void getBusMapLayout(final BusMapRequest request) {
        if (Utils.isOnline()) {
            busMapView.showLoading();
            busMapUseCase.execute(request).subscribe(new SingleSubscriber<BusMapResponse>() {
                @Override
                public void onSuccess(final BusMapResponse response) {

                    if (response.getSuccess()) {
                        Observable.create(new Observable.OnSubscribe<SeatMapModel>() {
                            @Override
                            public void call(Subscriber<? super SeatMapModel> subscriber) {
                                List<LayoutDetail> tierOneList;
                                tierOneList = Observable.from(response.getData().getLayoutDetails())
                                        .filter(new Func1<LayoutDetail, Boolean>() {
                                            @Override
                                            public Boolean call(LayoutDetail layoutDetail) {
                                                return layoutDetail.getTier() == 1;
                                            }
                                        }).toList().toBlocking().single();

                                List<LayoutDetail> tierTwoList;
                                tierTwoList = Observable.from(response.getData().getLayoutDetails())
                                        .filter(new Func1<LayoutDetail, Boolean>() {
                                            @Override
                                            public Boolean call(LayoutDetail layoutDetail) {
                                                return layoutDetail.getTier() == 2;
                                            }
                                        }).toList().toBlocking().single();

                                //Sort the columns by ascending order and then sort the rows by descending order
                                Collections.sort(tierOneList, new SeatMapSorter());
                                Collections.sort(tierTwoList, new SeatMapSorter());

                                boardingPoints = response.getData().getBoardingPoints();
                                droppingPoints = response.getData().getDroppingPoints();

                                Collections.sort(boardingPoints, new BoardingTimeMillisSortComparator(request.getTravelDate()));
                                Collections.sort(droppingPoints, new DroppingTimeMillisSortComparator(request.getTravelDate()));

                                SeatMapModel seatMapModel = new SeatMapModel();
                                seatMapModel.setNoColumns(response.getData().getMaxRow());
                                seatMapModel.setNoRows(response.getData().getMaxColumn());
                                seatMapModel.setLowerSeatList(tierOneList);

                                for (LayoutDetail detail:tierTwoList){
                                //  Only madurai-chennai_sivakasi_10pm case
                                    if (detail.getRow()== 4 && detail.getColumn() == 7){
                                        LayoutDetail R4C7prevEmptySeatDetail1 = new LayoutDetail();
                                        R4C7prevEmptySeatDetail1.setSeatType("AS");
                                        R4C7prevEmptySeatDetail1.setSeatNumber("-");
                                        R4C7prevEmptySeatDetail1.setStatus("BL");
                                        R4C7prevEmptySeatDetail1.setAvailableFor("-");
                                        R4C7prevEmptySeatDetail1.setTier(2);
                                        R4C7prevEmptySeatDetail1.setServiceTax((double) 0);
                                        R4C7prevEmptySeatDetail1.setRow(5);
                                        R4C7prevEmptySeatDetail1.setColumn(7);
                                        if (tierTwoList.size()>30){
                                            tierTwoList.add(30,R4C7prevEmptySeatDetail1);
                                        }
                                    }
                                }

                                seatMapModel.setUpperSeatList(tierTwoList);
                                seatMapModel.setTireCount(response.getData().getTierCount());

                                subscriber.onNext(seatMapModel);
                                subscriber.onCompleted();

                            }
                        }).observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Action1<SeatMapModel>() {
                                    @Override
                                    public void call(SeatMapModel seatMapModel) {
                                        busMapView.hideLoading();
                                        busMapView.showSeatMap(seatMapModel);
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {

                                    }
                                });
                    } else if (!response.getSuccess()) {
                        String errMsg = RestClient.get200StatusErrors(response.getError());
                        busMapView.showError(errMsg);
                    }

                }

                @Override
                public void onError(Throwable error) {
                    busMapView.hideLoading();
                    try {
                        error.printStackTrace();
                        if (error instanceof HttpException) {
                            ResponseBody responseBody = ((HttpException) error).response().errorBody();
                            busMapView.showError(RestClient.getErrorMessage(responseBody));
                        } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                            busMapView.showError(Constants.NETWORK_ERROR);
                        } else {
                            busMapView.showError(error.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        busMapView.showError(Constants.UNKNOWN_ERROR);
                    }
                }
            });
        } else {
            busMapView.showError(Constants.NO_INTERNET);
        }
    }

    public class SeatMapSorter implements Comparator<LayoutDetail> {
        @Override
        public int compare(LayoutDetail o1, LayoutDetail o2) {
            if (o1.getColumn() - o2.getColumn() == 0) {
                return o2.getRow() - o1.getRow();
            } else
                return o1.getColumn() - o2.getColumn();
        }
    }

    public List<BoardingPoint> getBoardingPoints() {
        return boardingPoints;
    }

    public List<DroppingPoint> getDroppingPoints() {
        return droppingPoints;
    }

    public class BoardingTimeMillisSortComparator implements Comparator<BoardingPoint> {
        String travelDate;

        public BoardingTimeMillisSortComparator(String travelDate) {
            this.travelDate = travelDate;
        }

        @Override
        public int compare(BoardingPoint o1, BoardingPoint o2) {
            o1.stationPtTimeInMillis = Utils.getDeptTimeInMillis(travelDate, "yyyy-MM-dd",
                    o1.getStationPointTime(), "HH:mm:ss");
            o2.stationPtTimeInMillis = Utils.getDeptTimeInMillis(travelDate, "yyyy-MM-dd",
                    o2.getStationPointTime(), "HH:mm:ss");

            return Long.valueOf(o1.stationPtTimeInMillis).compareTo((o2.stationPtTimeInMillis));
        }
    }

    public class DroppingTimeMillisSortComparator implements Comparator<DroppingPoint> {
        String travelDate;

        public DroppingTimeMillisSortComparator(String travelDate) {
            this.travelDate = travelDate;
        }

        @Override
        public int compare(DroppingPoint o1, DroppingPoint o2) {
            o1.stationPtTimeInMillis = Utils.getDeptTimeInMillis(travelDate, "yyyy-MM-dd",
                    o1.getStationPointTime(), "HH:mm:ss");
            o2.stationPtTimeInMillis = Utils.getDeptTimeInMillis(travelDate, "yyyy-MM-dd",
                    o2.getStationPointTime(), "HH:mm:ss");
            return Long.valueOf(o1.stationPtTimeInMillis).compareTo((o2.stationPtTimeInMillis));
        }
    }
}
