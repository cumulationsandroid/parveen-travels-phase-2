package com.ptravels.seatlayout.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.seatlayout.data.model.SeatMapModel;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.util.ArrayList;

/**
 * Created by cumulations on 28/12/17.
 */

public class BusSeatMapFragment extends Fragment {
    Layout_Type type;
    private SeatMapModel seatMapModel;
    private RecyclerView gridRecyclerView;
    private BusMapSeatAdapter adapter;
    private TextView lowerTv;
    private TextView upperTv;
    private ImageView steeringIcon;

    public static BusSeatMapFragment loadSeatMap(SeatMapModel seatMapModel) {
        BusSeatMapFragment fragment1 = new BusSeatMapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.SEATMAP_MODEL, seatMapModel);
        fragment1.setArguments(bundle);
        return fragment1;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_bus_seatmap,
                container, false);
        gridRecyclerView = inflate.findViewById(R.id.grid_layout);

        lowerTv = inflate.findViewById(R.id.seat_fragment_tv_lower);
        upperTv = inflate.findViewById(R.id.seat_fragment_tv_upper);
        steeringIcon = inflate.findViewById(R.id.steering);

        inflate.findViewById(R.id.seat_fragment_tv_lower).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lowerUpperChange();
            }
        });
        inflate.findViewById(R.id.seat_fragment_tv_upper).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lowerUpperChange();
            }
        });

        if (getArguments() != null && getArguments().getSerializable(Constants.SEATMAP_MODEL) != null) {
            seatMapModel = (SeatMapModel) getArguments().getSerializable(Constants.SEATMAP_MODEL);
            if (seatMapModel != null) {
                if (seatMapModel.getTireCount() == 1) {
                    type = Layout_Type.SINGLE_SEATER;
                    inflate.findViewById(R.id.seat_header).setVisibility(View.GONE);
                } else {
                    type = Layout_Type.LOWER_UPPER_SEATER;
                    inflate.findViewById(R.id.seat_header).setVisibility(View.VISIBLE);
                    lowerTv.setSelected(true);
                }
            }
        }

        initiateShowLayout();
        return inflate;
    }

    public void lowerUpperChange() {
        if (lowerTv.isSelected()) {
            steeringIcon.setVisibility(View.INVISIBLE);
            adapter.setType(Layout_Type.UPPER_SEATER);
            adapter.setUpperSeatlist((ArrayList<LayoutDetail>) seatMapModel.getUpperSeatList());
            lowerTv.setSelected(false);
            upperTv.setSelected(true);
        } else {
            steeringIcon.setVisibility(View.VISIBLE);
            adapter.setType(Layout_Type.LOWER_SEATER);
            adapter.setLowerSeatList((ArrayList<LayoutDetail>) seatMapModel.getLowerSeatList());
            upperTv.setSelected(false);
            lowerTv.setSelected(true);
        }
    }

    public void initiateShowLayout() {
        if (getActivity() != null && seatMapModel != null) {
            adapter = new BusMapSeatAdapter((BusMapActivity) getActivity(), type);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), seatMapModel.getNoColumns());
            gridRecyclerView.setLayoutManager(gridLayoutManager);
            gridRecyclerView.setItemAnimator(null);
            gridRecyclerView.setAdapter(adapter);
            adapter.setLowerSeatList((ArrayList<LayoutDetail>) seatMapModel.getLowerSeatList());
            logViewEventToFabric();
        }


    }

    private void logViewEventToFabric() {
        try {
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putContentId("Seat Layout")
                    .putContentName("Seat Layout")
                    .putContentType("Seat Layout"));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public Layout_Type getType() {
        return type;
    }

}
