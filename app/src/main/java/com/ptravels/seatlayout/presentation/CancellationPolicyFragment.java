package com.ptravels.seatlayout.presentation;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;

import com.ptravels.R;
import com.ptravels.global.Constants;

/**
 * Created by praveenkumar on 25/12/17.
 */

public class CancellationPolicyFragment extends BottomSheetDialogFragment {
    private BottomSheetDialogCancelTicketListener dialogCancelTicketListener;
    private static CancellationPolicyFragment cancellationPolicyFragment;

    public static CancellationPolicyFragment showCancelBtn(boolean showBtn){
        if (cancellationPolicyFragment == null)
            cancellationPolicyFragment = new CancellationPolicyFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.SHOW_CANCEL_BTN,showBtn);
        cancellationPolicyFragment.setArguments(bundle);
        return cancellationPolicyFragment;
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.fragment_cancelation_policy, null);
        dialog.setContentView(contentView);


        ImageView backButton= contentView.findViewById(R.id.closeicon);
        AppCompatButton cancelBtn = contentView.findViewById(R.id.cancelTicketBtn);

        if (getArguments()!=null && getArguments().getBoolean(Constants.SHOW_CANCEL_BTN)){
            cancelBtn.setVisibility(View.VISIBLE);
            setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        } else {
            setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            cancelBtn.setVisibility(View.GONE);
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCancelTicketListener.bottomSheetDialogCancelTicket();
                dismiss();
            }
        });
    }

    public void setDialogCancelTicketListener(BottomSheetDialogCancelTicketListener dialogCancelTicketListener) {
        this.dialogCancelTicketListener = dialogCancelTicketListener;
    }
}
