package com.ptravels.seatlayout.presentation;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.BookedSeatModel;

/**
 * Created by praveenkumar on 25/12/17.
 */

public class FareDeatilsFragment extends BottomSheetDialogFragment {
    BookedSeatModel bookedSeatModel;

    public void provideData(BookedSeatModel bookedSeatModel) {
        this.bookedSeatModel = bookedSeatModel;
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.fragment_faredeatils, null);
        dialog.setContentView(contentView);

        TextView fromToNameTv = contentView.findViewById(R.id.fromToValue);
        TextView travelDateTv = contentView.findViewById(R.id.fare_detailt_tv_traveldate);
        TextView boardingPtTv = contentView.findViewById(R.id.boardingPointName);
        TextView droppingPtTv = contentView.findViewById(R.id.droppingPtName);
        TextView totalSeatPriceTv = contentView.findViewById(R.id.fare_detailt_tv_seatcost);
        TextView totalGSTcost = contentView.findViewById(R.id.fare_detailt_tv_gstcost);
        TextView totalPrice = contentView.findViewById(R.id.fare_detailt_tv_totalcost);
        TextView busTypeTv = contentView.findViewById(R.id.fare_detailt_tv_bustype);
        TextView boardingTimeTv = contentView.findViewById(R.id.boardingPointTime);
        TextView droppingTimeTv = contentView.findViewById(R.id.droppingPointTime);

        contentView.findViewById(R.id.pnr).setVisibility(View.GONE);

        fromToNameTv.setText(bookedSeatModel.getFromStationName()+" \u2794 "+bookedSeatModel.getToStationName());
        if (bookedSeatModel.getBoardingPoint()!=null && bookedSeatModel.getDroppingPoint()!=null) {
            boardingPtTv.setText("Boarding point : "+bookedSeatModel.getBoardingPoint().getStationPointName());
            droppingPtTv.setText("Dropping point : "+bookedSeatModel.getDroppingPoint().getStationPointName());
        }
        totalPrice.setText("\u20B9 "+String.valueOf(bookedSeatModel.getTotalPrice()));
        totalGSTcost.setText("\u20B9 "+String.valueOf(bookedSeatModel.getTotalServiceTax()));
        totalSeatPriceTv.setText("\u20B9 "+String.valueOf(bookedSeatModel.getBookedSeatTotalCost()));
        busTypeTv.setText(bookedSeatModel.getBusType());

        busTypeTv.setSelected(true);
        boardingPtTv.setSelected(true);
        droppingPtTv.setSelected(true);

        if (bookedSeatModel.getTravelDate() != null) {
            travelDateTv.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, EEE", bookedSeatModel.getTravelDate()));
        }
        if (bookedSeatModel.getBoardingPoint() != null && bookedSeatModel.getBoardingPoint().getStationPointTime() != null) {
            boardingTimeTv.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", bookedSeatModel.getBoardingPoint().getStationPointTime()));
        }

        if (bookedSeatModel.getDroppingPoint() != null && bookedSeatModel.getDroppingPoint().getStationPointTime() != null) {
            droppingTimeTv.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", bookedSeatModel.getDroppingPoint().getStationPointTime()));
        }

        ImageView backButton = contentView.findViewById(R.id.closeicon);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
    }
}
