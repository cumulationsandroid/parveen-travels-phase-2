package com.ptravels.signin.data.model;

import java.io.Serializable;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SigninRequest implements Serializable
{
    private String email;
    private String password;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
