package com.ptravels.signup.presentation;

import com.ptravels.signup.data.model.SignupRequest;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SignupPresenter {
    void getSignupResponse(SignupRequest request);
}
