package com.ptravels.stationsList;

/**
 * Created by cumulations on 9/1/18.
 */

public enum SignInErrorTypes
{
    EMAIL_ID_DOESNOT_EXSIT("Email id doesn't not exist"),
    WRONG_PASSWORD("Wrong password");

    private String errorType;
    SignInErrorTypes(String errorType){
        this.errorType=errorType;
    }

    public String getErrorType() {
        return errorType;
    }
}
