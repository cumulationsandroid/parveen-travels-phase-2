package com.ptravels.stationsList.domain;

import com.ptravels.global.UseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;
import com.ptravels.stationsList.data.model.response.StationsListResponse;
import com.ptravels.stationsList.data.source.GetStationsDataSourceImpl;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class GetStationsUseCase extends UseCase<PathParamRequest,StationsListResponse>{

    private GetStationsDataInterface stationsDataSource = new GetStationsDataSourceImpl();

    @Override
    public Single<StationsListResponse> buildUseCase(PathParamRequest requestObj) {
        return stationsDataSource.getStations();
    }
}
