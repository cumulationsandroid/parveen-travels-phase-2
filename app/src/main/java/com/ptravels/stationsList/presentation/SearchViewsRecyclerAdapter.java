package com.ptravels.stationsList.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.R;
import com.ptravels.global.Constants;
import com.ptravels.stationsList.data.model.response.Station;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit Tumkur on 15-12-2017.
 */

public class SearchViewsRecyclerAdapter extends RecyclerView.Adapter<SearchViewsRecyclerAdapter.ViewHolder> implements Filterable {
    private List<Station> mStationList;
    private List<Station> mFilteredStationList;
    private Context context;

    public SearchViewsRecyclerAdapter(Context context,List<Station> mStationList) {
        this.context = context;
        this.mStationList = mStationList;
        this.mFilteredStationList = mStationList;
    }

    @Override
    public SearchViewsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.searchview_results_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SearchViewsRecyclerAdapter.ViewHolder viewHolder, int i) {
        viewHolder.stationName.setSelected(true);
        viewHolder.stationName.setText(mFilteredStationList.get(i).getName());

        final int position = i;
        viewHolder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Rare case handling : java.lang.IndexOutOfBoundsException*/
                /*if (position > mFilteredStationList.size())
                    return;*/
                try {
                    ((SearchActivity)context).updateSearchview(mFilteredStationList.get(position));
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(context, ""+Constants.UNKNOWN_ERROR, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilteredStationList.size();
    }



    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

              List<Station>  mFilteredStationList;
                String charString = charSequence.toString();

                if (charString.isEmpty() || charString.length() == 0) {
                    mFilteredStationList = mStationList;
                } else {
                    List<Station> filteredList = new ArrayList<>();
                    for (Station station : mStationList) {
                        if (station.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(station);
                        }
                    }
                    mFilteredStationList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredStationList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredStationList = (List<Station>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView stationName;
        private LinearLayout clickLayout;
        public ViewHolder(View view) {
            super(view);
            stationName = view.findViewById(R.id.stationName);
            clickLayout = view.findViewById(R.id.clickLayout);
        }
    }
}
