package com.ptravels.stationsList.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface StationListView extends LoadingView{
    void showStationListResults(StationsListResponse response);
    void showError(String error);
}
